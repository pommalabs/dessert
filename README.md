# Dessert

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docs][doxygen-docs-badge]][doxygen-docs]
[![NuGet version][nuget-version-badge]][nuget-package]
[![NuGet downloads][nuget-downloads-badge]][nuget-package]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Renovate enabled][renovate-badge]][renovate-website]

Discrete event simulation (DES) engine heavily based on the paradigm introduced by Simula and SimPy.

The aim of project Dessert is to bring the "powerful simplicity" of the [SimPy library][gitlab-simpy]
to the .NET ecosystem; what we are trying to do is to keep the concepts introduced by SimPy,
while offering much better performance, especially for large scale simulations.

Project was developed by [Alessio Parma][personal-website] and [Giovanni Lagorio][lagorio-website].
Since we do not have too much manpower and time to invest in this project,
our current goals are to maintain a working "clone" of the release 3.0 of SimPy.

For the same reasons, documentation is pretty short: in any case, please refer to our working examples
([C#][dessert-csharp-samples], [F#][dessert-fsharp-samples], [Visual Basic .NET][dessert-vbnet-samples])
to get a better insight of what you can do with Dessert, and how the code really resembles the one you could write with SimPy.

A [scientific paper][dessert-paper] has been written on this project and, if you understand Italian,
project development has been documented in a thesis ([PDF][dessert-essay], [slides][dessert-slides])

## Table of Contents

- [Install](#install)
- [Usage](#usage)
  - [C#](#c)
  - [F#](#f)
  - [Visual Basic .NET](#visual-basic-net)
  - [Dessert or SimPy?](#dessert-or-simpy)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Running tests](#running-tests)
- [License](#license)

## Install

NuGet package [PommaLabs.Dessert][nuget-package] is available for download:

```bash
dotnet add package PommaLabs.Dessert
```

## Usage

We will start by translating the first example exposed in the [SimPy documentation][simpy-documentation],
where we simulate the life of a simple clock. This is the original example:

```py
import simpy

def clock(env, name, tick):
    while True:
        print(name, env.now)
        yield env.timeout(tick)

env = simpy.Environment()
env.process(clock(env, 'fast', 0.5))
env.process(clock(env, 'slow', 1))
env.run(until=2)
```

And this is its output:

```txt
fast 0.0
slow 0.0
fast 0.5
slow 1.0
fast 1.0
fast 1.5
```

### C&#35;

Compared to Python, C# is far more verbose and full of unnecessary characters, like semicolons and brackets. However, we tried to mimic SimPy APIs as well as we could, in order to reduce to the minimum the syntactic "noise" coming from the usage of C#.

Let's see how the clock example gets translated into that language:

```cs
using System;
using System.Collections.Generic;
using PommaLabs.Dessert;

static class ClockExample
{
    static IEnumerable<IEvent> Clock(IEnvironment env, string name, double tick)
    {
        while (true) 
        {
            Console.WriteLine("{0} {1:0.0}", name, env.Now);
            yield return env.Timeout(tick);
        }
    }

    static void Run()
    {
        var env = Sim.Environment();
        env.Process(Clock(env, "fast", 0.5));
        env.Process(Clock(env, "slow", 1.0));
        env.Run(until: 2);
    }
}
```

### F&#35;

F#, thanks to a lean syntax and to its functional principles,
lets us write code which is considerably shorter than the one written in C# or in Visul Basic .NET.
However, there are a few "quirks" caused by the stricter typing,
but they do not hamper too much the usage of Dessert.

The following is the translation in F# of the clock example:

```fs
open PommaLabs.Dessert

let rec clock(env: IEnvironment, name, tick) = seq<IEvent> { 
    printfn "%s %.1f" name env.Now 
    yield upcast env.Timeout(tick)
    yield! clock(env, name, tick)
}

let run() =
    let env = Sim.Environment() 
    env.Process(clock(env, "fast", 0.5)) |> ignore
    env.Process(clock(env, "slow", 1.0)) |> ignore
    env.Run(until = 2)
```

### Visual Basic .NET

Visual Basic .NET is _enormously_ more verbose than Python and C#,
but the original simplicity of SimPy is preserved:

```vbnet
Module ClockExample
    Iterator Function Clock(env As IEnvironment, name As String, tick As Double) _
    As IEnumerable(Of IEvent)
        While True
            Console.WriteLine("{0} {1:0.0}", name, env.Now)
            Yield env.Timeout(tick)
        End While
    End Function

    Sub Run()
        Dim env = Sim.Environment()
        env.Process(Clock(env, "fast", 0.5))
        env.Process(Clock(env, "slow", 1.0))
        env.Run(until:=2)
    End Sub
End Module
```

### Dessert or SimPy?

This question often pops up in messages we receive, so we think its better to answer it here. 
First of all, let's start with a quick side to side comparison of the two projects:

|               | Dessert                   | SimPy                            |
|---------------|---------------------------|----------------------------------|
| License       | MIT                       | MIT                              |
| Language      | C#, F#, Visual Basic .NET | Python                           |
| Status        | Working and tested        | Production ready                 |
| Documentation | Samples, tests            | Samples, tests and proper docs   |
| OS            | Windows, GNU/Linux        | Windows, GNU/Linux               |
| Performance   | Fast                      | Can be vastly improved with PyPy |
| Support       | Very limited, best effort | Not known, never tried           |

If you need a stable project, documented and maintained, then SimPy is the right choice, at least as of April 2016.
When coupled with PyPy, SimPy can offer more than decent performance and its readability is unmatched.

Dessert, on the other hand, is a working project, but, as of April 2016,
it has been nearly 30 months since the last "important" commits.
Project has received minor fixes during those months,
but no work has been done to ensure that functionality is still aligned to the one offered by SimPy.
That might, and probably will, happen, but there is not a proper timeline.

Summing up, you can use Dessert for practical purposes, but be prepared to face some issues.
Should you find them, please report them through GitHub, so that they will be handled as soon as possible.
However, since both maintainers can work on Dessert only during their spare time,
answers or fixes might take a few days to be prepared.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `dessert.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Running tests

Tests can be run with following command:

```bash
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
./build.sh --target run-tests
```

## License

MIT © 2012-2022 [Alessio Parma][personal-website]

[dessert-csharp-samples]: https://gitlab.com/pommalabs/dessert/tree/main/samples/PommaLabs.Dessert.Samples.CSharp
[dessert-essay]: https://alessioparma.xyz/documents/dessert-thesis-essay.pdf
[dessert-fsharp-samples]: https://gitlab.com/pommalabs/dessert/tree/main/samples/PommaLabs.Dessert.Samples.FSharp
[dessert-paper]: https://alessioparma.xyz/documents/dessert-thesis-paper.pdf
[dessert-slides]: https://alessioparma.xyz/documents/dessert-thesis-slides.pdf
[dessert-vbnet-samples]: https://gitlab.com/pommalabs/dessert/tree/main/samples/PommaLabs.Dessert.Samples.VisualBasic
[doxygen-docs]: https://pommalabs.gitlab.io/dessert/
[doxygen-docs-badge]: https://img.shields.io/badge/Doxygen-OK-green?style=flat-square
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/dessert/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/dessert/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[gitlab-simpy]: https://gitlab.com/team-simpy/simpy/
[lagorio-website]: https://person.dibris.unige.it/lagorio-giovanni/
[nuget-downloads-badge]: https://img.shields.io/nuget/dt/PommaLabs.Dessert?style=flat-square
[nuget-package]: https://www.nuget.org/packages/PommaLabs.Dessert/
[nuget-version-badge]: https://img.shields.io/nuget/v/PommaLabs.Dessert?style=flat-square
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[project-license]: https://gitlab.com/pommalabs/dessert/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[simpy-documentation]: https://simpy.readthedocs.org/en/latest/index.html
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
