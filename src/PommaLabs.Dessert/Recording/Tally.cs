﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using PommaLabs.Dessert.Core;

namespace PommaLabs.Dessert.Recording
{
    /// <summary>
    ///   An instance of this interface records enough information (such as sums and sums of squares)
    ///   while the simulation runs to return simple data summaries. This has the advantage of speed and low memory use.
    ///   However, they do not preserve a time-series usable in more advanced statistical analysis. 
    /// </summary>
    /// <remarks>
    ///   Monitors and tallies may not be bound to a specific <see cref="SimEnvironment"/>, in order to ease
    ///   their usage in inter environment recordings; when they are unbound their <see cref="SimEntity.Env"/>
    ///   property points to a dummy environment.<br/>
    ///   However, please pay attention to the fact that both monitors and tallies are not thread safe:
    ///   therefore, recall this fact when you use them in a multi threaded simulation scenario. 
    /// </remarks>
    public sealed class Tally : SimEntity, IRecorder
    {
        private double _integral;
        private double _integral2;
        private double _lastSample;
        private double _sum;
        private double _sumOfSquares;

        internal Tally(SimEnvironment env) : base(env)
        {
            StartTime = LastTime = env.Now;
        }

        private void DoObserve(double sample, double time)
        {
            _integral += (time - LastTime) * _lastSample;
            _integral2 += (time - LastTime) * _lastSample * _lastSample;
            _lastSample = sample;
            LastTime = time;
            Count++;
            _sum += sample;
            _sumOfSquares += sample * sample;
        }

        private void DoReset(double time)
        {
            StartTime = LastTime = time;
            _integral = _integral2 = 0;
            _lastSample = 0;
            Count = 0;
            _sum = _sumOfSquares = 0;
        }

        #region IRecorder Members

        public int Count { get; private set; }

        public double LastTime { get; private set; }

        public double StartTime { get; private set; }

        public double Mean()
        {
            return _sum / Count;
        }

        public void Observe(double sample)
        {
            DoObserve(sample, Env.Now);
        }

        public void Observe(double sample, double time)
        {
            // Preconditions
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            DoObserve(sample, time);
        }

        public void Reset()
        {
            DoReset(Env.Now);
        }

        public void Reset(double time)
        {
            DoReset(time);
        }

        public double StdDev()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return Math.Sqrt(Variance());
        }

        public double TimeMean()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return TimeMean(Env.Now);
        }

        public double TimeMean(double time)
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            var integral = _integral + (time - LastTime) * _lastSample;
            return integral / (time - StartTime);
        }

        public double TimeStdDev()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return TimeStdDev(Env.Now);
        }

        public double TimeStdDev(double time)
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            return Math.Sqrt(TimeVariance(time));
        }

        public double TimeVariance()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return TimeVariance(Env.Now);
        }

        public double TimeVariance(double time)
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            var timeMean = TimeMean(time);
            var integral2 = _integral2 + (time - LastTime) * _lastSample * _lastSample;
            return integral2 / (time - StartTime) - timeMean * timeMean;
        }

        public double Total()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return _sum;
        }

        public double Variance()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return (_sumOfSquares - ((_sum * _sum) / Count)) / Count;
        }

        #endregion
    }
}
