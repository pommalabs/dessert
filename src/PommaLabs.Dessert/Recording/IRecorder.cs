﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Recording
{
    public interface IRecorder
    {
        /// <summary>
        ///    The current number of observations.
        /// </summary>
        int Count { get; }

        /// <summary>
        ///   Returns the environment in which this entity was created.
        /// </summary>
        SimEnvironment Env { get; }

        /// <summary>
        ///   The time of last recording.
        /// </summary>
        double LastTime { get; }

        /// <summary>
        ///   The time at which recording has started.
        /// </summary>
        double StartTime { get; }

        /// <summary>
        ///   Returns the simple average of the observed values, ignoring the times at which they were made.
        ///   This is equal to <code>Total/Count</code>.
        /// </summary>
        /// <returns>The simple average of the observed values, ignoring the times at which they were made.</returns>
        /// <exception cref="InvalidOperationException">There are no observations.</exception>
        double Mean();

        /// <summary>
        ///   Records the current value of the variable <paramref name="sample"/>.
        ///   Since time has not been specified, it is set to <see cref="SimEnvironment.Now"/>.     
        /// </summary>
        /// <param name="sample">The value that has to be recorded.</param>
        /// <remarks>
        ///   An <see cref="Monitor"/> retains the two values as a pair (time, sample), while
        ///   a <see cref="Tally"/> uses them to update the accumulated statistics.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   Implicitly assigned time is less than the last observation time.
        /// </exception>
        void Observe(double sample);

        /// <summary>
        ///   Records the current value of the variable <paramref name="sample"/> at given <paramref name="time"/>.     
        /// </summary>
        /// <param name="sample">The value that has to be recorded.</param>
        /// <param name="time">The time that will be associated with given value.</param>
        /// <remarks>
        ///   An <see cref="Monitor"/> retains the two values as a pair (time, sample), while
        ///   a <see cref="Tally"/> uses them to update the accumulated statistics.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   <paramref name="time"/> is less than the last observation time.
        /// </exception>
        void Observe(double sample, double time);

        /// <summary>
        ///   Resets the observations. The recorded data is re-initialized,
        ///   and the observation starting time is set to <see cref="SimEnvironment.Now"/>.
        /// </summary>
        void Reset();

        /// <summary>
        ///   Resets the observations. The recorded data is re-initialized,
        ///   and the observation starting time is set to <paramref name="time"/>.
        /// </summary>
        void Reset(double time);

        /// <summary>
        ///   Returns the standard deviation of the observations, computed as the square root of <see cref="Variance"/>.
        /// </summary>
        /// <returns>The standard deviation of the observations, computed as the square root of <see cref="Variance"/>.</returns>
        /// <exception cref="InvalidOperationException">There are no observations.</exception>
        double StdDev();

        /// <summary>
        ///   Returns the time-weighted mean, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to current time.
        /// </summary>
        /// <returns>
        ///   The time-weighted average, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to current time.
        /// </returns>
        double TimeMean();

        /// <summary>
        ///   Returns the time-weighted mean, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to <paramref name="time"/>.
        /// </summary>
        /// <returns>
        ///   The time-weighted average, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to <paramref name="time"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   <paramref name="time"/> is less than <see cref="StartTime"/>.
        /// </exception>
        double TimeMean(double time);

        /// <summary>
        ///   Returns the time-weighted variance, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to current time.
        /// </summary>
        /// <returns>
        ///   The time-weighted average, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to current time.
        /// </returns>
        double TimeStdDev();

        /// <summary>
        ///   Returns the time-weighted variance, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to <paramref name="time"/>.
        /// </summary>
        /// <returns>
        ///   The time-weighted average, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to <paramref name="time"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   <paramref name="time"/> is less than <see cref="StartTime"/>.
        /// </exception>
        double TimeStdDev(double time);

        /// <summary>
        ///   Returns the time-weighted variance, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to current time.
        /// </summary>
        /// <returns>
        ///   The time-weighted average, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to current time.
        /// </returns>
        double TimeVariance();

        /// <summary>
        ///   Returns the time-weighted variance, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to <paramref name="time"/>.
        /// </summary>
        /// <returns>
        ///   The time-weighted average, calculated from time 0
        ///   (or the last time <see cref="Reset()"/> was called) to <paramref name="time"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   <paramref name="time"/> is less than <see cref="StartTime"/>.
        /// </exception>
        double TimeVariance(double time);

        /// <summary>
        ///   Returns the sum of the observed values.
        /// </summary>
        /// <returns>The sum of the observed values.</returns>
        double Total();

        /// <summary>
        ///   Returns the sample variance of the observations, ignoring the times at which they were made. 
        ///   If an unbiased estimate of the population variance is desired, the sample variance
        ///   should be multiplied by <code>Count/(Count - 1)</code>.
        /// </summary>
        /// <returns>The sample variance of the observations, ignoring the times at which they were made.</returns>
        /// <exception cref="InvalidOperationException">There are no observations.</exception>
        double Variance();
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IRecordedResource
    {
        /// <summary>
        ///   An instance of <see cref="Tally"/> that periodically records the number of requests
        ///   fulfilled by this resource. The frequency of recordings is given by <see cref="RecordingFrequency"/>.
        /// </summary>
        Tally FulfilledRequestsTally { get; }

        /// <summary>
        ///   The frequency at which some tallies of this interface update themselves.
        /// </summary>
        double RecordingFrequency { get; }

        /// <summary>
        ///   An instance of <see cref="Tally"/> that periodically records the number of requests undone.
        ///   The frequency of recordings is given by <see cref="RecordingFrequency"/>.
        /// </summary>
        Tally UndoneRequestsTally { get; }

        /// <summary>
        ///   An instance of <see cref="Tally"/> that periodically records the number
        ///   of the users of this resource (given by <see cref="Resource.Count"/>).
        ///   The frequency of recordings is given by <see cref="RecordingFrequency"/>.
        /// </summary>
        Tally UsageTally { get; }

        /// <summary>
        ///   An instance of <see cref="Tally"/> that records the time waited by each process.
        /// </summary>
        Tally WaitingTimeTally { get; }
    }
}
