﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using PommaLabs.Dessert.Core;

namespace PommaLabs.Dessert.Recording
{
    /// <summary>
    ///   An instance of this interface preserves a complete time-series of the observed data values, sample,
    ///   and their associated times, time. It calculates the data summaries using these series only when they are needed.
    ///   It is slower and uses more memory than <see cref="Tally"/>. In long simulations its memory demands may be a disadvantage.
    /// </summary>
    /// <remarks>
    ///   Monitors and tallies may not be bound to a specific <see cref="SimEnvironment"/>, in order to ease
    ///   their usage in inter environment recordings; when they are unbound their <see cref="SimEntity.Env"/>
    ///   property points to a dummy environment.<br/>
    ///   However, please pay attention to the fact that both monitors and tallies are not thread safe:
    ///   therefore, recall this fact when you use them in a multi threaded simulation scenario. 
    /// </remarks>
    public sealed class Monitor : SimEntity, IRecorder
    {
        private readonly IList<MonitorSample> _samples = new List<MonitorSample>();

        internal Monitor(SimEnvironment env) : base(env)
        {
            StartTime = env.Now;
        }

        private void DoObserve(double sample, double time)
        {
            _samples.Add(new MonitorSample(sample, time));
        }

        private void DoReset(double time)
        {
            _samples.Clear();
            StartTime = time;
        }

        #region IRecorder Members

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<MonitorSample> Samples
        {
            get { return _samples; }
        }

        /// <summary>
        ///   Returns the i-th sample recorded inside the monitor. 
        /// </summary>
        /// <param name="i">The index of the sample that has to be retrieved.</param>
        /// <returns>The i-th sample recorded inside the monitor.</returns>
        public MonitorSample this[int i]
        {
            get { return _samples[i]; }
        }

        public int Count
        {
            get { return _samples.Count; }
        }

        public double LastTime
        {
            get { return (_samples.Count > 0) ? _samples[_samples.Count - 1].Time : StartTime; }
        }

        public double StartTime { get; private set; }

        public double Mean()
        {
            return Total() / Count;
        }

        public void Observe(double sample)
        {
            DoObserve(sample, Env.Now);
        }

        public void Observe(double sample, double time)
        {
            // Preconditions
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            DoObserve(sample, time);
        }

        public void Reset()
        {
            DoReset(Env.Now);
        }

        public void Reset(double time)
        {
            DoReset(time);
        }

        public double StdDev()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return Math.Sqrt(Variance());
        }

        public double TimeMean()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return TimeMean(Env.Now);
        }

        public double TimeMean(double time)
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            var sum = 0.0;
            var last = _samples[0];
            foreach (var s in _samples)
            {
                sum += last.Sample * (s.Time - last.Time);
                last = s;
            }
            sum += last.Sample * (time - last.Time);
            return sum / (time - _samples[0].Time);
        }

        public double TimeStdDev()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return TimeStdDev(Env.Now);
        }

        public double TimeStdDev(double time)
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            return Math.Sqrt(TimeVariance(time));
        }

        public double TimeVariance()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return TimeVariance(Env.Now);
        }

        public double TimeVariance(double time)
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);
            if (time < LastTime) throw new InvalidOperationException(ErrorMessages.InvalidRecordingTime);

            var sum = 0.0;
            var sumOfSquares = 0.0;
            var last = _samples[0];
            foreach (var s in _samples)
            {
                sum += last.Sample * (s.Time - last.Time);
                sumOfSquares += last.Sample * last.Sample * (s.Time - last.Time);
                last = s;
            }
            sum += last.Sample * (time - last.Time);
            sumOfSquares += last.Sample * last.Sample * (time - last.Time);
            var dt = time - _samples[0].Time;
            var mean = sum / dt;
            return sumOfSquares / dt - mean * mean;
        }

        public double Total()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            return _samples.Sum(s => s.Sample);
        }

        public double Variance()
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoObservations);

            var sum = 0.0;
            var sumOfSquares = 0.0;
            foreach (var s in _samples)
            {
                sum += s.Sample;
                sumOfSquares += s.Sample * s.Sample;
            }
            return (sumOfSquares - (sum * sum) / Count) / Count;
        }

        #endregion
    }

    /// <summary>
    ///   Represents a sample recorded inside a <see cref="Monitor"/>.
    /// </summary>
    public struct MonitorSample
    {
        private readonly double _sample;
        private readonly double _time;

        internal MonitorSample(double sample, double time)
        {
            _sample = sample;
            _time = time;
        }

        /// <summary>
        ///   The sample recorded in the monitor.
        /// </summary>
        public double Sample
        {
            get { return _sample; }
        }

        /// <summary>
        ///   The time at which <see cref="Sample"/> was recorded.
        /// </summary>
        public double Time
        {
            get { return _time; }
        }
    }
}
