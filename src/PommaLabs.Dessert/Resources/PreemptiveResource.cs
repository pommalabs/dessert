﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Events;
using PommaLabs.Hippie;

namespace PommaLabs.Dessert.Resources
{
    public sealed class PreemptiveResource : SimEntity
    {
        private readonly int _capacity;

        /// <summary>
        ///   Stores the requests waiting for this resource.
        /// </summary>
        private readonly ArrayHeap<RequestEvent, ReqPriority> _requestQueue;

        /// <summary>
        ///   Stores the users which own this resource.
        /// </summary>
        private readonly ArrayHeap<RequestEvent, ReqPriority> _users;
        private ulong _nextVersion;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="capacity"></param>
        internal PreemptiveResource(SimEnvironment env, int capacity) : base(env)
        {
            _capacity = capacity;
            _requestQueue = HeapFactory.NewRawBinaryHeap<RequestEvent, ReqPriority>(ReqPriority.Comparer);
            _users = HeapFactory.NewRawBinaryHeap<RequestEvent, ReqPriority>(ReqPriority.ReverseComparer);
        }

        private void BalanceQueues()
        {
            if (_requestQueue.Count > 0 && _requestQueue.Min.Value.TrySchedule())
            {
                _requestQueue.RemoveMin();
            }
        }

        #region IPreemptiveResource Members

        public int Capacity
        {
            get { return _capacity; }
        }

        public int Count
        {
            get { return _users.Count; }
        }

        public WaitPolicy RequestPolicy
        {
            get { return WaitPolicy.Priority; }
        }

        public IEnumerable<RequestEvent> RequestQueue
        {
            get
            {
                foreach (var req in _requestQueue)
                {
                    yield return req.Value;
                }
            }
        }

        public IEnumerable<RequestEvent> Users
        {
            get
            {
                foreach (var user in _users)
                {
                    yield return user.Value;
                }
            }
        }

        public ReleaseEvent Release(RequestEvent request)
        {
            // Preconditions
            if (request == null) throw new ArgumentNullException(nameof(request), ErrorMessages.NullRequest);
            if (!ReferenceEquals(this, request.Resource)) throw new ArgumentException(ErrorMessages.DifferentResource);

            return new ReleaseEvent(this, request);
        }

        public RequestEvent Request()
        {
            return new RequestEvent(this, Default.Priority, Default.Preempt);
        }

        public RequestEvent Request(double priority)
        {
            return new RequestEvent(this, priority, Default.Preempt);
        }

        public RequestEvent Request(double priority, bool preempt)
        {
            return new RequestEvent(this, priority, preempt);
        }

        #endregion

        private sealed class ReqPriority
        {
            public static readonly IComparer<ReqPriority> Comparer = new CustomComparer();
            public static readonly IComparer<ReqPriority> ReverseComparer = new ReverseCustomComparer();

            public readonly bool Preempt;
            public readonly double Priority;
            public readonly double Time;
            private readonly ulong _version;

            public ReqPriority(double priority, double time, bool preempt, ulong version)
            {
                Priority = priority;
                Time = time;
                Preempt = preempt;
                _version = version;
            }

            private sealed class CustomComparer : IComparer<ReqPriority>
            {
                public int Compare(ReqPriority x, ReqPriority y)
                {
                    var prCmp = x.Priority.CompareTo(y.Priority);
                    if (prCmp != 0)
                    {
                        return prCmp;
                    }
                    var timeCmp = x.Time.CompareTo(y.Time);
                    if (timeCmp != 0)
                    {
                        return timeCmp;
                    }
                    // A true preempt has a greater priority than a false one.
                    var preemptCmp = y.Preempt.CompareTo(x.Preempt);
                    return (preemptCmp != 0) ? preemptCmp : x._version.CompareTo(y._version);
                }
            }

            private sealed class ReverseCustomComparer : IComparer<ReqPriority>
            {
                public int Compare(ReqPriority x, ReqPriority y)
                {
                    return -1 * Comparer.Compare(x, y);
                }
            }
        }

        public sealed class RequestEvent : ResourceEvent<RequestEvent, object>
        {
            private readonly PreemptiveResource _resource;
            private readonly ReqPriority _priority;
            private readonly SimProcess _process;
            private IHeapHandle<RequestEvent, ReqPriority> _handle;

            internal RequestEvent(PreemptiveResource resource, double priority, bool preempt)
                : base(resource.Env, priority)
            {
                _resource = resource;
                _priority = new ReqPriority(priority, Env.Now, preempt, resource._nextVersion++);
                _process = Env.ActiveProcess;

                if (_resource._requestQueue.Count == 0 && TrySchedule())
                {
                    return;
                }
                _handle = _resource._requestQueue.Add(this, _priority);
                if (preempt && _resource._requestQueue.Min.Value.Equals(this))
                {
                    var toPreempt = _resource._users.Min.Value;
                    if (ReqPriority.Comparer.Compare(toPreempt._priority, _priority) <= 0)
                    {
                        return;
                    }
                    toPreempt.Dispose();
                    toPreempt._process.Interrupt(new PreemptionInfo(_process, toPreempt.Time));
                }
            }

            internal bool TrySchedule()
            {
                if (_resource._users.Count < _resource.Capacity)
                {
                    _handle = _resource._users.Add(this, _priority);
                    Env.ScheduleEvent(this);
                    return true;
                }
                return false;
            }

            #region Public Members

            public bool Preempt
            {
                get { return _priority.Preempt; }
            }

            public PreemptiveResource Resource
            {
                get { return _resource; }
            }

            public double Time
            {
                get { return _priority.Time; }
            }

            public override void Dispose()
            {
                if (Disposed)
                {
                    return;
                }
                if (Succeeded || !_resource._requestQueue.Contains(_handle))
                {
                    _resource._users.Remove(_handle);
                }
                else
                {
                    _resource._requestQueue.Remove(_handle);
                }
                _resource.BalanceQueues();
                // Marks event as disposed, so that the request event
                // cannot be disposed two or more times.
                Disposed = true;
                Debug.Assert(!_resource._users.Contains(_handle));
                Debug.Assert(!_resource._requestQueue.Contains(_handle));
            }

            #endregion

            #region SimEvent Members

            public override object Value
            {
                get { return Default.NoValue; }
            }

            #endregion
        }

        public sealed class ReleaseEvent : ResourceEvent<ReleaseEvent, object>
        {
            private readonly RequestEvent _request;

            internal ReleaseEvent(PreemptiveResource resource, RequestEvent request)
                : base(resource.Env, Default.Priority)
            {
                _request = request;
                Env.ScheduleEvent(this);
            }

            #region Public Members

            public RequestEvent Request
            {
                get { return _request; }
            }

            public override void Dispose()
            {
                // Do nothing...
            }

            #endregion

            #region SimEvent Members

            public override object Value
            {
                get { return Default.NoValue; }
            }

            protected override void OnEnd()
            {
                _request.Dispose();
            }

            #endregion
        }
    }
}
