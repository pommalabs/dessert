﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Resources
{
    public sealed class Store<T> : SimEntity
    {
        private readonly int _capacity;
        private readonly IWaitQueue<GetEvent> _getQueue;
        private readonly IWaitQueue<T> _itemQueue;
        private readonly IWaitQueue<PutEvent> _putQueue;

        internal Store(SimEnvironment env, int capacity, WaitPolicy getPolicy, WaitPolicy putPolicy,
                       WaitPolicy itemPolicy) : base(env)
        {
            _capacity = capacity;
            _getQueue = WaitQueue.New<GetEvent>(getPolicy, Env);
            _putQueue = WaitQueue.New<PutEvent>(putPolicy, Env);
            _itemQueue = WaitQueue.New<T>(itemPolicy, Env);
        }

        private void BalanceQueues()
        {
            if (_getQueue.Count > 0 && _getQueue.First.TrySchedule())
            {
                _getQueue.RemoveFirst();
            }
            if (_putQueue.Count > 0 && _putQueue.First.TrySchedule())
            {
                _putQueue.RemoveFirst();
            }
        }

        #region Public Members

        public int Capacity
        {
            get { return _capacity; }
        }

        public int Count
        {
            get { return _itemQueue.Count; }
        }

        public WaitPolicy GetPolicy
        {
            get { return _getQueue.Policy; }
        }

        public IEnumerable<GetEvent> GetQueue
        {
            get { return _getQueue; }
        }

        public WaitPolicy ItemPolicy
        {
            get { return _itemQueue.Policy; }
        }

        public IEnumerable<T> ItemQueue
        {
            get { return _itemQueue; }
        }

        public WaitPolicy PutPolicy
        {
            get { return _putQueue.Policy; }
        }

        public IEnumerable<PutEvent> PutQueue
        {
            get { return _putQueue; }
        }

        public GetEvent Get()
        {
            return new GetEvent(this, Default.Priority);
        }

        public GetEvent Get(double priority)
        {
            return new GetEvent(this, priority);
        }

        public PutEvent Put(T item)
        {
            return new PutEvent(this, item, Default.Priority, Default.Priority);
        }

        public PutEvent Put(T item, double putPriority)
        {
            return new PutEvent(this, item, putPriority, Default.Priority);
        }

        public PutEvent Put(T item, double putPriority, double itemPriority)
        {
            return new PutEvent(this, item, putPriority, itemPriority);
        }

        #endregion

        public sealed class GetEvent : ResourceEvent<GetEvent, T>
        {
            private readonly Store<T> _store;
            private T _item;

            internal GetEvent(Store<T> store, double getPriority) : base(store.Env, getPriority)
            {
                _store = store;
                if (_store._getQueue.Count > 0 || !TrySchedule())
                {
                    _store._getQueue.Add(this, getPriority);
                }
            }

            internal bool TrySchedule()
            {
                if (_store.Count > 0)
                {
                    _item = _store._itemQueue.RemoveFirst();
                    Env.ScheduleEvent(this);
                    return true;
                }
                return false;
            }

            #region Public Members

            public override void Dispose()
            {
                if (Disposed)
                {
                    // Nothing to do, event has already been disposed.
                    return;
                }
                // If event has not succeeded, then it means that
                // it may still be in the queue, from which it has to removed.
                if (!Succeeded && _store._getQueue.Contains(this))
                {
                    _store._getQueue.Remove(this);
                    _store.BalanceQueues();
                }
                // Marks token as disposed, so that the user and the system
                // can recognize the fact that this token cannot be used anymore.
                Disposed = true;
            }

            #endregion

            #region SimEvent Members

            public override T Value
            {
                get { return _item; }
            }

            protected override void OnEnd()
            {
                _store.BalanceQueues();
            }

            #endregion
        }

        public sealed class PutEvent : ResourceEvent<PutEvent, T>
        {
            private readonly T _item;
            private readonly double _itemPriority;
            private readonly Store<T> _store;

            internal PutEvent(Store<T> store, T item, double putPriority, double itemPriority)
                : base(store.Env, putPriority)
            {
                _store = store;
                _item = item;
                _itemPriority = itemPriority;
                if (_store._putQueue.Count > 0 || !TrySchedule())
                {
                    _store._putQueue.Add(this, putPriority);
                }
            }

            internal bool TrySchedule()
            {
                if (_store.Count < _store.Capacity)
                {
                    _store._itemQueue.Add(_item, _itemPriority);
                    Env.ScheduleEvent(this);
                    return true;
                }
                return false;
            }

            #region Public Members

            public double ItemPriority
            {
                get { return _itemPriority; }
            }

            public override void Dispose()
            {
                if (Disposed)
                {
                    // Nothing to do, event has already been disposed.
                    return;
                }
                // If event has not succeeded, then it means that
                // it may still be in the queue, from which it has to removed.
                if (!Succeeded && _store._putQueue.Contains(this))
                {
                    _store._putQueue.Remove(this);
                    _store.BalanceQueues();
                }
                // Marks token as disposed, so that the user and the system
                // can recognize the fact that this token cannot be used anymore.
                Disposed = true;
            }

            #endregion

            #region SimEvent Members

            public override T Value
            {
                get { return _item; }
            }

            protected override void OnEnd()
            {
                _store.BalanceQueues();
            }

            #endregion
        }
    }
}
