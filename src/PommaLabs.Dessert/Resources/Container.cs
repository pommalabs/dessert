﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Resources
{
    public sealed class Container : SimEntity
    {
        private readonly double _capacity;
        private readonly IWaitQueue<GetEvent> _getQueue;
        private readonly IWaitQueue<PutEvent> _putQueue;

        internal Container(SimEnvironment env, double capacity, double level, WaitPolicy getPolicy, WaitPolicy putPolicy)
            : base(env)
        {
            _capacity = capacity;
            Level = level;
            _getQueue = WaitQueue.New<GetEvent>(getPolicy, Env);
            _putQueue = WaitQueue.New<PutEvent>(putPolicy, Env);
        }

        private void BalanceQueues()
        {
            if (_getQueue.Count > 0 && _getQueue.First.TrySchedule())
            {
                _getQueue.RemoveFirst();
            }
            if (_putQueue.Count > 0 && _putQueue.First.TrySchedule())
            {
                _putQueue.RemoveFirst();
            }
        }

        #region Public Members

        public double Capacity
        {
            get { return _capacity; }
        }

        public WaitPolicy GetPolicy
        {
            get { return _getQueue.Policy; }
        }

        public IEnumerable<GetEvent> GetQueue
        {
            get { return _getQueue; }
        }

        public double Level { get; private set; }

        public WaitPolicy PutPolicy
        {
            get { return _putQueue.Policy; }
        }

        public IEnumerable<PutEvent> PutQueue
        {
            get { return _putQueue; }
        }

        public GetEvent Get(double quantity)
        {
            if (quantity < 0) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.NegativeQuantity);
            if (quantity > Capacity) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.ExcessiveQuantity);
            return new GetEvent(this, quantity, Default.Priority);
        }

        public GetEvent Get(double quantity, double priority)
        {
            if (quantity < 0) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.NegativeQuantity);
            if (quantity > Capacity) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.ExcessiveQuantity);
            return new GetEvent(this, quantity, priority);
        }

        public PutEvent Put(double quantity)
        {
            if (quantity < 0) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.NegativeQuantity);
            if (quantity > Capacity) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.ExcessiveQuantity);
            return new PutEvent(this, quantity, Default.Priority);
        }

        public PutEvent Put(double quantity, double priority)
        {
            if (quantity < 0) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.NegativeQuantity);
            if (quantity > Capacity) throw new ArgumentOutOfRangeException(nameof(quantity), ErrorMessages.ExcessiveQuantity);
            return new PutEvent(this, quantity, priority);
        }

        #endregion

        public sealed class GetEvent : ResourceEvent<GetEvent, double>
        {
            private readonly Container _container;
            private readonly double _quantity;

            internal GetEvent(Container container, double quantity, double priority) : base(container.Env, priority)
            {
                _container = container;
                _quantity = quantity;
                if (_container._getQueue.Count > 0 || !TrySchedule())
                {
                    _container._getQueue.Add(this, priority);
                }
            }

            internal bool TrySchedule()
            {
                if (_container.Level - _quantity >= 0)
                {
                    _container.Level -= _quantity;
                    Env.ScheduleEvent(this);
                    return true;
                }
                return false;
            }

            #region Public Members

            public override void Dispose()
            {
                if (Disposed)
                {
                    // Nothing to do, event has already been disposed.
                    return;
                }
                // If event has not succeeded, then it means that
                // it may still be in the queue, from which it has to removed.
                if (!Succeeded && _container._getQueue.Contains(this))
                {
                    _container._getQueue.Remove(this);
                    _container.BalanceQueues();
                }
                // Marks token as disposed, so that the user and the system
                // can recognize the fact that this token cannot be used anymore.
                Disposed = true;
            }

            #endregion

            #region SimEvent Members

            /// <summary>
            ///   QUANTITY
            /// </summary>
            public override double Value
            {
                get { return _quantity; }
            }

            protected override void OnEnd()
            {
                _container.BalanceQueues();
            }

            #endregion
        }

        public sealed class PutEvent : ResourceEvent<PutEvent, double>
        {
            private readonly Container _container;
            private readonly double _quantity;

            internal PutEvent(Container container, double quantity, double priority) : base(container.Env, priority)
            {
                _container = container;
                _quantity = quantity;
                if (_container._putQueue.Count > 0 || !TrySchedule())
                {
                    _container._putQueue.Add(this, priority);
                }
            }

            internal bool TrySchedule()
            {
                if (_container.Level + _quantity <= _container.Capacity)
                {
                    _container.Level += _quantity;
                    Env.ScheduleEvent(this);
                    return true;
                }
                return false;
            }

            #region Public Members

            public override void Dispose()
            {
                if (Disposed)
                {
                    // Nothing to do, event has already been disposed.
                    return;
                }
                // If event has not succeeded, then it means that
                // it may still be in the queue, from which it has to removed.
                if (!Succeeded && _container._putQueue.Contains(this))
                {
                    _container._putQueue.Remove(this);
                    _container.BalanceQueues();
                }
                // Marks token as disposed, so that the user and the system
                // can recognize the fact that this token cannot be used anymore.
                Disposed = true;
            }

            #endregion

            #region SimEvent Members

            /// <summary>
            ///   QUANTITY
            /// </summary>
            public override double Value
            {
                get { return _quantity; }
            }

            protected override void OnEnd()
            {
                _container.BalanceQueues();
            }

            #endregion
        }
    }
}
