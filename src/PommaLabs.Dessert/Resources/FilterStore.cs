﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Resources
{
    public sealed class FilterStore<T> : SimEntity
    {
        private readonly int _capacity;
        private readonly IWaitQueue<GetEvent> _getQueue;
        private readonly IWaitQueue<T> _itemQueue;
        private readonly IWaitQueue<PutEvent> _putQueue;

        internal FilterStore(SimEnvironment env, int capacity, WaitPolicy getPolicy, WaitPolicy putPolicy,
                             WaitPolicy itemPolicy) : base(env)
        {
            _capacity = capacity;
            _getQueue = WaitQueue.New<GetEvent>(getPolicy, Env);
            _putQueue = WaitQueue.New<PutEvent>(putPolicy, Env);
            _itemQueue = WaitQueue.New<T>(itemPolicy, Env);
        }

        private void BalanceQueues()
        {
            if (_getQueue.Count > 0 && _getQueue.First.TrySchedule())
            {
                _getQueue.RemoveFirst();
            }
            if (_putQueue.Count > 0 && _putQueue.First.TrySchedule())
            {
                _putQueue.RemoveFirst();
            }
        }

        #region Public Members

        public int Capacity
        {
            get { return _capacity; }
        }

        public int Count
        {
            get { return _itemQueue.Count; }
        }

        public WaitPolicy GetPolicy
        {
            get { return _getQueue.Policy; }
        }

        public IEnumerable<GetEvent> GetQueue
        {
            get { return _getQueue; }
        }

        public WaitPolicy ItemPolicy
        {
            get { return _itemQueue.Policy; }
        }

        public IEnumerable<T> ItemQueue
        {
            get { return _itemQueue; }
        }

        public WaitPolicy PutPolicy
        {
            get { return _putQueue.Policy; }
        }

        public IEnumerable<PutEvent> PutQueue
        {
            get { return _putQueue; }
        }

        public GetEvent Get()
        {
            return new GetEvent(this, item => true, Default.Priority);
        }

        public GetEvent Get(double priority)
        {
            return new GetEvent(this, item => true, priority);
        }

        public GetEvent Get(Predicate<T> filter)
        {
            return new GetEvent(this, filter, Default.Priority);
        }

        public GetEvent Get(Predicate<T> filter, double priority)
        {
            return new GetEvent(this, filter, priority);
        }

        public PutEvent Put(T item)
        {
            return new PutEvent(this, item, Default.Priority, Default.Priority);
        }

        public PutEvent Put(T item, double putPriority)
        {
            return new PutEvent(this, item, putPriority, Default.Priority);
        }

        public PutEvent Put(T item, double putPriority, double itemPriority)
        {
            return new PutEvent(this, item, putPriority, itemPriority);
        }

        #endregion

        public sealed class GetEvent : ResourceEvent<GetEvent, T>
        {
            private readonly Predicate<T> _filter;
            private readonly FilterStore<T> _filterStore;
            internal T Item;

            internal GetEvent(FilterStore<T> filterStore, Predicate<T> filter, double getPriority)
                : base(filterStore.Env, getPriority)
            {
                _filterStore = filterStore;
                _filter = filter;
                if (!TrySchedule())
                {
                    _filterStore._getQueue.Add(this, getPriority);
                }
            }

            internal bool TrySchedule()
            {
                foreach (var item in _filterStore._itemQueue)
                {
                    if (_filter(item))
                    {
                        _filterStore._itemQueue.Remove(item);
                        Item = item;
                        Env.ScheduleEvent(this);
                        return true;
                    }
                }
                return false;
            }

            #region Public Members

            public Predicate<T> Filter
            {
                get { return _filter; }
            }

            public override void Dispose()
            {
                if (Disposed)
                {
                    // Nothing to do, event has already been disposed.
                    return;
                }
                // If event has not succeeded, then it means that
                // it may still be in the queue, from which it has to removed.
                if (!Succeeded && _filterStore._getQueue.Contains(this))
                {
                    _filterStore._getQueue.Remove(this);
                    _filterStore.BalanceQueues();
                }
                // Marks token as disposed, so that the user and the system
                // can recognize the fact that this token cannot be used anymore.
                Disposed = true;
            }

            #endregion

            #region SimEvent Members

            public override T Value
            {
                get { return Item; }
            }

            protected override void OnEnd()
            {
                _filterStore.BalanceQueues();
            }

            #endregion
        }

        public sealed class PutEvent : ResourceEvent<PutEvent, T>
        {
            private readonly FilterStore<T> _filterStore;
            private readonly T _item;
            private readonly double _itemPriority;

            internal PutEvent(FilterStore<T> filterStore, T item, double putPriority, double itemPriority)
                : base(filterStore.Env, putPriority)
            {
                _filterStore = filterStore;
                _item = item;
                _itemPriority = itemPriority;
                if (!TrySchedule())
                {
                    _filterStore._putQueue.Add(this, putPriority);
                }
            }

            internal bool TrySchedule()
            {
                if (_filterStore.Count >= _filterStore.Capacity)
                {
                    return false;
                }
                Env.ScheduleEvent(this);
                foreach (var getEv in _filterStore._getQueue)
                {
                    if (!getEv.Filter(_item))
                    {
                        continue;
                    }
                    _filterStore._getQueue.Remove(getEv);
                    getEv.Item = _item;
                    Env.ScheduleEvent(getEv);
                    return true;
                }
                _filterStore._itemQueue.Add(_item, _itemPriority);
                return true;
            }

            #region Public Members

            public T Item
            {
                get { return _item; }
            }

            public double ItemPriority
            {
                get { return _itemPriority; }
            }

            public override void Dispose()
            {
                if (Disposed)
                {
                    // Nothing to do, event has already been disposed.
                    return;
                }
                // If event has not succeeded, then it means that
                // it may still be in the queue, from which it has to removed.
                if (!Succeeded && _filterStore._putQueue.Contains(this))
                {
                    _filterStore._putQueue.Remove(this);
                    _filterStore.BalanceQueues();
                }
                // Marks token as disposed, so that the user and the system
                // can recognize the fact that this token cannot be used anymore.
                Disposed = true;
            }

            #endregion

            #region SimEvent Members

            public override T Value
            {
                get { return Item; }
            }

            protected override void OnEnd()
            {
                _filterStore.BalanceQueues();
            }

            #endregion
        }
    }
}
