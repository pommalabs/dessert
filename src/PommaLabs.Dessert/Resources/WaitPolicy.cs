﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Resources
{
    public enum WaitPolicy : byte
    {
        FIFO,
        LIFO,
        Priority,
        Random
    }
}
