﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Events;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Dessert.Resources
{
    public sealed class Resource : SimEntity
    {
        private readonly int _capacity;
        private readonly IWaitQueue<RequestEvent> _requestQueue;

        /// <summary>
        ///   Stores the users which own this resource.
        /// </summary>
        private readonly ThinLinkedList<RequestEvent> _users = new ThinLinkedList<RequestEvent>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="capacity"></param>
        /// <param name="requestPolicy"></param>
        internal Resource(SimEnvironment env, int capacity, WaitPolicy requestPolicy) : base(env)
        {
            _capacity = capacity;
            _requestQueue = WaitQueue.New<RequestEvent>(requestPolicy, Env);
        }

        private void BalanceQueues()
        {
            if (_requestQueue.Count > 0 && _requestQueue.First.TrySchedule())
            {
                _requestQueue.RemoveFirst();
            }
        }

        #region IResource Members

        public int Capacity
        {
            get { return _capacity; }
        }

        public int Count
        {
            get { return _users.Count; }
        }

        public WaitPolicy RequestPolicy
        {
            get { return _requestQueue.Policy; }
        }

        public IEnumerable<RequestEvent> RequestQueue
        {
            get { return _requestQueue; }
        }

        public IEnumerable<RequestEvent> Users
        {
            get { return _users; }
        }

        public ReleaseEvent Release(RequestEvent request)
        {
            // Preconditions
            if (request == null) throw new ArgumentNullException(nameof(request), ErrorMessages.NullRequest);
            if (!ReferenceEquals(this, request.Resource)) throw new ArgumentException(ErrorMessages.DifferentResource);

            return new ReleaseEvent(this, request);
        }

        public RequestEvent Request()
        {
            return new RequestEvent(this, Default.Priority);
        }

        public RequestEvent Request(double priority)
        {
            return new RequestEvent(this, priority);
        }

        #endregion

        public sealed class ReleaseEvent : ResourceEvent<ReleaseEvent, object>
        {
            private readonly RequestEvent _request;

            internal ReleaseEvent(Resource resource, RequestEvent request) : base(resource.Env, Default.Priority)
            {
                _request = request;
                _request.Dispose();
                Env.ScheduleEvent(this);
            }

            #region Public Members

            public RequestEvent Request
            {
                get { return _request; }
            }

            public override void Dispose()
            {
                // Do nothing...
            }

            #endregion

            #region SimEvent Members

            public override object Value
            {
                get { return Default.NoValue; }
            }

            #endregion
        }

        public sealed class RequestEvent : ResourceEvent<RequestEvent, object>
        {
            private readonly Resource _resource;

            internal RequestEvent(Resource resource, double priority) : base(resource.Env, priority)
            {
                _resource = resource;
                if (_resource._requestQueue.Count > 0 || !TrySchedule())
                {
                    _resource._requestQueue.Add(this, priority);
                }
            }

            internal bool TrySchedule()
            {
                if (_resource._users.Count < _resource.Capacity)
                {
                    _resource._users.Add(this);
                    Env.ScheduleEvent(this);
                    return true;
                }
                return false;
            }

            #region Public Members

            public Resource Resource
            {
                get { return _resource; }
            }

            public override void Dispose()
            {
                if (Disposed)
                {
                    return;
                }
                if (Succeeded || !_resource._requestQueue.Contains(this))
                {
                    Debug.Assert(_resource._users.Contains(this));
                    _resource._users.Remove(this);
                }
                else
                {
                    Debug.Assert(_resource._requestQueue.Contains(this));
                    _resource._requestQueue.Remove(this);
                }
                _resource.BalanceQueues();
                // Marks event as disposed, so that the request event
                // cannot be disposed two or more times.
                Disposed = true;
                Debug.Assert(!_resource._users.Contains(this));
                Debug.Assert(!_resource._requestQueue.Contains(this));
            }

            #endregion

            #region SimEvent Members

            public override object Value
            {
                get { return Default.NoValue; }
            }

            #endregion
        }
    }
}
