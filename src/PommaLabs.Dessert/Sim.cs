﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Events;
using PommaLabs.Dessert.Recording;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert
{
    public static partial class Sim
    {
        private static readonly Dictionary<TimeUnit, double> SecondToUnit = new Dictionary<TimeUnit, double> {
            {TimeUnit.Nanosecond, 0.000000001},
            {TimeUnit.Microsecond, 0.000001},
            {TimeUnit.Millisecond, 0.001},
            {TimeUnit.Second, 1},
            {TimeUnit.Minute, 60},
            {TimeUnit.Hour, 3600},
            {TimeUnit.Day, 60*60*24}
        };

        /// <summary>
        ///   Stores a reference to a dummy environment used by unbound instances of <see
        ///   cref="Recording.Monitor"/> and <see cref="Recording.Tally"/>.
        /// </summary>
        private static readonly SimEnvironment DummyEnv = new SimEnvironment(0);

        #region Container Construction

        public static Container Container(SimEnvironment env)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);

            return new Container(env, Default.Capacity, Default.Level, Default.Policy, Default.Policy);
        }

        public static Container Container(SimEnvironment env, double capacity)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));

            return new Container(env, capacity, Default.Level, Default.Policy, Default.Policy);
        }

        public static Container Container(SimEnvironment env, double capacity, double level)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (level < 0 || level > capacity) throw new ArgumentOutOfRangeException(nameof(level));

            return new Container(env, capacity, level, Default.Policy, Default.Policy);
        }

        public static Container Container(SimEnvironment env, double capacity, double level, WaitPolicy getPolicy, WaitPolicy putPolicy)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (level < 0 || level > capacity) throw new ArgumentOutOfRangeException(nameof(level));
            if (!Enum.IsDefined(typeof(WaitPolicy), getPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), putPolicy)) throw new ArgumentException();

            return new Container(env, capacity, level, getPolicy, putPolicy);
        }

        #endregion Container Construction

        #region Environment Construction

        /// <summary>
        ///   Creates a new environment.
        /// </summary>
        /// <returns>A new simulation environment.</returns>
        public static SimEnvironment Environment() => Environment(System.Environment.TickCount);

        /// <summary>
        ///   Creates a new environment with a custom seed.
        /// </summary>
        /// <param name="seed">The seed used by the exposed random generator.</param>
        /// <returns>A new simulation environment.</returns>
        public static SimEnvironment Environment(int seed)
        {
            var env = new SimEnvironment(seed);
            env.RealTime.Locked = true;
            Debug.Assert(env.Now.Equals(0));
            Debug.Assert(env.Random.Seed == seed);
            lock (SuspendInfo)
            {
                SuspendInfo[env] = new Dictionary<SimProcess, SimEvent<object>>();
            }
            return env;
        }

        /// <summary>
        ///   Creates a new real-time environment with the default options.
        /// </summary>
        /// <returns>A new real-time simulation environment.</returns>
        public static SimEnvironment RealTimeEnvironment()
        {
            var env = Environment(System.Environment.TickCount);
            env.RealTime.Enabled = true;
            return env;
        }

        /// <summary>
        ///   Creates a new real-time environment with a custom seed and the default options.
        /// </summary>
        /// <param name="seed">The seed used by the exposed random generator.</param>
        /// <returns>A new real-time simulation environment.</returns>
        public static SimEnvironment RealTimeEnvironment(int seed)
        {
            var env = Environment(seed);
            env.RealTime.Enabled = true;
            return env;
        }

        /// <summary>
        ///   Creates a new real-time environment with custom options.
        /// </summary>
        /// <param name="realTimeOptions">The custom real-time options.</param>
        /// <returns>A new real-time simulation environment.</returns>
        /// <exception cref="ArgumentNullException">
        ///   <paramref name="realTimeOptions"/> is null, or the specified "wall clock" is null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   The specified scaling factor is too small (less than <see cref="SimEnvironment.RealTimeOptions.MinScalingFactor"/>).
        /// </exception>
        public static SimEnvironment RealTimeEnvironment(SimEnvironment.RealTimeOptions realTimeOptions)
        {
            // Preconditions
            if (realTimeOptions == null) throw new ArgumentNullException(nameof(realTimeOptions));
            if (realTimeOptions.ScalingFactor <= SimEnvironment.RealTimeOptions.MinScalingFactor) throw new ArgumentOutOfRangeException(nameof(realTimeOptions.ScalingFactor));

            var env = Environment(System.Environment.TickCount);
            env.RealTime.Enabled = true;
            env.RealTime.Locked = false;
            env.RealTime.WallClock = realTimeOptions.WallClock ?? throw new ArgumentNullException(nameof(realTimeOptions.WallClock));
            env.RealTime.ScalingFactor = realTimeOptions.ScalingFactor;
            env.RealTime.Locked = true;
            return env;
        }

        /// <summary>
        ///   Creates a new real-time environment with a custom seed and custom options.
        /// </summary>
        /// <param name="seed">The seed used by the exposed random generator.</param>
        /// <param name="realTimeOptions">The custom real-time options.</param>
        /// <returns>A new real-time simulation environment.</returns>
        /// <exception cref="ArgumentNullException">
        ///   <paramref name="realTimeOptions"/> is null, or the specified "wall clock" is null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   The specified scaling factor is too small (less than <see cref="SimEnvironment.RealTimeOptions.MinScalingFactor"/>).
        /// </exception>
        public static SimEnvironment RealTimeEnvironment(int seed, SimEnvironment.RealTimeOptions realTimeOptions)
        {
            // Preconditions
            if (realTimeOptions == null) throw new ArgumentNullException(nameof(realTimeOptions));
            if (realTimeOptions.ScalingFactor <= SimEnvironment.RealTimeOptions.MinScalingFactor) throw new ArgumentOutOfRangeException(nameof(realTimeOptions.ScalingFactor));

            var env = Environment(seed);
            env.RealTime.Enabled = true;
            env.RealTime.Locked = false;
            env.RealTime.WallClock = realTimeOptions.WallClock ?? throw new ArgumentNullException(nameof(realTimeOptions.WallClock));
            env.RealTime.ScalingFactor = realTimeOptions.ScalingFactor;
            env.RealTime.Locked = true;
            return env;
        }

        #endregion Environment Construction

        #region FilterStore Construction

        public static FilterStore<T> FilterStore<T>(SimEnvironment env)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);

            return new FilterStore<T>(env, Default.Capacity, Default.Policy, Default.Policy, Default.Policy);
        }

        public static FilterStore<T> FilterStore<T>(SimEnvironment env, int capacity)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));

            return new FilterStore<T>(env, capacity, Default.Policy, Default.Policy, Default.Policy);
        }

        public static FilterStore<T> FilterStore<T>(SimEnvironment env, int capacity, WaitPolicy getPolicy, WaitPolicy putPolicy)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (!Enum.IsDefined(typeof(WaitPolicy), getPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), putPolicy)) throw new ArgumentException();

            return new FilterStore<T>(env, capacity, getPolicy, putPolicy, Default.Policy);
        }

        public static FilterStore<T> FilterStore<T>(SimEnvironment env, int capacity, WaitPolicy getPolicy, WaitPolicy putPolicy, WaitPolicy itemPolicy)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (!Enum.IsDefined(typeof(WaitPolicy), getPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), putPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), itemPolicy)) throw new ArgumentException();

            return new FilterStore<T>(env, capacity, getPolicy, putPolicy, itemPolicy);
        }

        #endregion FilterStore Construction

        #region Monitor Construction

        public static Monitor Monitor(SimEnvironment env)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);

            return new Monitor(env);
        }

        /// <summary>
        ///   Returns a new monitor which is not bound to a specific environment.
        /// </summary>
        /// <returns>A new monitor which is not bound to a specific environment.</returns>
        public static Monitor Monitor()
        {
            return new Monitor(DummyEnv);
        }

        #endregion Monitor Construction

        #region PreemptiveResource Construction

        public static PreemptiveResource PreemptiveResource(SimEnvironment env, int capacity)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));

            return new PreemptiveResource(env, capacity);
        }

        #endregion PreemptiveResource Construction

        #region Resource Construction

        public static Resource Resource(SimEnvironment env, int capacity)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));

            return new Resource(env, capacity, Default.Policy);
        }

        public static Resource Resource(SimEnvironment env, int capacity, WaitPolicy requestPolicy)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (!Enum.IsDefined(typeof(WaitPolicy), requestPolicy)) throw new ArgumentException();

            return new Resource(env, capacity, requestPolicy);
        }

        #endregion Resource Construction

        #region Store Construction

        public static Store<T> Store<T>(SimEnvironment env)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);

            return new Store<T>(env, Default.Capacity, Default.Policy, Default.Policy, Default.Policy);
        }

        public static Store<T> Store<T>(SimEnvironment env, int capacity)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));

            return new Store<T>(env, capacity, Default.Policy, Default.Policy, Default.Policy);
        }

        public static Store<T> Store<T>(SimEnvironment env, int capacity, WaitPolicy getPolicy, WaitPolicy putPolicy)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (!Enum.IsDefined(typeof(WaitPolicy), getPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), putPolicy)) throw new ArgumentException();

            return new Store<T>(env, capacity, getPolicy, putPolicy, Default.Policy);
        }

        public static Store<T> Store<T>(SimEnvironment env, int capacity, WaitPolicy getPolicy, WaitPolicy putPolicy, WaitPolicy itemPolicy)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);
            if (capacity <= 0) throw new ArgumentOutOfRangeException(nameof(capacity));
            if (!Enum.IsDefined(typeof(WaitPolicy), getPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), putPolicy)) throw new ArgumentException();
            if (!Enum.IsDefined(typeof(WaitPolicy), itemPolicy)) throw new ArgumentException();

            return new Store<T>(env, capacity, getPolicy, putPolicy, itemPolicy);
        }

        #endregion Store Construction

        #region Tally Construction

        public static Tally Tally(SimEnvironment env)
        {
            // Preconditions
            if (env == null) throw new ArgumentNullException(nameof(env), ErrorMessages.NullEnvironment);

            return new Tally(env);
        }

        /// <summary>
        ///   Returns a new tally which is not bound to a specific environment.
        /// </summary>
        /// <returns>A new tally which is not bound to a specific environment.</returns>
        public static Tally Tally()
        {
            return new Tally(DummyEnv);
        }

        #endregion Tally Construction

        #region Dessert Extensions

        private static readonly IDictionary<SimEnvironment, IDictionary<SimProcess, SimEvent<object>>> SuspendInfo =
            new Dictionary<SimEnvironment, IDictionary<SimProcess, SimEvent<object>>>();

        public static void Resume(this SimProcess process)
        {
            SimEvent<object> suspend;
            if (SuspendInfo[process.Env].TryGetValue(process, out suspend))
            {
                suspend.TrySucceed();
            }
        }

        public static void Resume(this SimProcess process, double delay)
        {
            SimEvent<object> suspend;
            if (SuspendInfo[process.Env].TryGetValue(process, out suspend))
            {
                process.Env.Process(ResumeDelayed(suspend, delay));
            }
        }

        public static SimEvent Suspend(this SimEnvironment env)
        {
            return SuspendInfo[env][env.ActiveProcess] = env.Event<object>();
        }

        internal static void RemoveFromSuspendInfo(SimEnvironment env)
        {
            Debug.Assert(SuspendInfo.ContainsKey(env));
            lock (SuspendInfo)
            {
                SuspendInfo.Remove(env);
            }
        }

        private static IEnumerable<SimEvent> ResumeDelayed(SimEvent<object> suspend, double delay)
        {
            yield return suspend.Env.Timeout(delay);
            suspend.TrySucceed();
        }

        #endregion Dessert Extensions

        #region Time Utilities

        public static TimeUnit CurrentTimeUnit { get; set; }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Nanoseconds(this double time)
        {
            return ConvertTime(time, TimeUnit.Nanosecond);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Nanoseconds(this int time)
        {
            return ConvertTime(time, TimeUnit.Nanosecond);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Microseconds(this double time)
        {
            return ConvertTime(time, TimeUnit.Microsecond);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Microseconds(this int time)
        {
            return ConvertTime(time, TimeUnit.Microsecond);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Milliseconds(this double time)
        {
            return ConvertTime(time, TimeUnit.Millisecond);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Milliseconds(this int time)
        {
            return ConvertTime(time, TimeUnit.Millisecond);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Seconds(this double time)
        {
            return ConvertTime(time, TimeUnit.Second);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Seconds(this int time)
        {
            return ConvertTime(time, TimeUnit.Second);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Minutes(this double time)
        {
            return ConvertTime(time, TimeUnit.Minute);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Minutes(this int time)
        {
            return ConvertTime(time, TimeUnit.Minute);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Hours(this double time)
        {
            return ConvertTime(time, TimeUnit.Hour);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Hours(this int time)
        {
            return ConvertTime(time, TimeUnit.Hour);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Days(this double time)
        {
            return ConvertTime(time, TimeUnit.Day);
        }

        /// <summary>
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        /// <remarks>Design inspired by Humanizer library: http://www.mehdi-khalili.com/humanizer-v0-5</remarks>
        public static double Days(this int time)
        {
            return ConvertTime(time, TimeUnit.Day);
        }

        private static double ConvertTime(double time, TimeUnit unit)
        {
            return time * (SecondToUnit[unit] / SecondToUnit[CurrentTimeUnit]);
        }

        #endregion Time Utilities
    }

    public enum TimeUnit : byte
    {
        Nanosecond,
        Microsecond,
        Millisecond,
        Second,
        Minute,
        Hour,
        Day
    }

    /// <summary>
    /// </summary>
    public sealed class InterruptUncaughtException : Exception
    {
        public InterruptUncaughtException() : base(ErrorMessages.InterruptUncaught)
        {
        }

        public InterruptUncaughtException(string message) : base(message)
        {
        }

        public InterruptUncaughtException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InterruptUncaughtException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
