﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Events
{
    internal sealed class Interrupt : InnerEvent
    {
        private readonly SimProcess _process;
        private readonly object _value;

        internal Interrupt(SimEnvironment env, SimProcess process, object value) : base(env)
        {
            _process = process;
            _value = value;
        }

        #region SimEvents Members

        public override object Value
        {
            get { return _value; }
        }

        protected override void OnEnd()
        {
            _process.ReceiveInterrupt(_value);
        }

        protected override State ValidStatesMask()
        {
            return base.ValidStatesMask() | State.Succeeded;
        }

        #endregion
    }
}
