﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;

namespace PommaLabs.Dessert.Events
{
    public class Call<T> : StandaloneEvent<Call<T>, T>, IInternalCall
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly SimProcess _process;

        /// <summary>
        /// 
        /// </summary>
        private T _value;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="generator"></param>
        internal Call(SimEnvironment env, IEnumerator<SimEvent> generator) : base(env)
        {
            (this as IInternalCall).Steps = generator;
            _process = env.ActiveProcess;
            env.ScheduleEvent(this);
        }

        #region IInternalCall Members

        IInternalCall IInternalCall.PreviousCall { get; set; }

        IEnumerator<SimEvent> IInternalCall.Steps { get; set; }

        void IInternalCall.SetValue(object val)
        {
            _value = (T)val;
        }

        #endregion

        #region SimEvent Members

        public override T Value
        {
            get { return _value; }
        }

        protected override void OnEnd()
        {
            _process.PushCall(this);
        }

        protected override State ValidStatesMask()
        {
            return base.ValidStatesMask() | State.Succeeded;
        }

        #endregion
    }

    public sealed class Call : Call<object>
    {
        internal Call(SimEnvironment env, IEnumerator<SimEvent> generator) : base(env, generator)
        {
        }
    }

    internal interface IInternalCall
    {
        IInternalCall PreviousCall { get; set; }

        IEnumerator<SimEvent> Steps { get; set; }

        void SetValue(object value);
    }
}
