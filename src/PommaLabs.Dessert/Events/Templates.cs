﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using PommaLabs.Dessert.Core;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Events
{
    /// <summary>
    ///   Models aspects shared by all resource events.
    /// </summary>
    /// <typeparam name="TEv"></typeparam>
    /// <typeparam name="TVal"></typeparam>
    public abstract class ResourceEvent<TEv, TVal> : SimEvent<TEv, TVal>, IDisposable
        where TEv : ResourceEvent<TEv, TVal>
    {
        private readonly double _priority;

        internal ResourceEvent(SimEnvironment env, double priority) : base(env)
        {
            _priority = priority;
        }

        #region Public Members

        /// <summary>
        ///   Returns true if and only if event has been disposed; otherwise, it returns false.
        /// </summary>
        [System.Diagnostics.Contracts.Pure]
        public bool Disposed { get; protected set; }

        /// <summary>
        ///   The priority assigned to this resource event.
        ///   Usually, the priority is only considered when
        ///   the policy is set to <see cref="WaitPolicy.Priority"/>.
        /// </summary>
        public double Priority
        {
            get { return _priority; }
        }

        public abstract void Dispose();

        #endregion

        #region SimEvent Members

        protected sealed override State ValidStatesMask()
        {
            return base.ValidStatesMask();
        }

        #endregion
    }

    public abstract class StandaloneEvent<TEv, TVal> : SimEvent<TEv, TVal> where TEv : SimEvent<TEv, TVal>
    {
        internal StandaloneEvent(SimEnvironment env) : base(env)
        {
        }

        #region SimEvent Members

        protected sealed override bool CanHaveParents
        {
            get { return false; }
        }

        protected sealed override bool CanHaveSubscribers
        {
            get { return true; }
        }

        protected override State ValidStatesMask()
        {
            return State.Created;
        }

        #endregion
    }

    /// <summary>
    ///   Represents an event which cannot be "yielded" by any user process.
    ///   It is used internally to represent special events, like interrupts.
    /// </summary>
    internal abstract class InnerEvent : SimEvent<InnerEvent, object>
    {
        internal InnerEvent(SimEnvironment env) : base(env)
        {
        }

        #region SimEvent Members

        protected sealed override bool CanHaveParents
        {
            get { return false; }
        }

        protected sealed override bool CanHaveSubscribers
        {
            get { return false; }
        }

        protected override State ValidStatesMask()
        {
            return State.Created;
        }

        #endregion
    }
}
