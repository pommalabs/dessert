﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using PommaLabs.Dessert.Core;

namespace PommaLabs.Dessert.Events
{
    public sealed class SimEvent<T> : SimEvent<SimEvent<T>, T>
    {
        private State _finalState = State.Created;

        /// <summary>
        /// 
        /// </summary>
        private T _value;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env">The <see cref="SimEnvironment"/> this token will belong to.</param>
        internal SimEvent(SimEnvironment env) : base(env)
        {
        }

        #region IEvent Members

        public bool Triggered
        {
            get { return _finalState != State.Created; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="InvalidOperationException">
        ///   This event has already succeeded: therefore, it cannot fail anymore.
        /// </exception>
        public void Fail()
        {
            // Preconditions
            if (Triggered) throw new InvalidOperationException();

            Trigger(State.Failed, default(T));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val">
        ///   An object that will be sent to processes waiting for this event to occur.
        /// </param>
        /// <exception cref="InvalidOperationException">
        ///   This event has already succeeded: therefore, it cannot fail anymore.
        /// </exception>
        public void Fail(T val)
        {
            // Preconditions
            if (Triggered) throw new InvalidOperationException();

            Trigger(State.Failed, val);
        }

        public bool TryFail()
        {
            return TryTrigger(State.Failed, default(T));
        }

        public bool TryFail(T val)
        {
            return TryTrigger(State.Failed, val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="InvalidOperationException">
        ///   This event has already succeeded: therefore, it cannot fail anymore.
        /// </exception>
        public void Succeed()
        {
            // Preconditions
            if (Triggered) throw new InvalidOperationException();

            Trigger(State.Succeeded, default(T));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val">
        ///   An object that will be sent to processes waiting for this event to occur.
        /// </param>
        /// <exception cref="InvalidOperationException">
        ///   This event has already succeeded: therefore, it cannot succeed anymore.
        /// </exception>
        public void Succeed(T val)
        {
            // Preconditions
            if (Triggered) throw new InvalidOperationException();

            Trigger(State.Succeeded, val);
        }

        public bool TrySucceed()
        {
            return TryTrigger(State.Succeeded, default(T));
        }

        public bool TrySucceed(T val)
        {
            return TryTrigger(State.Succeeded, val);
        }

        private void Trigger(State finalState, T val)
        {
            _finalState = finalState;
            _value = val;
            Env.ScheduleEvent(this);
        }

        private bool TryTrigger(State finalState, T val)
        {
            if (_finalState != State.Created)
            {
                return false;
            }
            _finalState = finalState;
            _value = val;
            Env.ScheduleEvent(this);
            return true;
        }

        #endregion

        #region SimEvent Members

        protected override State FinalState
        {
            get { return _finalState; }
        }

        public override T Value
        {
            get { return _value; }
        }

        protected override State ValidStatesMask()
        {
            return base.ValidStatesMask() | State.Failed;
        }

        #endregion
    }
}
