﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Diagnostics;
using PommaLabs.Dessert.Core;

namespace PommaLabs.Dessert.Events
{
    /// <summary>
    ///   An event that is scheduled with a certain delay after its creation.<br/>
    ///   This event can be used by processes to wait (or hold their state) for delay time steps.
    ///   It is immediately scheduled at Env.Now + delay and has thus
    ///   (in contrast to <see cref="SimEvent{T}"/>) no Success() or Fail() methods.
    /// </summary>
    public class Timeout<T> : SimEvent<Timeout<T>, T>
    {
        /// <summary>
        ///   The delay at which this event was scheduled.
        /// </summary>
        private readonly double _delay;

        /// <summary>
        ///   The value which was assigned to this event.
        /// </summary>
        private readonly T _value;

        /// <summary>
        ///   Creates an event that is scheduled with a certain delay after its creation.
        /// </summary>
        /// <param name="env">The <see cref="SimEnvironment"/> this event will belong to.</param>
        /// <param name="delay">The delay at which the timeout event will be scheduled.</param>
        /// <param name="value">The value which will be returned on event success.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///   <paramref name="delay"/> is less than zero, or <paramref name="delay"/> plus
        ///   current time clock is greater than <see cref="double.MaxValue"/>.
        /// </exception>
        internal Timeout(SimEnvironment env, double delay, T value) : base(env)
        {
            _delay = delay;
            _value = value;
            env.ScheduleTimeout(this, _delay);
        }

        #region ITimeout Members

        /// <summary>
        ///   The delay at which this event was scheduled.
        /// </summary>
        public double Delay
        {
            get
            {
                Debug.Assert(_delay >= 0);
                Debug.Assert(!double.IsPositiveInfinity(_delay));
                return _delay;
            }
        }

        #endregion

        #region SimEvent Members

        public override T Value => _value;

        protected override void OnEnd()
        {
            Env.SetNow(At, AtWallClock);
        }

        #endregion

        #region Real-time Helpers

        internal const long NoWallClock = -1;

        internal double AtWallClock = NoWallClock;

        #endregion
    }

    public sealed class Timeout : Timeout<double>
    {
        internal Timeout(SimEnvironment env, double delay) : base(env, delay, delay)
        {
        }
    }
}
