﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Core
{
    internal static class ConditionEvaluators
    {
        #region All Evaluators

        public static bool AllEvents<T1>(Condition<T1> c) where T1 : SimEvent
        {
            return c.Value.Count == 1;
        }

        public static bool AllEvents<T1, T2>(Condition<T1, T2> c) where T1 : SimEvent where T2 : SimEvent
        {
            return c.Value.Count == 2;
        }

        public static bool AllEvents<T1, T2, T3>(Condition<T1, T2, T3> c) where T1 : SimEvent where T2 : SimEvent
            where T3 : SimEvent
        {
            return c.Value.Count == 3;
        }

        public static bool AllEvents<T1, T2, T3, T4>(Condition<T1, T2, T3, T4> c) where T1 : SimEvent
            where T2 : SimEvent where T3 : SimEvent where T4 : SimEvent
        {
            return c.Value.Count == 4;
        }

        public static bool AllEvents<T1, T2, T3, T4, T5>(Condition<T1, T2, T3, T4, T5> c) where T1 : SimEvent
            where T2 : SimEvent where T3 : SimEvent where T4 : SimEvent where T5 : SimEvent
        {
            return c.Value.Count == 5;
        }

        #endregion

        #region Any Evaluators

        public static bool AnyEvent<T1>(Condition<T1> c) where T1 : SimEvent
        {
            return c.Value.Count >= 1;
        }

        public static bool AnyEvent<T1, T2>(Condition<T1, T2> c) where T1 : SimEvent where T2 : SimEvent
        {
            return c.Value.Count >= 1;
        }

        public static bool AnyEvent<T1, T2, T3>(Condition<T1, T2, T3> c) where T1 : SimEvent where T2 : SimEvent
            where T3 : SimEvent
        {
            return c.Value.Count >= 1;
        }

        public static bool AnyEvent<T1, T2, T3, T4>(Condition<T1, T2, T3, T4> c) where T1 : SimEvent where T2 : SimEvent
            where T3 : SimEvent where T4 : SimEvent
        {
            return c.Value.Count >= 1;
        }

        public static bool AnyEvent<T1, T2, T3, T4, T5>(Condition<T1, T2, T3, T4, T5> c) where T1 : SimEvent
            where T2 : SimEvent where T3 : SimEvent where T4 : SimEvent where T5 : SimEvent
        {
            return c.Value.Count >= 1;
        }

        #endregion
    }
}
