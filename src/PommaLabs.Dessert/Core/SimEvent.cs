﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Dessert.Events;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Dessert.Core
{
    /// <summary>
    ///   A stronger typed event, which adds type notation to many properties which are untyped in SimPy.
    /// </summary>
    /// <typeparam name="TEv">The type of the event which implements this interface.</typeparam>
    /// <typeparam name="TVal">The type of the value returned by this interface.</typeparam>
    /// <remarks>
    ///   This class could not be named "Event" in order to maintain compatibility
    ///   with Visual Basic code, where "Event" is a language keyword.
    /// </remarks>
    public abstract class SimEvent<TEv, TVal> : SimEvent where TEv : SimEvent<TEv, TVal>
    {
        /// <summary>
        /// 
        /// </summary>
        private SinglyLinkedList<Action<TEv>> _callbacks;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        internal SimEvent(SimEnvironment env) : base(env)
        {
        }

        #region Abstract Members

        /// <summary>
        ///   The strongly typed value returned by the event. This property contains the value that in SimPy
        ///   is "sent" to the process through the generator itself; since we cannot do anything
        ///   similar in .NET, we have to use this property to store that kind of values.<br/>
        /// </summary>
        /// <remarks>
        ///   As a rule of thumb, the value on this property will be ready only after
        ///   <see cref="SimEvent.Succeeded"/> or <see cref="SimEvent.Failed"/> will be true. However, 
        ///   this property can always be accessed: therefore, please pay attention to the fact that
        ///   this property will return a null value when a value is not ready or 
        ///   when an event does not have a proper value.
        /// </remarks>
        public new abstract TVal Value { get; }

        #endregion

        #region IEvent Members

        /// <summary>
        ///   Collection of functions that are called when the event is processed.
        /// </summary>
        public ICollection<Action<TEv>> Callbacks
        {
            get { return _callbacks ??= new SinglyLinkedList<Action<TEv>>(); }
        }

        /// <inheritdoc/>
        protected sealed override object GetValue()
        {
            return Value;
        }

        #endregion

        #region SimEvent Members

        internal override void End()
        {
            OnEnd();
            var finalState = FinalState;
            Debug.Assert(!InFinalState && (FinalStatesMask & finalState) > 0);
            // Final state must be assigned before triggering conditions, because they will use it
            // to determine whether or not undo the events they control.
            SetFinalState(finalState);
            if (_callbacks != null)
            {
                // Debug.Assert(_callbacks.Count > 0);
                // Assert above is potentially false, since the user may call
                // Clear on _callbacks, as it is an ICollection.
                foreach (var callback in _callbacks)
                {
                    callback(this as TEv);
                }
                _callbacks = null;
            }
            // Following piece of code is optimezed for cases in which an event
            // belongs to a single event, which represents the great majority of situations.
            if (Conditions != null)
            {
                Debug.Assert(Conditions.Count > 0);
                // Triggers all conditions to which this event belongs.
                IParentCondition condition;
                if (Conditions.Count == 1 && !(condition = Conditions.First).Succeeded)
                {
                    condition.Trigger(this);
                }
                else
                {
                    var en = Conditions.GetEnumerator();
                    while (en.MoveNext())
                    {
                        if (!(condition = en.Current).Succeeded)
                        {
                            condition.Trigger(this);
                        }
                    }
                }
                Conditions = null;
            }
            // Following piece of code is optimezed for cases in which an event
            // is "yielded" by a single process, which represents the great majority of situations.
            if (Subscribers != null)
            {
                Debug.Assert(Subscribers.Count > 0);
                // Reschedules all processes which are not completed yet.
                // The "where" clause filters yielders, so that only those processes
                // which are still running are picked up for scheduling.
                SimProcess subscriber;
                if (Subscribers.Count == 1 && (subscriber = Subscribers.First).IsAlive)
                {
                    Env.ScheduleProcess(subscriber);
                }
                else
                {
                    var en = Subscribers.GetEnumerator();
                    while (en.MoveNext())
                    {
                        if ((subscriber = en.Current).IsAlive)
                        {
                            Env.ScheduleProcess(subscriber);
                        }
                    }
                }
                Subscribers = null;
            }
        }

        #endregion
    }
}
