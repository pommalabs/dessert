﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Runtime.Serialization;

namespace PommaLabs.Dessert.Core
{
    internal sealed class DessertException : Exception
    {
        public DessertException()
        {
        }

        public DessertException(string message) : base(message)
        {
        }

        public DessertException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public DessertException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
