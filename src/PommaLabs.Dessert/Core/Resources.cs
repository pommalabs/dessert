﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Core
{
    internal static partial class ErrorMessages
    {
        public const string ContractClass = "A contract class cannot be instanced";
        public const string DifferentEnvironment = "Given event belongs to a different environment";
        public const string DifferentResource = "Given request belongs to a different resource";
        public const string EndedProcess = "Given process has ended its lifecycle.";
        public const string ExcessiveQuantity = "Given quantity is greater than container capacity.";
        public const string InternalError = "???";
        public const string InterruptSameProcess = "A process cannot interrupt itself.";
        public const string InterruptedDifferentProcess = "A process cannot query another process for interrupts.";
        public const string InterruptUncaught = "Process was interrupted but it did not check for it.";
        public const string InvalidDelay = "Delay should not be negative and Now+delay must not cause overflow.";
        public const string InvalidEventCount = "No more than five events can take part in a condition.";
        public const string InvalidMethod = "Given method should not be used.";
        public const string InvalidRecordingTime = "Given time is less than start time.";
        public const string NegativeQuantity = "Quantity should not be negative.";
        public const string NoObservations = "Tally or monitor has no observations.";
        public const string NullEnvironment = "Given simulation environment cannot be null.";
        public const string NullEvaluator = "Condition evaluator cannot be null.";
        public const string NullEvent = "Yielded events, or condition events, cannot be null.";
        public const string NullGenerator = "Generator used by process or call event cannot be null.";
        public const string NullRequest = "A null request cannot be released.";
        public const string ScalingFactorNotUpdatable = "Scaling factor can be set only before environment creation and it cannot be overwritten.";
        public const string WallClockNotUpdatable = "Wall clock can be set only before environment creation and it cannot be overwritten.";

        public static string InvalidEnum<TEnum>() where TEnum : struct
        {
            return string.Format("Invalid value for {0}.", typeof(TEnum).Name);
        }
    }

    internal static class Default
    {
        public const int Capacity = int.MaxValue;
        public const int Level = 0;
        public const int NoDelay = 0;
        public const WaitPolicy Policy = WaitPolicy.FIFO;
        public const bool Preempt = true;
        public const double Priority = 0.0;

        public static readonly object NoValue = new object();
        public static readonly object Value = null;
    }
}
