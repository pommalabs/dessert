﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Core
{
    internal sealed class FakeReadOnlyList<T> : IList<T>
    {
        private const string ReadOnlyCollection = "Collection is readonly";
        private T[] _array = new T[1];

        public void ForceAdd(T item)
        {
            if (_array.Length == Count)
            {
                Array.Resize(ref _array, Count << 1);
            }
            _array[Count++] = item;
        }

        #region IList Members

        public int Count { get; private set; }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public T this[int index]
        {
            get { return _array[index]; }
            set { throw new InvalidOperationException(ReadOnlyCollection); }
        }

        public void Add(T item)
        {
            throw new InvalidOperationException(ReadOnlyCollection);
        }

        public void Clear()
        {
            throw new InvalidOperationException(ReadOnlyCollection);
        }

        public bool Contains(T item)
        {
            for (var i = 0; i < Count; ++i)
            {
                if (ReferenceEquals(item, _array[i]))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///   Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"/> 
        ///   to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">
        ///   The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements
        ///   copied from <see cref="T:System.Collections.Generic.ICollection`1"/>.
        ///   The <see cref="T:System.Array"/> must have zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">
        ///   The zero-based index in <paramref name="array"/> at which copying begins.
        /// </param>
        /// <exception cref="T:System.ArgumentNullException">
        ///   <paramref name="array"/> is null.</exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///   <paramref name="arrayIndex"/> is less than 0.</exception>
        /// <exception cref="T:System.ArgumentException">
        ///   The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"/> 
        ///   is greater than the available space from <paramref name="arrayIndex"/>
        ///   to the end of the destination <paramref name="array"/>.
        /// </exception>
        public void CopyTo(T[] array, int arrayIndex)
        {
            for (var i = 0; i < Count; ++i)
            {
                array[arrayIndex + i] = _array[i];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; ++i)
            {
                yield return _array[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (var i = 0; i < Count; ++i)
            {
                yield return _array[i];
            }
        }

        /// <summary>
        ///   Determines the index of a specific item in the <see cref="FakeReadOnlyList{T}"/>.
        /// </summary>
        /// <param name="item">
        ///   The object to locate in the <see cref="FakeReadOnlyList{T}"/>.
        /// </param>
        /// <returns>
        ///   The index of <paramref name="item"/> if found in the list; otherwise, -1.
        /// </returns>
        public int IndexOf(T item)
        {
            for (var i = 0; i < Count; ++i)
            {
                if (ReferenceEquals(item, _array[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            throw new InvalidOperationException(ReadOnlyCollection);
        }

        public bool Remove(T item)
        {
            throw new InvalidOperationException(ReadOnlyCollection);
        }

        public void RemoveAt(int index)
        {
            throw new InvalidOperationException(ReadOnlyCollection);
        }

        #endregion
    }
}
