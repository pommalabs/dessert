﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Core
{
    /// <summary>
    ///   Represents an entity that belongs to a specific environment.
    ///   An entity can only be "used" in the environment it belongs to.
    /// </summary>
    public abstract class SimEntity
    {
        /// <summary>
        ///   Stores the environment in which this entity was created.
        /// </summary>
        private readonly SimEnvironment _env;

        /// <summary>
        ///   Creates a new entity (with given name) belonging to given environment.
        /// </summary>
        /// <param name="env">The environment this entity will belong to.</param>
        internal SimEntity(SimEnvironment env)
        {
            _env = env;
        }

        #region Public Members

        /// <summary>
        ///   Returns the environment in which this entity was created.
        /// </summary>
        public SimEnvironment Env
        {
            get { return _env; }
        }

        #endregion Public Members
    }
}
