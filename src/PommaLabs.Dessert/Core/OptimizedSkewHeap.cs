﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics;

namespace PommaLabs.Dessert.Core
{
    internal sealed class OptimizedSkewHeap
    {
        public OptimizedSkewHeap(SimEvent initialRoot)
        {
            Min = initialRoot;
            Count = 1;
        }

        public int Count { get; private set; }

        public SimEvent Min { get; private set; }

        public void Add(SimEvent ev)
        {
            Debug.Assert(Min != null && ev != null);
            Debug.Assert(ev.Left == null && ev.Right == null);
            ev.Scheduled = true;
            if (SimEvent.IsSmaller(ev, Min))
            {
                ev.Left = Min;
                Min = ev;
            }
            else
            {
                var x = Min;
                var y = x;
                x = x.Right;
                y.Right = y.Left;
                while (x != null)
                {
                    if (SimEvent.IsSmaller(ev, x))
                    {
                        y.Left = ev;
                        y = ev;
                        ev = x;
                    }
                    else
                    {
                        y.Left = x;
                        y = x;
                    }
                    x = y.Right;
                    y.Right = y.Left;
                }
                y.Left = ev;
            }
            Count++;
        }

        public void RemoveMin()
        {
            Debug.Assert(Count > 0);
            Debug.Assert(Min != null);
            var min = Min;
            var h1 = Min.Left;
            var h2 = Min.Right;
            if (h1 == null)
            {
                Min = h2;
            }
            else if (h2 == null)
            {
                Min = h1;
            }
            else
            {
                if (SimEvent.IsSmaller(h2, h1))
                {
                    var tmp = h2;
                    h2 = h1;
                    h1 = tmp;
                }
                Min = h1;
                var y = h1;
                h1 = h1.Right;
                y.Right = y.Left;
                while (h1 != null)
                {
                    if (SimEvent.IsSmaller(h2, h1))
                    {
                        y.Left = h2;
                        y = h2;
                        h2 = h1;
                    }
                    else
                    {
                        y.Left = h1;
                        y = h1;
                    }
                    h1 = y.Right;
                    y.Right = y.Left;
                }
                y.Left = h2;
            }
            min.Scheduled = false;
            min.Left = min.Right = null;
            Count--;
        }
    }
}
