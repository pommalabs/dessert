﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class ConditionOperators
    {
        private static IEnumerable<SimEvent> Process(SimEnvironment env)
        {
            var c1 = env.Timeout(3).And(env.Timeout(5)).And(env.Timeout(7));
            yield return c1;
            Console.WriteLine(env.Now); // 7
            Console.WriteLine(c1.Value.Contains(c1.Ev1)); // True
            Console.WriteLine(c1.Value.Contains(c1.Ev2)); // True
            Console.WriteLine(c1.Value.Contains(c1.Ev3)); // True

            var c2 = env.Timeout(7).Or(env.Timeout(5).Or(env.Timeout(3)));
            yield return c2;
            Console.WriteLine(env.Now); // 10
            Console.WriteLine(c2.Value.Contains(c2.Ev1)); // False
            Console.WriteLine(c2.Value.Contains(c2.Ev2)); // False
            Console.WriteLine(c2.Value.Contains(c2.Ev3)); // True
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Process(env));
            env.Run();
        }
    }
}
