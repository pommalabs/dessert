﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class TimeoutsWithValues
    {
        private static IEnumerable<SimEvent> Process(SimEnvironment env)
        {
            var a = env.Timeout(3, "A");
            var b = env.Timeout(5, "B");
            var c = env.Timeout(7, "C");
            var cond = a.And(b.Or(c));
            yield return cond;
            Console.WriteLine(env.Now);
            foreach (var e in cond.Value)
            {
                Console.WriteLine(e.Value);
            }
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Process(env));
            env.Run();
        }
    }
}
