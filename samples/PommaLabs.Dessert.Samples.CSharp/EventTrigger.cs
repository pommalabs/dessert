﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class EventTrigger
    {
        private static IEnumerable<SimEvent> DoSucceed(SimEnvironment env, SimEvent<object> ev)
        {
            yield return env.Timeout(5);
            ev.Succeed("SI :)");
        }

        private static IEnumerable<SimEvent> DoFail(SimEnvironment env, SimEvent<object> ev)
        {
            yield return env.Timeout(5);
            ev.Fail("NO :(");
        }

        private static IEnumerable<SimEvent> Proc(SimEnvironment env)
        {
            var ev1 = env.Event();
            env.Process(DoSucceed(env, ev1));
            yield return ev1;
            if (ev1.Succeeded)
            {
                Console.WriteLine(ev1.Value);
            }

            var ev2 = env.Event();
            env.Process(DoFail(env, ev2));
            yield return ev2;
            if (ev2.Failed)
            {
                Console.WriteLine(ev2.Value);
            }
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Proc(env));
            env.Run();
        }
    }
}
