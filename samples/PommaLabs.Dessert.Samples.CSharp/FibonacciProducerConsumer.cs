﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class FibonacciProducerConsumer
    {
        private static IEnumerable<SimEvent> FibonacciFunc(SimEnvironment env, int n)
        {
            if (n <= 0)
            {
                yield return env.Exit(0);
            }
            else if (n == 1)
            {
                yield return env.Exit(1);
            }
            else
            {
                var call = env.Call<int>(FibonacciFunc(env, n - 1));
                yield return call;
                var n1 = call.Value;
                call = env.Call<int>(FibonacciFunc(env, n - 2));
                yield return call;
                var n2 = call.Value;
                yield return env.Exit(n1 + n2);
            }
        }

        private static IEnumerable<SimEvent> Producer(SimEnvironment env, Store<int> store, int n)
        {
            var call = env.Call(FibonacciFunc(env, n));
            yield return call;
            store.Put((int)call.Value);
        }

        private static IEnumerable<SimEvent> Consumer(Store<int> store)
        {
            var getEv = store.Get();
            yield return getEv;
            Console.WriteLine(getEv.Value);
        }

        public static void Run()
        {
            const int Count = 10;
            var env = Sim.Environment();
            var store = Sim.Store<int>(env);
            for (var i = 0; i < Count; ++i)
            {
                env.Process(Producer(env, store, i));
                env.Process(Consumer(store));
            }
            env.Run();
        }
    }
}
