﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class TrainInterrupt
    {
        private static readonly double s_avgTravelTime;
        private static readonly double s_breakTime;

        static TrainInterrupt()
        {
            Sim.CurrentTimeUnit = TimeUnit.Minute;
            s_avgTravelTime = 10.Minutes();
            s_breakTime = 50.Minutes();
        }

        private static IEnumerable<SimEvent> Train(SimEnvironment env)
        {
            object cause;
            while (true)
            {
                var time = env.Random.Exponential(1.0 / s_avgTravelTime);
                Console.WriteLine("Treno in viaggio per {0:.00} minuti", time);
                yield return env.Timeout(time);
                if (env.ActiveProcess.Interrupted(out cause))
                {
                    break;
                }
                Console.WriteLine("Arrivo in stazione, attesa passeggeri");
                yield return env.Timeout(2.Minutes());
                if (env.ActiveProcess.Interrupted(out cause))
                {
                    break;
                }
            }
            Console.WriteLine("Al minuto {0:.00}: {1}", env.Now, cause);
        }

        private static IEnumerable<SimEvent> EmergencyBrake(SimEnvironment env, SimProcess train)
        {
            yield return env.Timeout(s_breakTime);
            train.Interrupt("FRENO EMERGENZA");
        }

        public static void Run()
        {
            var env = Sim.Environment(21);
            var train = env.Process(Train(env));
            env.Process(EmergencyBrake(env, train));
            env.Run();
        }
    }
}
