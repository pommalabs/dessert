﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp.Galois
{
    internal sealed class Server : Entity
    {
        private readonly IDictionary<int, CodePacket> _cache = new Dictionary<int, CodePacket>();
        private readonly int _id;
        private readonly IDictionary<int, CodePacket> _packets = new Dictionary<int, CodePacket>();
        private readonly Store<RequestPacket> _requests;
        private bool _isUp;

        public Server(SimEnvironment env, G g, int id) : base(env, g)
        {
            _id = id;
            _isUp = true;
            _requests = Sim.Store<RequestPacket>(env);
        }

        public void Receive(RequestPacket newRequest)
        {
            if (!_isUp)
            {
                return;
            }
            _requests.Put(newRequest);
        }

        public IEnumerable<SimEvent> Run()
        {
            while (true)
            {
                var reqGet = _requests.Get();
                yield return reqGet;

                var rp = reqGet.Value;
                _logger.Information("Handling request {Request}", rp);

                // We gather the requested code packet
                if (_cache.TryGetValue(rp.Owner, out var cp))
                {
                    _logger.Information("Gathering {Packet} from cache, holding for {Wait}", cp, Ts(G.CacheAccessTime));
                    yield return Env.Timeout(G.CacheAccessTime);
                }
                else
                {
                    // Not in cache, packet must be recovered from disk
                    _cache.Add(rp.Owner, cp = _packets[rp.Owner]);
                    var w = Env.Random.NextDouble(0, G.MaxAccessTime);
                    _logger.Information("Gathering {Packet} from disk and caching it, holding for {Wait}", cp, Ts(w));
                    yield return Env.Timeout(w);
                }

                G.ServerOSes[_id].Send(cp, rp.SessionId);

                // With some probability, the server goes down for some time
                if (Env.Random.NextDouble() >= G.ServerDownProb)
                {
                    continue;
                }
                _isUp = false;
                var wait = Env.Random.Exponential(1.0 / G.ServerStopMean);
                _logger.Information("Going offline, holding for {Wait}", Ts(wait));
                yield return Env.Timeout(wait);
                _cache.Clear();
                _isUp = true;
            }
        }

        public void StoreCodePacket(CodePacket cp)
        {
            _packets.Add(cp.Owner, cp);
        }
    }

    internal sealed class ServerOS : BaseOS
    {
        private readonly Store<AnswerInfo> _sendRequests;

        public ServerOS(SimEnvironment env, G g, int osId) : base(env, g, osId)
        {
            _sendRequests = Sim.Store<AnswerInfo>(env);
        }

        public IEnumerable<SimEvent> Run()
        {
            var incomingGet = IncomingFrames.Get();
            var sendGet = _sendRequests.Get();
            while (true)
            {
                yield return incomingGet.Or(sendGet);
                if (incomingGet.Succeeded)
                {
                    var packet = incomingGet.Value;
                    var reqPacket = new RequestPacket(packet.Src, packet.SessionId);
                    _logger.Information("Sending packet {Packet} to server", reqPacket);
                    G.Servers[Id].Receive(reqPacket);
                    incomingGet = IncomingFrames.Get();
                }
                if (!sendGet.Succeeded)
                {
                    continue;
                }

                var sr = sendGet.Value;
                var cp = sr.Packet;
                var sessionId = sr.SessionId;

                // At this point we need to break the code packet into some UDP packets and send them
                var nUdp = (int)Math.Ceiling(cp.Len / (double)G.MTU);
                var lastPacketSize = cp.Len - nUdp * G.MTU;
                if (lastPacketSize != 0)
                {
                    nUdp++;
                }
                _logger.Information("Breaking packet into {PacketCount} UDP packets", nUdp);

                UdpPacket p;
                double w;
                for (var i = 0; i < nUdp - 1; ++i)
                {
                    p = new UdpPacket(sessionId, Id, cp.Owner, G.MTU, PacketType.Answer, nUdp);
                    w = G.Latency + p.Len / G.Bandwidth;
                    _logger.Information("Sending {Packet}, holding for {Wait}", p, Ts(w));
                    yield return Env.Timeout(w);
                    G._switch.Receive(p);
                }
                // Must send the remaining part of the packet
                if (lastPacketSize == 0)
                {
                    p = new UdpPacket(sessionId, Id, cp.Owner, G.MTU, PacketType.Answer, nUdp);
                }
                else
                {
                    p = new UdpPacket(sessionId, Id, cp.Owner, lastPacketSize, PacketType.Answer, nUdp);
                }
                w = G.Latency + p.Len / G.Bandwidth;
                _logger.Information("Sending {Packet}, holding for {Wait}", p, Ts(w));
                yield return Env.Timeout(w);
                G._switch.Receive(p);
                sendGet = _sendRequests.Get();
            }
        }

        public void Send(CodePacket cp, object sessionId)
        {
            _sendRequests.Put(new AnswerInfo(cp, sessionId));
        }

        private struct AnswerInfo
        {
            public readonly CodePacket Packet;
            public readonly object SessionId;

            public AnswerInfo(CodePacket cp, object sessionId)
            {
                Packet = cp;
                SessionId = sessionId;
            }
        }
    }
}
