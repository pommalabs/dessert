﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using System.Linq;
using PommaLabs.Dessert.Events;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp.Galois
{
    internal sealed class Client : Entity
    {
        private readonly int _id;
        private readonly ServerInfo[] _peers = new ServerInfo[G.MachineCount - 1];
        private readonly ICollection<CodePacket> _receivedPackets = new LinkedList<CodePacket>();
        private SimEvent<object> _packetConstructed;

        public Client(SimEnvironment env, G g, int id) : base(env, g)
        {
            _id = id;
            for (var i = 1; i < G.MachineCount; ++i)
            {
                var index = (i + id) % G.MachineCount;
                _peers[i - 1] = new ServerInfo(index);
            }

            _packetConstructed = env.Event();
        }

        public void Receive(IEnumerable<CodePacket> packets)
        {
            foreach (var codePacket in packets)
            {
                _receivedPackets.Add(codePacket);
            }
            if (_receivedPackets.Count >= G.RequestCount)
            {
                _packetConstructed.Succeed();
            }
        }

        public IEnumerable<SimEvent> Run()
        {
            while (true)
            {
                _logger.Information("Deciding wheter to sleep or not");
                if (Env.Random.NextDouble() < G.ClientSleepProb)
                {
                    var w = Env.Random.Exponential(1.0 / G.ClientStopMean);
                    _logger.Information("Going inactive, holding for {Wait}", Ts(w));
                    yield return Env.Timeout(w);
                }

                // We reset the state of all peers, generating a new sessionId for the request;
                // after that, we send the request to G.RequestCount + G.ExtraRequestCount 
                // peers starting after this node.
                foreach (var p in _peers)
                {
                    p._isUp = true;
                }
                _receivedPackets.Clear();

                var sessionId = string.Format("{0}-{1}", _id, Env.Random.Next(0, 10000));
                var reqCount = G.RequestCount + G.ExtraRequestCount;
                var nextServer = 0;

                // To collect statistics
                var totalReqCount = 0;
                var startTime = Env.Now;

                while (reqCount > G.ExtraRequestCount)
                {
                    _logger.Information("Gathered code packets: {PacketCount}", _receivedPackets.Count);
                    var counter = 0;
                    for (var s = nextServer; s < G.MachineCount + nextServer - 1; ++s)
                    {
                        var dst = _peers[s % (G.MachineCount - 1)];
                        if (counter < reqCount)
                        {
                            if (!dst._isUp)
                            {
                                continue;
                            }
                            _logger.Information("Sending request to {Destination}", dst);
                            var p = new UdpPacket(sessionId, _id, dst.Id, G.RequestSize, PacketType.Request, 1);
                            G.ClientOSes[_id].Send(p);
                            counter++;
                        }
                        else
                        {
                            nextServer = s % (G.MachineCount - 1);
                            break;
                        }
                    }

                    _logger.Information("Starting timeout");
                    yield return _packetConstructed.Or(Env.Timeout(G.Timeout * counter * 8));
                    _packetConstructed = Env.Event();

                    SetPeersDown();
                    reqCount = G.RequestCount + G.ExtraRequestCount - _receivedPackets.Count;
                    totalReqCount += counter;
                }

                // At this point we surely have:
                // _receivedPackets.Count >= G.RequestCount
                // So, we can reconstruct the data.
                G.Stats.AddClientTimeWaited(Env.Now - startTime);
                G.Stats.AddClientRequests(totalReqCount);
                G.Stats.FileReconstructed();
            }
        }

        private void SetPeersDown()
        {
            foreach (var peer in _peers.Where(p => p._isUp && _receivedPackets.Any(r => r.Keeper == p.Id)))
            {
                peer._isUp = false;
            }
        }

        private sealed class ServerInfo
        {
            public readonly int Id;
            public bool _isUp;

            public ServerInfo(int id)
            {
                Id = id;
                _isUp = true;
            }

            public override string ToString()
            {
                return string.Format("ServerInfo(Id: {0}, IsUp: {1})", Id, _isUp);
            }
        }
    }

    internal sealed class ClientOS : BaseOS
    {
        private readonly ICollection<UdpPacket> _arrivedFrames = new LinkedList<UdpPacket>();
        private readonly Store<UdpPacket> _sendRequests;
        private object _currSessionId;

        public ClientOS(SimEnvironment env, G g, int osId) : base(env, g, osId)
        {
            _sendRequests = Sim.Store<UdpPacket>(env);
        }

        public void Send(UdpPacket p)
        {
            _currSessionId = p.SessionId;
            _sendRequests.Put(p);
        }

        public IEnumerable<SimEvent> Run()
        {
            var incomingGet = IncomingFrames.Get();
            var sendGet = _sendRequests.Get();
            while (true)
            {
                yield return incomingGet.Or(sendGet);
                if (incomingGet.Succeeded)
                {
                    var p = incomingGet.Value;
                    if (p.SessionId == _currSessionId)
                    {
                        _logger.Information("Received a new frame: {Frame}", p);
                        _arrivedFrames.Add(p);
                        _logger.Information("Reconstructing packets from {FrameCount} frames", _arrivedFrames.Count);
                        var packets = ReconstructPackets();
                        if (packets.Count != 0)
                        {
                            G.Clients[Id].Receive(packets);
                            _logger.Information("Reconstructed and sent {PacketCount} packets", packets.Count);
                        }
                    }
                    incomingGet = IncomingFrames.Get();
                }
                if (sendGet.Succeeded)
                {
                    var p = sendGet.Value;
                    var w = G.Latency + p.Len / G.Bandwidth;
                    _logger.Information("Sending {Frame}, holding for {Wait}", p, Ts(w));
                    yield return Env.Timeout(w);
                    G._switch.Receive(p);
                    sendGet = _sendRequests.Get();
                }
            }
        }

        private LinkedList<CodePacket> ReconstructPackets()
        {
            var visited = new HashSet<int>();
            var usedServers = new HashSet<int>();
            var unusedFrames = new LinkedList<UdpPacket>();
            var codePackets = new LinkedList<CodePacket>();

            foreach (var f in _arrivedFrames)
            {
                if (visited.Contains(f.Src))
                {
                    if (!usedServers.Contains(f.Src))
                    {
                        unusedFrames.AddLast(f);
                    }
                    continue;
                }

                visited.Add(f.Src);
                var frameCount = f.Count;
                var actualCount = _arrivedFrames.Count(p => p.Src == f.Src);

                // If there are enough UDP packets to make a code packet,
                // then we put the new code packet into array.
                if (actualCount >= frameCount)
                {
                    usedServers.Add(f.Src);
                    var cp = new CodePacket(Id, f.Src, f.Len);
                    codePackets.AddLast(cp);
                }
                else
                {
                    unusedFrames.AddLast(f);
                }
            }

            _arrivedFrames.Clear();
            foreach (var f in unusedFrames)
            {
                _arrivedFrames.Add(f);
            }
            return codePackets;
        }
    }
}
