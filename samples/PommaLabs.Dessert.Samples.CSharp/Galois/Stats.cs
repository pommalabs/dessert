﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using PommaLabs.Dessert.Recording;

namespace PommaLabs.Dessert.Samples.CSharp.Galois
{
    internal sealed class Stats
    {
        private static readonly Tally s_clientRequestsTally = Sim.Tally();
        private static readonly Tally s_clientTimeWaitedTally = Sim.Tally();
        private static readonly Tally s_memoryUsedTally = Sim.Tally();
        private static readonly Tally s_reconstructedFilesTally = Sim.Tally();
        private static readonly Tally s_lostSwitchMessagesTally = Sim.Tally();
        private readonly Tally _clientRequestsTmpTally = Sim.Tally();
        private readonly Tally _clientTimeWaitedTmpTally = Sim.Tally();
        private readonly Tally _memoryUsedTmpTally = Sim.Tally();
        private int _lostSwitchMessages;
        private int _reconstructedFiles;

        public double ClientRequestsAvg()
        {
            return _clientRequestsTmpTally.Mean();
        }

        public static double ClientRequestsTotalAvg()
        {
            return s_clientRequestsTally.Mean();
        }

        public void AddClientRequests(int n)
        {
            s_clientRequestsTally.Observe(n);
            _clientRequestsTmpTally.Observe(n);
        }

        public double ClientTimeWaitedAvg()
        {
            return _clientTimeWaitedTmpTally.Mean();
        }

        public static double ClientTimeWaitedTotalAvg()
        {
            return s_clientTimeWaitedTally.Mean();
        }

        public void AddClientTimeWaited(double t)
        {
            s_clientTimeWaitedTally.Observe(t);
            _clientTimeWaitedTmpTally.Observe(t);
        }

        public int MemoryUsedAvg()
        {
            return (int)_memoryUsedTmpTally.Mean();
        }

        public static int MemoryUsedTotalAvg()
        {
            var res = (int)s_memoryUsedTally.Mean();
            s_memoryUsedTally.Reset();
            return res;
        }

        public void AddMemoryUsed(double m)
        {
            s_memoryUsedTally.Observe(m);
            _memoryUsedTmpTally.Observe(m);
        }

        public int ReconstructedFiles()
        {
            return _reconstructedFiles;
        }

        public int LostSwitchMessages()
        {
            return _lostSwitchMessages;
        }

        public static int ReconstructedFilesAvg()
        {
            return (int)s_reconstructedFilesTally.Mean();
        }

        public static int LostSwitchMessagesAvg()
        {
            return (int)s_lostSwitchMessagesTally.Mean();
        }

        public void FileReconstructed()
        {
            _reconstructedFiles++;
        }

        public void MessageLost()
        {
            _lostSwitchMessages++;
        }

        /// <summary>
        ///   Called when each simulation has ended.
        /// </summary>
        public void Reset()
        {
            s_reconstructedFilesTally.Observe(_reconstructedFiles);
            s_lostSwitchMessagesTally.Observe(_lostSwitchMessages);
        }

        /// <summary>
        ///   Called when all simulations have ended.
        /// </summary>
        public static void ResetMon()
        {
            s_clientRequestsTally.Reset();
            s_clientTimeWaitedTally.Reset();
            // MemoryUsedTally.Reset();
            // It has not to be reset, since it collects data for all simulations.
            s_reconstructedFilesTally.Reset();
            s_lostSwitchMessagesTally.Reset();
        }
    }
}
