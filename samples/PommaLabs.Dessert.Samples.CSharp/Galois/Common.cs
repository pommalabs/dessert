﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using PommaLabs.Dessert.Resources;
using Serilog;

namespace PommaLabs.Dessert.Samples.CSharp.Galois
{
    internal enum PacketType
    {
        Answer,
        Request
    }

    internal sealed class G
    {
        // Quantities
        public const short SimCount = 20;

        // Sizes (in bytes)
        public const int FileSize = 64 * 1024;
        public const int MTU = 1024;
        public const int RequestSize = 64;

        // Probabilities (between 0 and 1)
        public const double ClientSleepProb = 0.3;
        public const double ServerDownProb = 0.6;
        public static short RequestCount = 8;
        public static short MachineCount = 16;
        public static short FrameCount = 128;

        // Times
        public static readonly double MaxSimTime;
        public static readonly double MemoryRecordingFrequency;
        public static readonly double ClientStopMean;
        public static readonly double ServerStopMean;
        public static readonly double Timeout;
        public static readonly double CacheAccessTime;
        public static readonly double MaxAccessTime;
        public static readonly double Latency;
        public static readonly double Bandwidth;

        public static int ExtraRequestCount; // Between 0 and (MachineCount - RequestCount - 1)

        public readonly ClientOS[] ClientOSes = new ClientOS[MachineCount];
        public readonly Client[] Clients = new Client[MachineCount];
        public readonly ServerOS[] ServerOSes = new ServerOS[MachineCount];
        public readonly Server[] Servers = new Server[MachineCount];
        public readonly Stats Stats = new Stats();
        public Switch _switch;

        static G()
        {
            Sim.CurrentTimeUnit = TimeUnit.Microsecond;
            MaxSimTime = 10.Seconds();
            MemoryRecordingFrequency = MaxSimTime / 10;
            ClientStopMean = 25.Milliseconds(); // Exponential
            ServerStopMean = 15.Milliseconds(); // Exponential
            Timeout = 1.5.Milliseconds(); // Fixed
            CacheAccessTime = 100.Microseconds(); // Fixed
            MaxAccessTime = 1.Microseconds(); // Fixed
            Latency = 100.Nanoseconds(); // Fixed
            Bandwidth = 11 * 1024 * 1024 / 1.Seconds(); // Bandwidth MB/s = 11 * 1024^2 B / 1 sec
        }

        public static void SetParameters(short machineCount, short frameCount)
        {
            MachineCount = machineCount;
            RequestCount = (short)(machineCount / 2);
            FrameCount = frameCount;
        }
    }

    internal abstract class Entity
    {
        protected readonly SimEnvironment Env;
        protected readonly G G;
        protected ILogger _logger;

        protected Entity(SimEnvironment env, G g)
        {
            Env = env;
            G = g;
        }

        public void SetLogger(string entityName)
        {
            _logger = new LoggerConfiguration()
                .Enrich.WithProperty("EntityName", entityName)
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] #{EntityName} {Message:lj}{NewLine}{Exception}")
                .CreateLogger();
        }

        public static string Ts(double waitTime)
        {
            return string.Format("{0:.###}", waitTime);
        }

        protected IEnumerable<SimEvent> WaitForSend(object packet, int packetLen)
        {
            var w = G.Latency + packetLen / G.Bandwidth;
            _logger.Information("Sending {Packet}, waiting for {Wait}", packet, Ts(w));
            yield return Env.Timeout(w);
        }
    }

    internal abstract class BaseOS : Entity
    {
        protected readonly int Id;
        protected readonly Store<UdpPacket> IncomingFrames;

        protected BaseOS(SimEnvironment env, G g, int osId) : base(env, g)
        {
            Id = osId;
            IncomingFrames = Sim.Store<UdpPacket>(env);
        }

        public void Receive(UdpPacket p)
        {
            IncomingFrames.Put(p);
        }
    }
}
