﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp.Galois
{
    internal sealed class Switch : Entity
    {
        private readonly Store<UdpPacket> _frames;

        public Switch(SimEnvironment env, G g) : base(env, g)
        {
            _frames = Sim.Store<UdpPacket>(env, G.FrameCount);
        }

        public IEnumerable<SimEvent> Run()
        {
            while (true)
            {
                var getFrame = _frames.Get();
                yield return getFrame;
                // Switch is awakened again by the Receive method.
                var p = getFrame.Value;
                yield return Env.Call(WaitForSend(p, p.Len));
                Send(p);
            }
        }

        public void Receive(UdpPacket packet)
        {
            if (_frames.Count == G.FrameCount)
            {
                _logger.Information("Lost a packet");
                G.Stats.MessageLost();
            }
            else
            {
                _logger.Information("Packet {Packet} incoming", packet);
                _frames.Put(packet);
            }
        }

        public override string ToString()
        {
            return string.Format("Switch(UsedFrames: {0})", _frames.Count);
        }

        private void Send(UdpPacket packet)
        {
            if (packet.Type == PacketType.Request)
            {
                G.ServerOSes[packet.Dst].Receive(packet);
            }
            else
            {
                G.ClientOSes[packet.Dst].Receive(packet);
            }
        }
    }
}
