﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;
using System.Diagnostics;

namespace PommaLabs.Dessert.Samples.CSharp.Galois
{
    internal static class MemoryRecorder
    {
        public static IEnumerable<SimEvent> Run(SimEnvironment env, Stats stats)
        {
            var currProc = Process.GetCurrentProcess();
            while (true)
            {
                yield return env.Timeout(G.MemoryRecordingFrequency);
                currProc.Refresh();
                var procMemInMb = currProc.WorkingSet64 / (1024 * 1024);
                stats.AddMemoryUsed(procMemInMb);
            }
        }
    }
}
