﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using Bogus;
using PommaLabs.Dessert.Recording;
using PommaLabs.Dessert.Resources;
using Serilog;

namespace PommaLabs.Dessert.Samples.CSharp
{
    /// <summary>
    ///   Simulation is based on a very interesting CodeProject article:
    ///   https://www.codeproject.com/Articles/1111093/Discrete-Event-Simulation-using-R-Hospital-Capacit
    ///
    ///   Simulation described in the article has not been fully reimplemented;
    ///   following example shows how easier to read the simulation code is
    ///   when written with the iterator pattern.
    /// </summary>
    public static class HospitalCapacityPlanning
    {
        private static readonly int s_numberOfBeds;
        private static readonly double s_period;
        private static readonly double s_interArrivalTime;

        static HospitalCapacityPlanning()
        {
            Sim.CurrentTimeUnit = TimeUnit.Day;
            s_numberOfBeds = 30;
            s_period = 730.Days();
            s_interArrivalTime = (730.0 / 200.0).Days(); // 200 patients over the whole period.
        }

        private static IEnumerable<SimEvent> BedMonitor(SimEnvironment env, Stats stats, Resource beds)
        {
            while (true)
            {
                stats.UsedBeds.Observe(beds.Count);
                Log.Verbose("Number of used beds is {UsedBedCount} at {Now:0.00}", beds.Count, env.Now);
                yield return env.Timeout(1.Days());
            }
        }

        private static IEnumerable<SimEvent> PatientGenerator(SimEnvironment env, Stats stats)
        {
            Randomizer.Seed = new Random((int)env.Random.Seed);

            var beds = Sim.Resource(env, capacity: s_numberOfBeds);
            env.Process(BedMonitor(env, stats, beds));

            while (true)
            {
                var faker = new Faker(locale: "en");
                var patientName = faker.Person.FullName;

                env.Process(Patient(env, stats, patientName, beds));
                stats.PatientsGenerated.Observe(1);
                Log.Verbose("New patient {PatientName} generated at {Now:0.00}", patientName, env.Now);

                var interArrivalTime = env.Random.Exponential(1 / s_interArrivalTime).Days();
                yield return env.Timeout(interArrivalTime);
            }
        }

        private static IEnumerable<SimEvent> Patient(SimEnvironment env, Stats stats, string patientName, Resource beds)
        {
            using (var bed = beds.Request(priority: 1))
            {
                yield return bed;
                stats.Phase1In.Observe(1);
                Log.Verbose("Patient {PatientName} receives a bed at {Now:0.00} and enters Phase 1", patientName, env.Now);

                var phase1Duration = env.Random.Normal(31, 2).Days();
                Log.Verbose("Patient {PatientName} will remain in Phase 1 for {Phase1Duration:0.00} days", patientName, phase1Duration);
                yield return env.Timeout(phase1Duration);
            }

            var shouldContinueToPhase2 = env.Random.NextDouble() < 0.9;
            if (!shouldContinueToPhase2)
            {
                stats.GoneHomeAfterPhase1.Observe(1);
                Log.Verbose("Patient {PatientName} can go home from Phase 1 at {Now:0.00}", patientName, env.Now);
                yield break;
            }

            using (var bed = beds.Request(priority: 8))
            {
                yield return bed;
                stats.Phase2In.Observe(1);
                Log.Verbose("Patient {PatientName} receives a bed at {Now:0.00} and enters Phase 2", patientName, env.Now);

                var phase2Duration = env.Random.Normal(28, 2).Days();
                Log.Verbose("Patient {PatientName} will remain in Phase 2 for {Phase1Duration:0.00} days", patientName, phase2Duration);
                yield return env.Timeout(phase2Duration);
            }

            var shouldContinueToPhase3 = env.Random.NextDouble() < 0.81;
            if (!shouldContinueToPhase3)
            {
                stats.GoneHomeAfterPhase2.Observe(1);
                Log.Verbose("Patient {PatientName} can go home from Phase 2 at {Now:0.00}", patientName, env.Now);
                yield break;
            }

            using (var bed = beds.Request(priority: 9))
            {
                yield return bed;
                stats.Phase3In.Observe(1);
                Log.Verbose("Patient {PatientName} receives a bed at {Now:0.00} and enters Phase 3", patientName, env.Now);

                var phase3Duration = env.Random.Normal(25, 2).Days();
                Log.Verbose("Patient {PatientName} will remain in Phase 3 for {Phase1Duration:0.00} days", patientName, phase3Duration);
                yield return env.Timeout(phase3Duration);
            }
        }

        public static void Run()
        {
            var env = Sim.Environment();

            var stats = new Stats
            {
                GoneHomeAfterPhase1 = Sim.Tally(env),
                GoneHomeAfterPhase2 = Sim.Tally(env),
                PatientsGenerated = Sim.Tally(env),
                Phase1In = Sim.Tally(env),
                Phase2In = Sim.Tally(env),
                Phase3In = Sim.Tally(env),
                UsedBeds = Sim.Tally(env),
            };

            env.Process(PatientGenerator(env, stats));
            env.Run(until: s_period);

            Log.Information("Treatment results:");
            Log.Information("Number of beds: {NumberOfBeds}", s_numberOfBeds);
            Log.Information("Average bed usage: {AverageBedUsage:0.0}", stats.UsedBeds.Mean());
            Log.Information("Patients generated: {PatientCount}", stats.PatientsGenerated.Total());
            Log.Information("Patients which entered Phase 1: {PatientCount}", stats.Phase1In.Total());
            Log.Information("Patients which went home after Phase 1: {PatientCount}", stats.GoneHomeAfterPhase1.Total());
            Log.Information("Patients which entered Phase 2: {PatientCount}", stats.Phase2In.Total());
            Log.Information("Patients which went home after Phase 2: {PatientCount}", stats.GoneHomeAfterPhase2.Total());
            Log.Information("Patients which entered Phase 3: {PatientCount}", stats.Phase3In.Total());
        }

        private struct Stats
        {
            public IRecorder GoneHomeAfterPhase1 { get; set; }
            public IRecorder GoneHomeAfterPhase2 { get; set; }
            public IRecorder PatientsGenerated { get; set; }
            public IRecorder Phase1In { get; set; }
            public IRecorder Phase2In { get; set; }
            public IRecorder Phase3In { get; set; }
            public IRecorder UsedBeds { get; set; }
        }
    }
}
