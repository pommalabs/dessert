﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class EventFailure
    {
        private static IEnumerable<SimEvent> EventFailer(SimEvent<string> ev)
        {
            ev.Fail("SOMETHING BAD HAPPENED");
            yield break;
        }

        private static IEnumerable<SimEvent> Process(SimEnvironment env)
        {
            var ev = env.Event<string>();
            env.Process(EventFailer(ev));
            yield return ev;
            if (ev.Failed)
            {
                Console.WriteLine(ev.Value);
            }
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Process(env));
            env.Run();
        }
    }
}
