﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class DelayedStart
    {
        private static IEnumerable<SimEvent> Process(SimEnvironment env, char id)
        {
            Console.WriteLine("{0}: {1}", id, env.Now);
            yield break;
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.DelayedProcess(Process(env, 'A'), 7);
            env.Process(Process(env, 'B'));
            env.Run();
        }
    }
}
