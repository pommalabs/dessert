﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using SimEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.SimPy2
{
    public static class Customer
    {
        public static SimEvents Buy(SimEnvironment env, string name, double budget = 40)
        {
            Console.WriteLine("Here I am at the shop {0}", name);
            for (var i = 0; i < 4; ++i)
            {
                yield return env.Timeout(5); // Executed 4 times at intervals of 5 time units
                Console.WriteLine("I just bought something {0}", name);
                budget -= 10;
            }
            Console.WriteLine("All I have left is {0} I am going home {1}", budget, name);
        }

        // Expected output:
        // Starting simulation
        // Here I am at the shop Marta
        // I just bought something Marta
        // I just bought something Marta
        // I just bought something Marta
        // I just bought something Marta
        // All I have left is 60 I am going home Marta
        // Current time is 30
        public static void Main()
        {
            var env = Sim.Environment();
            env.DelayedProcess(Buy(env, "Marta", 100), delay: 10);
            Console.WriteLine("Starting simulation");
            env.Run(until: 100);
            Console.WriteLine("Current time is {0}", env.Now);
        }
    }
}
