﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using SimEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.SimPy2
{
    public static class Source
    {
        private static SimEvents Execute(SimEnvironment env, int finish)
        {
            while (env.Now < finish)
            {
                // Creates a new Customer and activates it (with default parameters).
                env.Process(Customer.Buy(env, "Marta"));
                Console.WriteLine("{0} {1}", env.Now, "Marta");
                yield return env.Timeout(10);
            }
        }

        public static void Main()
        {
            var env = Sim.Environment();
            env.Process(Execute(env, 33));
            env.Run(until: 100);
        }
    }
}
