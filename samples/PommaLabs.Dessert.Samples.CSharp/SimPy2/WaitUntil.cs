﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using PommaLabs.Dessert.Events;
using SimEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.SimPy2
{
    public sealed class Player
    {
        public Player(int lives = 1, string name = "ImaTarget")
        {
            Name = name;
            Lives = lives;
            Damage = 0;
        }

        public string Name { get; private set; }
        public string Message { get; private set; }
        public SimEvent<object> LifeEvent { get; private set; }
        public int Lives { get; private set; }
        public int Damage { get; set; }

        public SimEvents Life(SimEnvironment env)
        {
            LifeEvent = env.Event();
            Message = "Drat! Some " + Name + " survived Federation attack!";
            while (true)
            {
                yield return LifeEvent;
                Lives -= 1;
                Damage = 0;
                if (Lives > 0)
                {
                    continue;
                }
                Message = string.Format("{0} wiped out by Federation at {1}", Name, env.Now);
                //sim.Stop();
            }
        }
    }

    public sealed class Federation
    {
        private readonly Random _random = new Random();

        public SimEvents Fight(SimEnvironment env, Player target)
        {
            Console.WriteLine("Three {0} attempting to escape!", target.Name);
            while (true)
            {
                if (_random.Next(0, 10) < 2)
                {
                    // Checks for hit on player
                    target.Damage += 1; // Hit! Increment damage to player
                    if (target.Damage <= 5) // Target survives...
                    {
                        Console.WriteLine("Ha! {0} hit! Damage = {1}", target.Name, target.Damage);
                    }
                    else
                    {
                        target.LifeEvent.Succeed();
                        if (target.Lives - 1 == 0)
                        {
                            Console.WriteLine("No more {0} left!", target.Name);
                        }
                        else
                        {
                            Console.WriteLine("Now only {0} {1} left!", target.Lives - 1, target.Name);
                        }
                    }
                }
                yield return env.Timeout(1);
            }
        }
    }

    public static class WaitUntil
    {
        public static void Main()
        {
            var sim = Sim.Environment();
            const int GameOver = 100;
            // Creates a Player object named "Romulans"
            var target = new Player(lives: 3, name: "Romulans");
            sim.Process(target.Life(sim));
            // Creates a Federation object
            var shooter = new Federation();
            sim.Process(shooter.Fight(sim, target));
            sim.Run(until: GameOver);
            Console.WriteLine(target.Message);
        }
    }
}
