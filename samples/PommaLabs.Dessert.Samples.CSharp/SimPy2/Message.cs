﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using SimEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.SimPy2
{
    public static class Message
    {
        private static SimEvents Go(SimEnvironment env, int id)
        {
            Console.WriteLine("{0} {1} Starting", env.Now, id);
            yield return env.Timeout(100);
            Console.WriteLine("{0} {1} Arrived", env.Now, id);
        }

        // Expected output:
        // Starting simulation
        // 0 1 Starting
        // 6 2 Starting
        // 100 1 Arrived
        // 106 2 Arrived
        // Current time is 106 
        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Go(env, 1));
            env.DelayedProcess(Go(env, 2), delay: 6);
            Console.WriteLine("Starting simulation");
            env.Run(until: 200);
            Console.WriteLine("Current time is {0}", env.Now);
        }
    }
}
