﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using IEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.SimPy2
{
    public static class BusBreakdown
    {
        private static SimProcess s_breakBus;

        private static IEvents OperateBus(SimEnvironment env, double repairDuration, double tripLength)
        {
            var tripLeft = tripLength;
            while (tripLeft > 0)
            {
                var startTime = env.Now;
                yield return env.Timeout(tripLeft);
                if (!env.ActiveProcess.Interrupted(out var value))
                {
                    break; // No more breakdowns, bus finished trip
                }
                Console.WriteLine("{0} at {1}", value, env.Now);
                tripLeft -= env.Now - startTime;
                s_breakBus.Resume(delay: repairDuration);
                yield return env.Timeout(repairDuration);
                Console.WriteLine("Bus repaired at {0}", env.Now);
            }
            Console.WriteLine("Bus has arrived at {0}", env.Now);
        }

        private static IEvents BreakBus(SimEnvironment env, SimProcess bus, double interval)
        {
            while (true)
            {
                yield return env.Timeout(interval);
                if (bus.Succeeded)
                {
                    break;
                }
                bus.Interrupt("Breakdown Bus");
                yield return env.Suspend();
            }
        }

        // Expected output:
        // Breakdown Bus at 300
        // Bus repaired at 320
        // Breakdown Bus at 620
        // Bus repaired at 640
        // Breakdown Bus at 940
        // Bus repaired at 960
        // Bus has arrived at 1060
        // Dessert: No more events at time 1260
        public static void Main()
        {
            var env = Sim.Environment();
            var bus = env.Process(OperateBus(env, repairDuration: 20, tripLength: 1000));
            s_breakBus = env.Process(BreakBus(env, bus, interval: 300));
            env.Run(until: 4000);
            Console.WriteLine("Dessert: No more events at time {0}", env.Now);
        }
    }
}
