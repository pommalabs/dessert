﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using IEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.RealTime
{
    public static class SayHelloInRealTime
    {
        private static IEvents SayHello(SimEnvironment env, char id)
        {
            while (true)
            {
                Console.WriteLine("{0} - Sleeping at {1}, real {2}...", id, env.Now, env.RealTime.WallClock.GetCurrentInstant());
                yield return env.Timeout(3);
                Console.WriteLine("{0} - Awake at {1}, real {2}", id, env.Now, env.RealTime.WallClock.GetCurrentInstant());
            }
        }

        public static void Run()
        {
            Console.WriteLine("Hello World real-time simulation :)");
            var env = Sim.RealTimeEnvironment(21);
            env.Process(SayHello(env, 'A'));
            env.DelayedProcess(SayHello(env, 'B'), 1);
            env.Run(9.5);
        }
    }
}
