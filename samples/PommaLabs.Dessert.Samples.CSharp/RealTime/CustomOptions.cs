﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using IEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp.RealTime
{
    public static class CustomOptions
    {
        private static IEvents ShowOptions(SimEnvironment env, char id)
        {
            while (true)
            {
                Console.WriteLine("{0} - Sleeping at {1}, real {2}...", id, env.Now, env.RealTime.WallClock.GetCurrentInstant());
                yield return env.Timeout(3);
                Console.WriteLine("{0} - Awake at {1}, real {2}", id, env.Now, env.RealTime.WallClock.GetCurrentInstant());
            }
        }

        public static void Run()
        {
            Console.WriteLine("Custom real-time options");
            var env = Sim.RealTimeEnvironment(21, new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = 3.0, // Each time unit lasts 3 seconds.
            });
            env.Process(ShowOptions(env, 'A'));
            env.DelayedProcess(ShowOptions(env, 'B'), 1);
            env.Run(9.5);
        }
    }
}
