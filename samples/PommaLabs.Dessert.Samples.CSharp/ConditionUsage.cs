﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class ConditionUsage
    {
        private static IEnumerable<SimEvent> Process(SimEnvironment env)
        {
            var t1 = env.Timeout(3);
            var t2 = env.Timeout(7);
            var c1 = env.AllOf(t1, t2);
            yield return c1;
            Console.WriteLine(env.Now); // 7
            Console.WriteLine(c1.Value.Contains(t1)); // True
            Console.WriteLine(c1.Value.Contains(t2)); // True

            t1 = env.Timeout(3);
            t2 = env.Timeout(7);
            var c2 = env.Condition(t1, t2, c => c.Ev1.Succeeded || c.Ev2.Succeeded);
            yield return c2;
            Console.WriteLine(env.Now); // 10
            Console.WriteLine(c2.Value.Contains(t1)); // True
            Console.WriteLine(c2.Value.Contains(t2)); // False
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Process(env));
            env.Run();
        }
    }
}
