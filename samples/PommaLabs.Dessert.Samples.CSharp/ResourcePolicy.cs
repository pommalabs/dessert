﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class ResourcePolicy
    {
        private static IEnumerable<SimEvent> Getter(Store<int> store, char id)
        {
            var getEv = store.Get();
            yield return getEv;
            Console.WriteLine("{0}: {1}", id, getEv.Value);
        }

        public static void Run()
        {
            var env = Sim.Environment(seed: 21);
            const int Capacity = 10;
            var store = Sim.Store<int>(env, Capacity, WaitPolicy.FIFO, WaitPolicy.FIFO, WaitPolicy.Random);
            for (var i = 0; i < Capacity; ++i)
            {
                store.Put(i);
            }
            for (var i = 0; i < Capacity; ++i)
            {
                env.Process(Getter(store, (char)('A' + i)));
            }
            env.Run();
        }
    }
}
