﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class InterruptHandling
    {
        private static IEnumerable<SimEvent> Interrupter(SimProcess victim)
        {
            victim.Interrupt("NOW");
            yield break;
        }

        private static IEnumerable<SimEvent> Process(SimEnvironment env)
        {
            yield return env.Process(Interrupter(env.ActiveProcess));
            if (env.ActiveProcess.Interrupted(out var cause))
            {
                Console.WriteLine("Interrupted at: " + cause);
            }
            // Following instructions are commented, because
            // they would result in an exception. In fact,
            // second interrupt would be uncaught.
            //
            // yield return env.Start(Interrupter(env.ActiveProcess));
            // yield return env.Timeout(5);
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Process(env));
            env.Run();
        }
    }
}
