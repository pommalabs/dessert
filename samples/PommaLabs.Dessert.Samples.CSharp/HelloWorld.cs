﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using IEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class HelloWorld
    {
        private static IEvents SayHello(SimEnvironment env)
        {
            while (true)
            {
                yield return env.Timeout(2.1);
                Console.WriteLine("Hello World at {0:0.0}!", env.Now);
            }
        }

        // Expected output:
        // Hello World simulation :)
        // Hello World at 2.1!
        // Hello World at 4.2!
        // Hello World at 6.3!
        // Hello World at 8.4!
        public static void Run()
        {
            Console.WriteLine("Hello World simulation :)");
            var env = Sim.Environment();
            env.Process(SayHello(env));
            env.Run(10);
        }
    }
}
