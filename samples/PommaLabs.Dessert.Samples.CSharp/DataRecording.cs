﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Recording;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class DataRecording
    {
        private static IEnumerable<SimEvent> Person(SimEnvironment env, Resource queue, Tally rec)
        {
            using var req = queue.Request();
            var startTime = env.Now;
            yield return req;
            rec.Observe(env.Now - startTime);
            var workTime = env.Random.Next(3, 10);
            yield return env.Timeout(workTime);
        }

        private static IEnumerable<SimEvent> Spawner(SimEnvironment env, Tally rec)
        {
            var queue = Sim.Resource(env, 2);
            while (true)
            {
                env.Process(Person(env, queue, rec));
                yield return env.Timeout(env.Random.Next(2, 5));
            }
        }

        public static void Run()
        {
            var env = Sim.Environment(seed: 42);
            var rec = Sim.Tally(env);
            env.Process(Spawner(env, rec));
            env.Run(2 * 60);
            Console.WriteLine("Total clients: {0}", rec.Total());
            Console.WriteLine("Average wait: {0:.0}", rec.Mean());
        }
    }
}
