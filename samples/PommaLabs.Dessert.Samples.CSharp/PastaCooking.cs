﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class PastaCooking
    {
        private static readonly double s_avgCookTime;
        private static readonly double s_stdCookTime;
        private static readonly double s_simTime;

        static PastaCooking()
        {
            Sim.CurrentTimeUnit = TimeUnit.Minute;
            s_avgCookTime = 10.Minutes();
            s_stdCookTime = 1.Minutes();
            s_simTime = 50.Minutes();
        }

        private static IEnumerable<SimEvent> PastaCook(SimEnvironment env)
        {
            while (true)
            {
                var cookTime = env.Random.Normal(s_avgCookTime, s_stdCookTime);
                Console.WriteLine("Pasta in cottura per {0:0.0} minuti", cookTime);
                yield return env.Timeout(cookTime);
                if (cookTime < s_avgCookTime - s_stdCookTime)
                {
                    Console.WriteLine("Pasta poco cotta!");
                }
                else if (cookTime > s_avgCookTime + s_stdCookTime)
                {
                    Console.WriteLine("Pasta troppo cotta...");
                }
                else
                {
                    Console.WriteLine("Pasta ben cotta!!!");
                }
            }
        }

        public static void Run()
        {
            var env = Sim.Environment(21);
            env.Process(PastaCook(env));
            env.Run(s_simTime);
        }
    }
}
