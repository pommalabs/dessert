﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Dessert.Samples.CSharp.SimPy3
{
    public static class ClockExample
    {
        private static IEnumerable<SimEvent> Clock(SimEnvironment env, string name, double tick)
        {
            while (true)
            {
                Console.WriteLine("{0} {1:0.0}", name, env.Now);
                yield return env.Timeout(tick);
            }
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(Clock(env, "fast", 0.5));
            env.Process(Clock(env, "slow", 1.0));
            env.Run(until: 2);
        }
    }
}
