﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using PommaLabs.Dessert.Events;
using PommaLabs.Dessert.Resources;
using Troschuetz.Random;
using IEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

#region Original SimPy3 Example

//"""
//Movie renege example

//Covers:

//- Resources: Resource
//- Condition events
//- Shared events

//Scenario:
//  A movie theatre has one ticket counter selling tickets for three
//  movies (next show only). When a movie is sold out, all people waiting
//  to buy tickets for that movie renege (leave queue).

//"""
//import collections
//import random

//import simpy

//RANDOM_SEED = 42
//TICKETS = 50  # Number of tickets per movie
//SIM_TIME = 120  # Run until

//def moviegoer(env, movie, num_tickets, theater):
//    """A moviegoer tries to by a number of tickets (*num_tickets*) for
//    a certain *movie* in a *theater*.

//    If the movie becomes sold out, she leaves the theater. If she gets
//    to the counter, she tries to buy a number of tickets. If not enough
//    tickets are left, she argues with the teller and leaves.

//    If at most one ticket is left after the moviegoer bought her
//    tickets, the *sold out* event for this movie is triggered causing
//    all remaining moviegoers to leave.

//    """
//    with theater.counter.request() as my_turn:
//        # Wait until its our turn or until the movie is sold out
//        result = yield my_turn | theater.sold_out[movie]

//        # Check if it's our turn of if movie is sold out
//        if my_turn not in result:
//            theater.num_renegers[movie] += 1
//            env.exit()

//        # Check if enough tickets left.
//        if theater.available[movie] < num_tickets:
//            # Moviegoer leaves after some discussion
//            yield env.timeout(0.5)
//            env.exit()

//        # Buy tickets
//        theater.available[movie] -= num_tickets
//        if theater.available[movie] < 2:
//            # Trigger the "sold out" event for the movie
//            theater.sold_out[movie].succeed()
//            theater.when_sold_out[movie] = env.now
//            theater.available[movie] = 0
//        yield env.timeout(1)

//def customer_arrivals(env, theater):
//    """Create new *moviegoers* until the sim time reaches 120."""
//    while True:
//        yield env.timeout(random.expovariate(1 / 0.5))

//        movie = random.choice(theater.movies)
//        num_tickets = random.randint(1, 6)
//        if theater.available[movie]:
//            env.start(moviegoer(env, movie, num_tickets, theater))

//Theater = collections.namedtuple('Theater', 'counter, movies, available, '
//                                            'sold_out, when_sold_out, '
//                                            'num_renegers')

//# Setup and start the simulation
//print('Movie renege')
//random.seed(RANDOM_SEED)
//env = simpy.Environment()

//# Create movie theater
//counter = simpy.Resource(env, capacity=1)
//movies = ['Python Unchained', 'Kill Process', 'Pulp Implementation']
//available = {movie: TICKETS for movie in movies}
//sold_out = {movie: env.event() for movie in movies}
//when_sold_out = {movie: None for movie in movies}
//num_renegers = {movie: 0 for movie in movies}
//theater = Theater(counter, movies, available, sold_out, when_sold_out,
//                  num_renegers)

//# Start process and simulate
//env.start(customer_arrivals(env, theater))
//simpy.simulate(env, until=SIM_TIME)

//# Analysis/results
//for movie in movies:
//    if theater.sold_out[movie]:
//        print('Movie "%s" sold out %.1f minutes after ticket counter '
//              'opening.' % (movie, theater.when_sold_out[movie]))
//        print('  Number of people leaving queue when film sold out: %s' %
//              theater.num_renegers[movie])

#endregion

namespace PommaLabs.Dessert.Samples.CSharp.SimPy3
{
    public static class MovieRenege
    {
        private const int RandomSeed = 63;
        private const int Tickets = 50;
        private const int SimTime = 120;
        private static SimEnvironment s_env;
        private static Theater s_theater;

        /// <summary>
        ///   A moviegoer tries to by a number of tickets (<paramref name="ticketCount"/>)
        ///   for a certain <paramref name="movie"/> in a <see cref="s_theater"/>.
        ///   If the movie becomes sold out, she leaves the theater. If she gets
        ///   to the counter, she tries to buy a number of tickets. If not enough
        ///   tickets are left, she argues with the teller and leaves.
        ///   If at most one ticket is left after the moviegoer bought her
        ///   tickets, the sold out event for this movie is triggered causing
        ///   all remaining moviegoers to leave.
        /// </summary>
        private static IEvents MovieGoer(MovieInfo movie, int ticketCount)
        {
            using var myTurn = s_theater.Counter.Request();

            // Wait until its our turn or until the movie is sold out
            yield return myTurn.Or(movie.SoldOut);

            // Check if it's our turn or if movie is sold out
            if (!myTurn.Succeeded)
            {
                movie._renegerCount++;
                yield break;
            }

            // Check if there are enough tickets left
            if (movie._available < ticketCount)
            {
                // Moviegoer leaves after some discussion
                yield return s_env.Timeout(0.5);
                yield break;
            }

            // Buy tickets
            movie._available -= ticketCount;
            if (movie._available < 2 && !movie.SoldOut.Succeeded)
            {
                // Trigger the "sold out" event for the movie
                movie.SoldOut.Succeed();
                movie._whenSoldOut = s_env.Now;
                movie._available = 0;
            }

            yield return s_env.Timeout(1);
        }

        /// <summary>
        ///   Creates new <see cref="MovieGoer"/> processes
        ///   until the simulation time reaches <see cref="SimTime"/>.
        /// </summary>
        private static IEvents CustomerArrivals()
        {
            while (true)
            {
                yield return s_env.Timeout(s_env.Random.Exponential(1.0 / 0.5));
                var movie = s_env.Random.Choice(s_theater.Movies);
                if (movie._available > 0)
                {
                    var ticketCount = s_env.Random.Next(1, 7); // Possible outputs: 1, 2, 3, 4, 5, 6
                    s_env.Process(MovieGoer(movie, ticketCount));
                }
            }
        }

        public static void Run()
        {
            // Sets up and starts simulation
            Console.WriteLine("Movie renege");
            s_env = Sim.Environment(RandomSeed);

            // Creates movie theater
            var counter = Sim.Resource(s_env, 1);
            var titles = new[] { ".NET Unchained", "Kill Process", "Pulp Implementation" };
            var movies = new List<MovieInfo>(titles.Length);
            movies.AddRange(titles.Select(t => new MovieInfo(t, s_env.Event())));
            s_theater = new Theater(counter, movies);

            // Starts process and simulates
            s_env.Process(CustomerArrivals());
            s_env.Run(until: SimTime);

            // Analysis and results
            foreach (var movie in movies.Where(m => m.SoldOut.Succeeded))
            {
                Console.WriteLine("Movie \"{0}\" sold out {1:.0} minutes after ticket counter opening.", movie.Title,
                                  movie._whenSoldOut);
                Console.WriteLine("  Number of people leaving queue when film sold out: {0}", movie._renegerCount);
            }
        }

        private sealed class MovieInfo
        {
            public readonly SimEvent<object> SoldOut;
            public readonly string Title;
            public int _available;
            public int _renegerCount;
            public double _whenSoldOut;

            public MovieInfo(string title, SimEvent<object> soldOut)
            {
                Title = title;
                _available = Tickets;
                SoldOut = soldOut;
            }
        }

        private sealed class Theater
        {
            public readonly Resource Counter;
            public readonly IList<MovieInfo> Movies;

            public Theater(Resource counter, IList<MovieInfo> movies)
            {
                Counter = counter;
                Movies = movies;
            }
        }
    }
}
