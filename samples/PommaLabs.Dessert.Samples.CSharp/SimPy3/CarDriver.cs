﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using SimEvents = System.Collections.Generic.IEnumerable<PommaLabs.Dessert.SimEvent>;

#region Original SimPy3 Example

//import simpy

//def driver(env, car):
//    yield env.timeout(3)
//    car.action.interrupt()

//class Car(object):
//    def __init__(self, env):
//        self.env = env
//        self.action = env.start(self.run())

//    def run(self):
//        while True:
//            print('Start parking and charging at %d' % env.now)
//            charge_duration = 5
//            # We may get interrupted while charging the battery
//            try:
//                yield env.start(self.charge(charge_duration))
//            except simpy.Interrupt:
//                # When we received an interrupt, we stop charging and
//                # switch to the "driving" state
//                print('Was interrupted. Hope, the battery is full enough ...')

//            print('Start driving at %d' % env.now)
//            trip_duration = 2
//            yield env.timeout(trip_duration)

//    def charge(self, duration):
//        yield self.env.timeout(duration)

//env = simpy.Environment()
//car = Car(env)
//env.start(driver(env, car))
//simpy.simulate(env, until=15)

#endregion

namespace PommaLabs.Dessert.Samples.CSharp.SimPy3
{
    public static class CarDriver
    {
        private static SimEvents Driver(SimEnvironment sim, Car car)
        {
            yield return sim.Timeout(3);
            car.Action.Interrupt();
        }

        public static void Run()
        {
            var sim = Sim.Environment();
            var car = new Car(sim);
            sim.Process(Driver(sim, car));
            sim.Run(until: 15);
        }

        private sealed class Car
        {
            public readonly SimProcess Action;
            private readonly SimEnvironment _env;

            public Car(SimEnvironment env)
            {
                _env = env;
                Action = env.Process(Start());
            }

            private SimEvents Start()
            {
                while (true)
                {
                    Console.WriteLine("Start parking and charging at {0}", _env.Now);
                    const int ChargeDuration = 5;
                    // We may get interrupted while charging the battery
                    yield return _env.Process(Charge(ChargeDuration));
                    if (_env.ActiveProcess.Interrupted())
                    {
                        Console.WriteLine("Was interrupted. Hope, the battery is full enough ...");
                    }
                    Console.WriteLine("Start driving at {0}", _env.Now);
                    const int TripDuration = 2;
                    yield return _env.Timeout(TripDuration);
                }
            }

            private SimEvents Charge(double duration)
            {
                yield return _env.Timeout(duration);
            }
        }
    }
}
