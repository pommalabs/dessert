﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Events;
using Troschuetz.Random;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class TargetShooting
    {
        private const double HitProb = 0.7;
        private const double SimTime = 100;
        private static readonly string[] s_targets = { "Alieno", "Pollo", "Unicorno" };

        private static Timeout<string> NewTarget(SimEnvironment env)
        {
            var delay = env.Random.DiscreteUniform(1, 20);
            var target = env.Random.Choice(s_targets);
            return env.Timeout(delay, target);
        }

        private static IEnumerable<SimEvent> Shooter(SimEnvironment env)
        {
            while (true)
            {
                var timeout = NewTarget(env);
                yield return timeout;
                var hit = env.Random.NextDouble();
                if (hit < HitProb)
                {
                    Console.WriteLine("{0}: {1} colpito, si!", env.Now, timeout.Value);
                }
                else
                {
                    Console.WriteLine("{0}: {1} mancato, no...", env.Now, timeout.Value);
                }
            }
        }

        public static void Run()
        {
            var env = Sim.Environment(21);
            env.Process(Shooter(env));
            env.Run(SimTime);
        }
    }
}
