﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Dessert.Resources;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class PublicToilet
    {
        private static readonly double s_avgPersonArrival;
        private static readonly double s_avgTimeInToilet;
        private static readonly double s_simTime;

        static PublicToilet()
        {
            Sim.CurrentTimeUnit = TimeUnit.Minute;
            s_avgPersonArrival = 1.Minutes();
            s_avgTimeInToilet = 5.Minutes();
            s_simTime = 10.Minutes();
        }

        private static IEnumerable<SimEvent> Person(SimEnvironment env, string gender, Resource toilet)
        {
            using var req = toilet.Request();
            yield return req;
            Console.WriteLine("{0:0.00}: {1} --> Bagno", env.Now, gender);
            yield return env.Timeout(env.Random.Exponential(1.0 / s_avgTimeInToilet));
            Console.WriteLine("{0:0.00}: {1} <-- Bagno", env.Now, gender);
        }

        private static IEnumerable<SimEvent> PersonGenerator(SimEnvironment env)
        {
            var womenToilet = Sim.Resource(env, 1);
            var menToilet = Sim.Resource(env, 1);
            var count = 0;
            while (true)
            {
                var rnd = env.Random.NextDouble();
                var gender = ((rnd < 0.5) ? "Donna" : "Uomo") + count++;
                var toilet = (rnd < 0.5) ? womenToilet : menToilet;
                Console.WriteLine("{0:0.00}: {1} in coda", env.Now, gender);
                env.Process(Person(env, gender, toilet));
                yield return env.Timeout(env.Random.Exponential(1.0 / s_avgPersonArrival));
            }
        }

        public static void Run()
        {
            var env = Sim.Environment(21);
            env.Process(PersonGenerator(env));
            env.Run(s_simTime);
        }
    }
}
