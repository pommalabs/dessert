﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using Troschuetz.Random;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class MachineLoad
    {
        private static readonly string[] s_tasks = { "A", "B", "C" };
        private static readonly double s_loadTime;
        private static readonly double s_workTime;
        private static readonly double s_simTime;

        static MachineLoad()
        {
            Sim.CurrentTimeUnit = TimeUnit.Minute;
            s_loadTime = 5.Minutes();
            s_workTime = 25.Minutes();
            s_simTime = 100.Minutes();
        }

        private static IEnumerable<SimEvent> Worker(SimEnvironment env)
        {
            Console.WriteLine("{0}: Carico la macchina...", env.Now);
            yield return env.Timeout(s_loadTime);
            env.Exit(env.Random.Choice(s_tasks));
        }

        private static IEnumerable<SimEvent> Machine(SimEnvironment env)
        {
            while (true)
            {
                var worker = env.Process(Worker(env));
                yield return worker;
                Console.WriteLine("{0}: Eseguo il comando {1}", env.Now, worker.Value);
                yield return env.Timeout(s_workTime);
            }
        }

        public static void Run()
        {
            var env = Sim.Environment(21);
            env.Process(Machine(env));
            env.Run(s_simTime);
        }
    }
}
