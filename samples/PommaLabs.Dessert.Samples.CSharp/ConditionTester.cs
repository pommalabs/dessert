﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using PommaLabs.Dessert.Events;

namespace PommaLabs.Dessert.Samples.CSharp
{
    public static class ConditionTester
    {
        private static IEnumerable<SimEvent> AProcess(SimEnvironment env)
        {
            yield return env.Timeout(7);
            env.Exit("VAL_P");
        }

        private static IEnumerable<SimEvent> CondTester(SimEnvironment env)
        {
            var aProc = env.Process(AProcess(env));
            var cond = env.AllOf(env.Timeout(5, "VAL_T"), aProc);
            yield return cond;
            Console.WriteLine("ALL: {0}", cond.Value.Select(x => x.Value).Aggregate((s1, s2) => s1 + ", " + s2));

            aProc = env.Process(AProcess(env));
            cond = env.AnyOf(env.Timeout(5, "VAL_T"), aProc);
            yield return cond;
            Console.WriteLine("ANY: {0}", cond.Value.Select(x => x.Value).Aggregate((s1, s2) => s1 + ", " + s2));

            aProc = env.Process(AProcess(env));
            var aTime = env.Timeout(5, "VAL_T");
            ConditionEval<Timeout<string>, SimProcess> pred =
                c => c.Ev1.Succeeded && c.Ev2.Succeeded && c.Ev1.Value.Equals("VAL_T") && c.Ev2.Value.Equals("VAL_P");
            cond = env.Condition(aTime, aProc, pred);
            yield return cond;
            Console.WriteLine("CUSTOM: {0}", cond.Value.Select(x => x.Value).Aggregate((s1, s2) => s1 + ", " + s2));
        }

        public static void Run()
        {
            var env = Sim.Environment();
            env.Process(CondTester(env));
            env.Run();
        }
    }
}
