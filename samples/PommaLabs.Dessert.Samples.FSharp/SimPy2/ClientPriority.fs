﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.SimPy2.ClientPriority

open System.Collections.Generic
open PommaLabs.Dessert
open PommaLabs.Dessert.Samples.FSharp.Utilities
open PommaLabs.Dessert.Resources

let inClients = List<string>()  // List with the clients ordered by their requests.
let outClients = List<string>() // List with the clients ordered by completion of service.
let servTime = 100.0

let getServed (env: SimEnvironment) name priority (myServer: Resource) = seq<SimEvent> {
    inClients.Add name
    printfn "%s requests 1 unit at time = %g" name env.Now
    let req = myServer.Request(priority)
    yield upcast req
    yield upcast env.Timeout servTime
    req.Dispose()
    printfn "%s done at time = %g" name env.Now
    outClients.Add name
}   

// Expected output:
// c1 requests 1 unit at time = 0
// c2 requests 1 unit at time = 0
// c3 requests 1 unit at time = 0
// c4 requests 1 unit at time = 0
// c5 requests 1 unit at time = 0
// c6 requests 1 unit at time = 0
// c1 done at time = 100
// c2 done at time = 100
// c6 done at time = 200
// c5 done at time = 200
// c4 done at time = 300
// c3 done at time = 300
// Request order: ['c1', 'c2', 'c3', 'c4', 'c5', 'c6']
// Service order: ['c1', 'c2', 'c6', 'c5', 'c4', 'c3']
let run() =
    let env = Sim.Environment()
    let server = Sim.Resource(env, 2, WaitPolicy.Priority) 
    inClients.Clear()
    outClients.Clear()
     
    // Six client processes are created and started.
    for i = 1 to 6 do
        let id = i.ToString()
        let priority = float(6-i)
        let name = "c" + id
        env.Process((getServed env name priority server)) |> ignore

    env.Run (until = 500.0)

    printfn "Request order: %s" (listToString inClients)
    printfn "Service order: %s" (listToString outClients)