﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.SimPy2.Message

open PommaLabs.Dessert

let go (env: SimEnvironment) id len = seq<SimEvent> {
    printfn "%g %d Starting" env.Now id
    yield upcast env.Timeout 100.0
    printfn "%g %d Arrived" env.Now id
}

// Expected output:
// Starting simulation
// 0 1 Starting
// 6 2 Starting
// 100 1 Arrived
// 106 2 Arrived
// Current time is 106
let run() =
    let env = Sim.Environment()
    env.Process (go env 1 203) |> ignore
    env.DelayedProcess (go env 2 33, delay = 6.0) |> ignore
    printfn "Starting simulation"
    env.Run (until = 200.0)
    printfn "Current time is %g" env.Now