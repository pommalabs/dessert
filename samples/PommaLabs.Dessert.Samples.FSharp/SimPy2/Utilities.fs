﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.Utilities

open System.Collections.Generic
open System.Text

let listToString<'a> (list : IList<'a>) = 
    let builder = StringBuilder()
    builder.Append('[') |> ignore
    let tmp = list.Count - 1;
    if tmp >= 0 then
        builder.Append('\'') |> ignore
        for i = 0 to tmp-1 do
            builder.Append(list.[i]) |> ignore
            builder.Append("', '") |> ignore
        builder.Append(list.[tmp]) |> ignore
        builder.Append('\'') |> ignore
    builder.Append(']') |> ignore
    builder.ToString()