﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.BankExample

open PommaLabs.Dessert
open PommaLabs.Dessert.Resources
open MoreLinq // Espone MinBy, usato dentro Spawner

Sim.CurrentTimeUnit <- TimeUnit.Minute
let avgIncomingT, avgServiceT = (3).Minutes(), (10).Minutes()
let queueCount = 3 // Numero sportelli
let bankCap, bankLvl = 20000.0, 2000.0 // Euro
let waitTally, servTally = Sim.Tally(), Sim.Tally()
let mutable totClients = 0

let client(env: SimEnvironment, queue: Resource, bank: Container, amount, get) = seq<SimEvent> {
    use req = queue.Request()
    let s1 = env.Now
    yield upcast req
    waitTally.Observe(env.Now - s1)
    let s2 = env.Now
    yield upcast env.Timeout(env.Random.Exponential(1.0/avgServiceT))
    if get then yield upcast bank.Get(amount)
    else yield upcast bank.Put(amount)
    servTally.Observe(env.Now - s2)
}

let rec spawner(env: SimEnvironment, queues: Resource list, bank) = seq<SimEvent> {
    yield upcast env.Timeout(env.Random.Exponential(1.0/avgIncomingT))
    let queue = queues.MinBy(fun q -> q.Count).First()
    let amount = float(env.Random.Next(50, 500))
    let get = env.Random.NextDouble() < 0.4
    env.Process(client(env, queue, bank, amount, get)) |> ignore
    totClients <- totClients + 1
    yield! spawner(env, queues, bank)
}

let run() =
    let env = Sim.Environment(seed = 21)
    let queues = [for x in 1 .. queueCount do yield Sim.Resource(env, 1)]
    let bank = Sim.Container(env, bankCap, bankLvl)
    waitTally.Reset()
    servTally.Reset()
    totClients <- 0

    // Avvio della simulazione
    env.Process(spawner(env, queues, bank)) |> ignore
    env.Run(until = (5).Hours())

    // Raccolta dati statistici
    printfn "Finanze totali al tempo %.2f: %g" env.Now bank.Level
    printfn "Clienti entrati: %d" totClients
    printfn "Clienti serviti: %d" servTally.Count
    printfn "Tempo medio di attesa: %.2f" (waitTally.Mean())
    printfn "Tempo medio di servizio: %.2f" (servTally.Mean())
