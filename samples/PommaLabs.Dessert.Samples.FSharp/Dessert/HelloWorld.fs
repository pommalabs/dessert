﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.HelloWorld

open PommaLabs.Dessert

let rec sayHello (env : SimEnvironment) = seq<SimEvent> {
    yield upcast env.Timeout 2.1
    printfn "Hello World at %g!" env.Now
    yield! sayHello env
}

let run() = 
    printfn "Hello World simulation :)"
    let env = Sim.Environment();
    env.Process (sayHello env) |> ignore
    env.Run 10.0