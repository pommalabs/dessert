﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.WaterDrinkers

open PommaLabs.Dessert
open PommaLabs.Dessert.Events
open PommaLabs.Dessert.Resources

let boxCapacity = 1.0 // Liters
let glassCapacity = 0.25 // Liters
let mutable fillBox: SimEvent<int> = null

let rec filler(env: SimEnvironment, box: Container) = seq<SimEvent> {
    yield upcast box.Put(boxCapacity - box.Level)
    fillBox <- env.Event<int>()
    yield upcast fillBox
    let id = fillBox.Value
    printfn "%f: %d chiama tecnico" env.Now id
    yield! filler(env, box)
}

let drinker(env: SimEnvironment, id, box: Container) = seq<SimEvent> {
    // Occorre controllare che l'evento fillBox non sia gia'
    // stato attivato, perche' attivarlo nuovamente
    // risulterebbe in una eccezione da parte di SimPy.
    if box.Level < glassCapacity && not fillBox.Succeeded then
        fillBox.Succeed(id)    
    yield upcast box.Get(glassCapacity)
    printfn "%f: %d ha bevuto!" env.Now id
}

let rec spawner(env: SimEnvironment, box, nextId) = seq<SimEvent> {
    yield upcast env.Timeout(5.0)
    env.Process(drinker(env, nextId, box)) |> ignore
    yield! spawner(env, box, nextId+1)
}

let run() =
    let env = Sim.Environment()
    let box = Sim.Container(env, capacity=boxCapacity)
    env.Process(filler(env, box)) |> ignore
    env.Process(spawner(env, box, 0)) |> ignore
    env.Run(until=31)