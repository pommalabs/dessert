﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.SimPy3.Car

open PommaLabs.Dessert

let rec car(env: SimEnvironment) = seq<SimEvent> {
    printfn "Start parking at %g" env.Now
    let parkingDuration = 5.0
    yield upcast env.Timeout(parkingDuration)

    printfn "Start driving at %g" env.Now
    let tripDuration = 2.0
    yield upcast env.Timeout(tripDuration)

    // Runs the same process again. Tail recursion optimization
    // makes following instruction safe and efficient.
    yield! car(env)
}

// Expected output:
// Start parking at 0
// Start driving at 5
// Start parking at 7
// Start driving at 12
// Start parking at 14
let run() =
    let env = Sim.Environment()
    env.Process(car(env)) |> ignore
    env.Run(until = 15.0)         