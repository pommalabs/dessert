﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.SimPy3.Clock

open PommaLabs.Dessert

let rec clock(env: SimEnvironment, name, tick) = seq<SimEvent> { 
    printfn "%s %.1f" name env.Now 
    yield upcast env.Timeout(tick)
    yield! clock(env, name, tick)
}

let run() =    
    let env = Sim.Environment() 
    env.Process(clock(env, "fast", 0.5)) |> ignore
    env.Process(clock(env, "slow", 1.0)) |> ignore
    env.Run(until = 2)