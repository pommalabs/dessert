﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

module PommaLabs.Dessert.Samples.FSharp.SimPy3.CarCharge

open PommaLabs.Dessert

type Car (env: SimEnvironment) as x =
    // Starts the "run" process every time an instance is created.
    do env.Process (x.run) |> ignore

    member x.run = seq<SimEvent> {
        printfn "Start parking and charging at %g" env.Now
        let chargeDuration = 5.0
        // We yield the process that start() returns to wait for it to finish.
        yield upcast env.Process (x.charge chargeDuration)

        // The charge process has finished and we can start driving again.
        printfn "Start driving at %g" env.Now
        let tripDuration = 2.0
        yield upcast env.Timeout tripDuration
            
        // Runs the same process again. Tail recursion optimization
        // makes following instruction safe and efficient.
        yield! x.run
    }

    member x.charge duration = seq<SimEvent> {
        yield upcast env.Timeout(duration)
    }

// Expected output:
// Start parking and charging at 0
// Start driving at 5
// Start parking and charging at 7
// Start driving at 12
// Start parking and charging at 14
let run() =
    let env = Sim.Environment()
    let car = Car env
    env.Run (until = 15.0)