# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from threading import RLock
from Tally import Tally

class Stats:
    clientRequestsTally = Tally()
    clientTimeWaitedTally = Tally()
    usedMemoryTally = Tally()
    _reconstructedFilesTally = Tally()
    lostSwitchMessagesTally = Tally()

    def __init__(self):
        self._clientRequestsTmpTally = Tally()
        self._clientTimeWaitedTmpTally = Tally()
        self._usedMemoryTmpTally = Tally()
        self._reconstructedFiles = 0
        self._lostSwitchMessages = 0

    def clientRequestsAvg(self): 
        return self._clientRequestsTmpTally.mean()

    def clientRequestsTotalAvg(self):
        return Stats.clientRequestsTally.mean()

    def addClientRequests(self, n):
        Stats.clientRequestsTally.observe(n)
        self._clientRequestsTmpTally.observe(n)

    def clientTimeWaitedAvg(self):
        return self._clientTimeWaitedTmpTally.mean()

    def clientTimeWaitedTotalAvg(self):
        return Stats.clientTimeWaitedTally.mean()

    def addClientTimeWaited(self, t):
        Stats.clientTimeWaitedTally.observe(t)
        self._clientTimeWaitedTmpTally.observe(t)

    def usedMemoryAvg(self):
        return int(self._usedMemoryTmpTally.mean())

    def usedMemoryTotalAvg(self):
        mem = int(Stats.usedMemoryTally.mean())
        Stats.usedMemoryTally.reset()
        return mem

    def addUsedMemory(self, m):
        Stats.usedMemoryTally.observe(m)
        self._usedMemoryTmpTally.observe(m)
        
    def reconstructedFiles(self):
    	return self._reconstructedFiles

    def lostSwitchMessages(self):
        return self._lostSwitchMessages
       
    def reconstructedFilesAvg(self):
    	return Stats._reconstructedFilesTally.mean()

    def lostSwitchMessagesAvg(self):
        return Stats.lostSwitchMessagesTally.mean()

    def fileReconstructed(self):
        self._reconstructedFiles += 1

    def messageLost(self):
        self._lostSwitchMessages += 1
    
    # Called when each simulation has ended.
    def tmpReset(self):
        Stats._reconstructedFilesTally.observe(self._reconstructedFiles)
        Stats.lostSwitchMessagesTally.observe(self._lostSwitchMessages)

    # Called when all simulations have ended.
    def reset(self):
        Stats.clientRequestsTally.reset()
        Stats.clientTimeWaitedTally.reset()
        Stats._reconstructedFilesTally.reset()
        Stats.lostSwitchMessagesTally.reset()