# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from simpy import *
from simpy.resources.store import Store
from Common import *
from Packets import *

class Switch(Entity):
    
    def __init__(self, env, g):
        Entity.__init__(self, env, g)
        self._frames = Store(env, G.frameCount)
    
    def run(self):
        while True:
            p = yield self._frames.get()
            # Switch is awakened again by the Receive method.
            # In Python 3.3, following instruction can be replaced with "yield from".
            for ev in self.waitForSend(p): yield ev
            self._send(p);

    def receive(self, udpPacket):
        if len(self._frames.items) == G.frameCount:
            self.logger.info("Lost a packet at %g!", self.env.now)
            self.g.stats.messageLost()
        else:
            self.logger.info("Packet %s incoming", udpPacket)
            self._frames.put(udpPacket)

    def _send(self, udpPacket):
        if udpPacket.type == REQUEST_TYPE:
            self.g.serverOSes[udpPacket.dst].receive(udpPacket)
        else:
            assert udpPacket.type == ANSWER_TYPE
            self.g.clientOSes[udpPacket.dst].receive(udpPacket)

    def __str__(self):
        return "Switch[usedFrames: {0}]".format(self._frames.count)