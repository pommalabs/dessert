# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os
import platform
import psutil

currProc = psutil.Process(os.getpid())

def memory_usage():
    return currProc.get_memory_info()[0] / (1024*1024) # RSS