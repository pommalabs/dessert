﻿# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import gc
import simpy
import sys
import time
from Common import *
from Tally import Tally

def timeoutBenchmark_PEM(env, counter):
    while True:
        yield env.timeout(counter.randomDelay())
        counter.increment()

def timeoutBenchmark(processCount):
    counterTally = Tally()
    memoryTally = Tally()
    for i in range(repetitionCount):
        gc.collect() 
        start = time.time()
        env = simpy.Environment()
        tally = Tally()
        env.process(memoryRecorder(env, tally))
        counter = Counter()
        for i in range(processCount):
            env.process(timeoutBenchmark_PEM(env, counter))
        env.run(until=simTime)
        end = time.time()
        counterTally.observe(counter.total() / (end-start))
        memoryTally.observe(tally.mean())
    return Result(counterTally.mean(), memoryTally.mean())

def run():
    outputName = "timeout-benchmark-{0}.csv".format(tag)
    output = open(outputName, "w")
    print("TIMEOUT BENCHMARK - SIMPY")
    print("* Warming up...")
    for i in range(repetitionCount):  
        timeoutBenchmark(processCounts[0])
    for processCount in processCounts:  
        result = timeoutBenchmark(processCount)
        evPerSec = result.eventCount()
        avgMemUsage = result.averageMemUsage()
        print("* %d processes: %d timeouts/sec, %d MB" % (processCount, evPerSec, avgMemUsage))
        sys.stdout.flush()
        output.write("{0};{1};{2}\n".format(processCount, int(evPerSec), int(avgMemUsage)))
        output.flush()
    output.close()