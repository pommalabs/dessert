﻿# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import gc
from simpy import *
import sys
import time
from Common import *
from Tally import Tally

def prodConsBenchmark_Consumer(env, store, counter):
    while True:
        yield env.timeout(counter.randomDelay())
        yield store.get()
        counter.increment()

def prodConsBenchmark_Producer(env, store, counter):
    while True:
        yield env.timeout(counter.randomDelay())
        yield store.put(counter.randomDelay())
        counter.increment()

def prodConsBenchmark(processCount):
    counterTally = Tally()
    memoryTally = Tally()
    for i in range(repetitionCount): 
        gc.collect()
        start = time.time()
        env = Environment()
        store = Store(env)
        tally = Tally()
        env.process(memoryRecorder(env, tally))
        counter = Counter()
        for i in range(processCount/2):
            env.process(prodConsBenchmark_Consumer(env, store, counter))
        for i in range(processCount/2, processCount):
            env.process(prodConsBenchmark_Producer(env, store, counter))
        env.run(until=simTime)
        end = time.time()
        counterTally.observe(counter.total() / (end-start))
        memoryTally.observe(tally.mean())
    return Result(counterTally.mean(), memoryTally.mean())

def run():
    outputName = "prodcons-benchmark-{0}.csv".format(tag)
    output = open(outputName, "w")
    print("PRODUCER CONSUMER BENCHMARK - SIMPY")
    print("* Warming up...")
    for i in range(repetitionCount):  
		prodConsBenchmark(processCounts[0])
    for processCount in processCounts:  
        result = prodConsBenchmark(processCount)
        evPerSec = result.eventCount()
        avgMemUsage = result.averageMemUsage()
        print("* %d processes: %d storeEvents/sec, %d MB" % (processCount, evPerSec, avgMemUsage))
        sys.stdout.flush()
        output.write("{0};{1};{2}\n".format(processCount, int(evPerSec), int(avgMemUsage)))
        output.flush()
    output.close()