﻿# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import random
from Galois.MemoryRecorder import memory_usage

simTime = 1000
memRecFreq = simTime/5.0
minTimeout = simTime/100.0
maxTimeout = simTime/20.0
repetitionCount = 21
processCounts = range(500, 20500, 500)

import platform
if platform.system().lower().startswith("linux"):
    tag = "simpy-linux"
else:
    tag = "simpy-windows"

class Counter:
    def __init__(self):
        self._random = random.Random()
        self._total = 0

    def total(self):
        return self._total
    
    def increment(self):
        self._total += 1

    def randomDelay(self):
        return self._random.uniform(minTimeout, maxTimeout)

class Result:
    def __init__(self, eventCount, avgMemUsage):
        self._eventCount = eventCount
        self._avgMemUsage = avgMemUsage
    
    def eventCount(self):
        return self._eventCount
    
    def averageMemUsage(self):
        return self._avgMemUsage

def memoryRecorder(env, tally):
    while True:
        yield env.timeout(memRecFreq)
        tally.observe(memory_usage())
