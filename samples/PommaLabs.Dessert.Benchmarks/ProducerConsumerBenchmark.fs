﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Benchmarks

open System
open System.Diagnostics
open System.IO
open PommaLabs.Dessert
open PommaLabs.Dessert.Benchmarks.Common
open PommaLabs.Dessert.Resources

module ProducerConsumerBenchmark =
    
    let prodConsBenchmark_Consumer(env: SimEnvironment, store: Store<int>, counter: Counter) = seq<SimEvent> {
        while true do
            yield upcast env.Timeout(counter.RandomDelay)
            yield upcast store.Get()
            counter.Increment()
    }  
      
    let prodConsBenchmark_Producer(env: SimEnvironment, store: Store<int>, counter: Counter) = seq<SimEvent> {
        while true do
            yield upcast env.Timeout(counter.RandomDelay)
            yield upcast store.Put(int counter.RandomDelay)
            counter.Increment()
    }
    
    let prodConsBenchmark(processCount) =
        let counterTally = Sim.Tally()
        let memoryTally = Sim.Tally()
        for i = 1 to repetitionCount do
            cleanUp()
            let stopwatch = Stopwatch()
            stopwatch.Start()
            let env = Sim.Environment()
            let store = Sim.Store<int>(env)
            let tally = Sim.Tally()
            env.Process(memoryRecorder(env, tally)) |> ignore
            let counter = Counter()
            for i = 1 to processCount/2 do
                env.Process(prodConsBenchmark_Consumer(env, store, counter)) |> ignore
            for i = processCount/2 to processCount do
                env.Process(prodConsBenchmark_Producer(env, store, counter)) |> ignore      
            env.Run(simTime)
            stopwatch.Stop()
            counterTally.Observe(toEventsPerSec(counter.Total, stopwatch.ElapsedMilliseconds))
            memoryTally.Observe(tally.Mean())
        Result(counterTally.Mean(), int(memoryTally.Mean()))

    let run() =
        let outputName = String.Format("prodcons-benchmark-{0}.csv", tag)
        let output = new StreamWriter(outputName)
        printfn "PRODUCER CONSUMER BENCHMARK - DESSERT"
        printfn "* Warming up..."
        for i = 1 to repetitionCount do
            prodConsBenchmark(processCounts.[0]) |> ignore
        for processCount in processCounts do
            printf "* %d processes: " processCount
            let result = prodConsBenchmark(processCount)
            let evPerSec = int(result.EventCount)
            let avgMem = int(result.AverageMemUsage)
            printfn "%d storeEvents/sec, %d MB" evPerSec avgMem
            output.WriteLine("{0};{1};{2}", processCount, evPerSec, avgMem)
            output.Flush()
        printfn ""          
        output.Close()

