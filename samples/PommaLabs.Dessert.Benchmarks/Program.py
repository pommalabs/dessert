# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import sys
import GaloisBenchmark
import ProducerConsumerBenchmark
import TimeoutBenchmark

for arg in sys.argv[1:]:
	if arg == "-t":
		TimeoutBenchmark.run()
	elif arg == "-pc":
		ProducerConsumerBenchmark.run()
	elif arg == "-g":
		GaloisBenchmark.run()
	else:
		raise ValueError("Invalid benchmark flag")