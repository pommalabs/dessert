﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Benchmarks

open System
open System.Diagnostics
open System.IO
open PommaLabs.Dessert.Benchmarks.Common
open PommaLabs.Dessert.Samples.CSharp

module GaloisBenchmark =
    
    let parameters = [(4, 64); (8, 96); (16, 128); (32, 256)]

    let run() =
        let outputName = String.Format("galois-benchmark-{0}.csv", tag)
        let output = new StreamWriter(outputName)
        for (machineCount, frameCount) in parameters do
            printfn "### Running Galois with (mc = %d, fc = %d) ###" machineCount frameCount
            let stopwatch = Stopwatch()
            stopwatch.Start()
            let usedMem = Galois.Starter.Run(int16 machineCount, int16 frameCount)
            stopwatch.Stop()
            let execTime = stopwatch.Elapsed.TotalMinutes
            output.WriteLine("{0};{1};{2}", machineCount, execTime, usedMem)
            output.Flush()
            printfn "### Execution time: %A minutes ###" execTime
            printfn "### Used memory: %A MB ###" usedMem
            printfn ""
        output.Close()