﻿# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import time
from Common import tag
from Galois.Starter import runSimulations

parameters = [(4, 64), (8, 96), (16, 128), (32, 256)]

def run():
    outputName = "galois-benchmark-{0}.csv".format(tag)
    output = open(outputName, "w")
    for (machineCount, frameCount) in parameters:
        print("### Running Galois with (mc = {0}, fc = {1}) ###".format(machineCount, frameCount))
        start = time.time()
        usedMemory = runSimulations(machineCount, frameCount)
        end = time.time()
        execTime = (end-start)/60.0
        output.write("{0};{1};{2}\n".format(machineCount, execTime, int(usedMemory)))
        output.flush()
        print("### Execution time: {0} minutes ###".format(execTime))
        print("### Used memory: {0} MB ###".format(usedMemory))
        print("")
    output.close()