﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Benchmarks

open System
open System.Diagnostics
open PommaLabs.Dessert
open PommaLabs.Dessert.Benchmarks.Common

module Program =
    
    [<EntryPoint>]
    let main argv =
        for arg in argv do
            match argv.[0] with
            | "-t" -> TimeoutBenchmark.run()
            | "-pc" -> ProducerConsumerBenchmark.run()
            | "-g" -> GaloisBenchmark.run()
            | _ -> raise(new Exception("Invalid benchmark flag"))
        0
