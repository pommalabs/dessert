﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Benchmarks

open System
open System.Diagnostics
open System.Globalization
open System.Threading
open PommaLabs.Dessert
open PommaLabs.Dessert.Recording
open Troschuetz.Random
open Troschuetz.Random.Generators

module Common =
    let simTime = 1000.0
    let memRecFreq = simTime/5.0
    let minTimeout = double simTime/100.0
    let maxTimeout = double simTime/20.0
    let repetitionCount = 21
    let currProc = Process.GetCurrentProcess()

    let tag = match Environment.OSVersion.Platform with
              | PlatformID.Unix -> "dessert-linux"
              | _ -> "dessert-windows"

    let processCounts = [for i in 1..40 do yield 500*i]

    Thread.CurrentThread.CurrentCulture <- CultureInfo("en-GB") // To avoid commas in decimal values.

    let memoryRecorder(env: SimEnvironment, tally: Tally) = seq<SimEvent> {
        while true do
            yield upcast env.Timeout(memRecFreq)
            currProc.Refresh()
            let procMemInMb = currProc.WorkingSet64/(1024L*1024L)
            tally.Observe(float procMemInMb);
    }

    type Counter() =
        let random = TRandom(MT19937Generator())
        let mutable total = 0UL
        member c.Total = total
        member c.Increment() = total <- total + 1UL
        member c.RandomDelay = random.NextDouble(minTimeout, maxTimeout)

    type Result(eventCount: double, avgMem: int) =
        let eventCount = eventCount
        let avgMem = avgMem
        member r.EventCount = eventCount
        member r.AverageMemUsage = avgMem
    
    let toEventsPerSec(eventCount: uint64, timeInMillisec: int64) =
        let seconds = (float timeInMillisec/1000.0)
        (float eventCount/seconds)
    
    let cleanUp() =
        GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, blocking = true)
        GC.WaitForPendingFinalizers()