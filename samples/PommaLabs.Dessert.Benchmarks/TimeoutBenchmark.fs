﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Benchmarks

open System
open System.Diagnostics
open System.IO
open PommaLabs.Dessert
open PommaLabs.Dessert.Benchmarks.Common

module TimeoutBenchmark =

    let timeoutBenchmark_Dessert_PEM(env: SimEnvironment, counter: Counter) = seq<SimEvent> {
        while true do
            yield upcast env.Timeout(counter.RandomDelay)
            counter.Increment()
    }

    let timeoutBenchmark_Dessert(processCount) =
        let counterTally = Sim.Tally()
        let memoryTally = Sim.Tally()
        for i = 1 to repetitionCount do
            cleanUp()
            let stopwatch = Stopwatch()
            stopwatch.Start()
            let env = Sim.Environment()
            let tally = Sim.Tally()
            env.Process(memoryRecorder(env, tally)) |> ignore
            let counter = Counter()
            for i = 1 to processCount do
                env.Process(timeoutBenchmark_Dessert_PEM(env, counter)) |> ignore
            env.Run(simTime)
            stopwatch.Stop()
            counterTally.Observe(toEventsPerSec(counter.Total, stopwatch.ElapsedMilliseconds))
            memoryTally.Observe(tally.Mean())
        Result(counterTally.Mean(), int(memoryTally.Mean()))

    let run() =
        let outputName = String.Format("timeout-benchmark-{0}.csv", tag)
        let output = new StreamWriter(outputName)
        printfn "TIMEOUT BENCHMARK - DESSERT"
        printfn "* Warming up..."
        for i = 1 to repetitionCount do
            timeoutBenchmark_Dessert(processCounts.[0]) |> ignore
        for processCount in processCounts do
            printf "* %d processes: " processCount
            let result = timeoutBenchmark_Dessert(processCount)
            let evPerSec = int(result.EventCount)
            let avgMem = int(result.AverageMemUsage)
            printfn "%d timeouts/sec, %d MB" evPerSec avgMem
            output.WriteLine("{0};{1};{2}", processCount, evPerSec, avgMem)
            output.Flush()
        printfn ""
        output.Close()