﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Public Module HelloWorld

    Private Iterator Function SayHello(sim As SimEnvironment) As IEnumerable(Of SimEvent)
        While True
            Yield sim.Timeout(2.1)
            Console.WriteLine("Hello World at {0:0.0}!", sim.Now)
        End While
    End Function

    ' Expected output:
    ' Hello World simulation :)
    ' Hello World at 2.1!
    ' Hello World at 4.2!
    ' Hello World at 6.3!
    ' Hello World at 8.4!
    Sub Run()
        Console.WriteLine("Hello World simulation :)")
        Dim env = Sim.Environment()
        env.Process(SayHello(env))
        env.Run(10)
    End Sub

End Module
