﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Public Module ValueUsage

    Private Iterator Function Process(env As SimEnvironment) As IEnumerable(Of SimEvent)
        Dim t = env.Timeout(5, value:="A BORING EXAMPLE")
        Yield t
        ' Since t.Value is a string, we can call
        ' string methods on it.
        Console.WriteLine(t.Value.Substring(2, 6))

        Dim intStore = Sim.Store(Of Double)(env)
        intStore.Put(t.Delay)
        Dim getEv = intStore.Get()
        Yield getEv
        ' Since getEv.Value is a double, we can
        ' multiply it by 2.5, as expected.
        Console.WriteLine(getEv.Value * 2.5)
    End Function

    Sub Run()
        Dim env = Sim.Environment()
        env.Process(Process(env))
        env.Run()
    End Sub

End Module
