﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Imports PommaLabs.Dessert.Events

Public Module EventCallbacks

    Private Iterator Function DoFail(env As SimEnvironment, ev As SimEvent(Of String)) As IEnumerable(Of SimEvent)
        Yield env.Timeout(5)
        ev.Fail("NO")
    End Function

    Private Sub MyCallback(ev As SimEvent)
        Console.WriteLine("Successo: '{0}'; Valore: '{1}'", ev.Succeeded, ev.Value)
    End Sub

    Private Iterator Function Proc(env As SimEnvironment) As IEnumerable(Of SimEvent)
        Dim ev1 = env.Timeout(7, "SI")
        ev1.Callbacks.Add(AddressOf MyCallback)
        Yield ev1
        Dim ev2 = env.Event(Of String)()
        ev2.Callbacks.Add(AddressOf MyCallback)
        env.Process(DoFail(env, ev2))
        Yield ev2
    End Function

    Sub Run()
        Dim env = Sim.Environment()
        env.Process(Proc(env))
        env.Run()
    End Sub

End Module
