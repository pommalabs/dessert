﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Imports PommaLabs.Dessert.Resources

Namespace SimPy3
    Public Module EventLatency
        Const SimDuration As Integer = 100

        ''' <summary>
        '''   This class represents the propagation through a cable.
        ''' </summary>
        Private Class Cable
            ReadOnly _env As SimEnvironment
            ReadOnly _delay As Integer
            ReadOnly _store As Store(Of String)

            Public Sub New(env As SimEnvironment, delay As Integer)
                _env = env
                _delay = delay
                _store = Sim.Store(Of String)(env)
            End Sub

            Private Iterator Function Latency(value As String) As IEnumerable(Of SimEvent)
                Yield _env.Timeout(_delay)
                Yield _store.Put(value)
            End Function

            Public Sub Put(value As String)
                _env.Process(Latency(value))
            End Sub

            Public Function Take() As Store(Of String).GetEvent
                Return _store.Get()
            End Function

        End Class

        ''' <summary>
        '''   A process which randomly generates messages.
        ''' </summary>
        Private Iterator Function Sender(env As SimEnvironment, cable As Cable) As IEnumerable(Of SimEvent)
            While True
                ' Waits for next transmission.
                Yield env.Timeout(5)
                cable.Put(String.Format("Sender sent this at {0}", env.Now))
            End While
        End Function

        ''' <summary>
        '''   A process which consumes messages.
        ''' </summary>
        ''' <param name="env"></param>
        ''' <param name="cable"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Iterator Function Receiver(env As SimEnvironment, cable As Cable) As IEnumerable(Of SimEvent)
            While True
                ' Gets the event for the message pipe.
                Dim getEv = cable.Take()
                Yield getEv
                Console.WriteLine("Received this at {0} while {1}", env.Now, getEv.Value)
            End While
        End Function

        Public Sub Run()
            ' Sets up and starts the simulation.
            Console.WriteLine("Event Latency")
            Dim env = Sim.Environment()

            Dim cable = New Cable(env, 10)
            env.Process(Sender(env, cable))
            env.Process(Receiver(env, cable))

            env.Run(until:=SimDuration)
        End Sub

    End Module
End Namespace
