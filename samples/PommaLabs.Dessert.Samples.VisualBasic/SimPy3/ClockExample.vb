﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Namespace SimPy3
    Public Module ClockExample

        Private Iterator Function Clock(env As SimEnvironment, name As String, tick As Double) _
            As IEnumerable(Of SimEvent)
            While True
                Console.WriteLine("{0} {1:0.0}", name, env.Now)
                Yield env.Timeout(tick)
            End While
        End Function

        Sub Run()
            Dim env = Sim.Environment()
            env.Process(Clock(env, "fast", 0.5))
            env.Process(Clock(env, "slow", 1.0))
            env.Run(until:=2)
        End Sub

    End Module
End Namespace
