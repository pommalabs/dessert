﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Imports PommaLabs.Dessert.Resources

Public Module ProducerFilteredConsumer

    Private Iterator Function Producer(env As SimEnvironment, store As FilterStore(Of Integer)) _
        As IEnumerable(Of SimEvent)
        While True
            Yield env.Timeout(env.Random.Next(1, 20))
            Dim item = env.Random.Next(1, 20)
            Yield store.Put(item)
            Console.WriteLine("{0}: Prodotto un {1}", env.Now, item)
        End While
    End Function

    Private Iterator Function Consumer(env As SimEnvironment, store As FilterStore(Of Integer), name As String,
                                       filter As Predicate(Of Integer)) As IEnumerable(Of SimEvent)
        While True
            Yield env.Timeout(env.Random.Next(1, 20))
            Dim getEv = store.Get(filter)
            Yield getEv
            Console.WriteLine("{0}: {1}, consumato un {2}", env.Now, name, getEv.Value)
        End While
    End Function

    Sub Run()
        Dim env = Sim.Environment(21)
        Dim store = Sim.FilterStore(Of Integer)(env, capacity:=2)
        env.Process(Producer(env, store))
        env.Process(Producer(env, store))
        env.Process(Consumer(env, store, "PARI", Function(i) i Mod 2 = 0))
        env.Process(Consumer(env, store, "DISPARI", Function(i) i Mod 2 = 1))
        env.Run(until:=60)
    End Sub

End Module
