﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Imports PommaLabs.Dessert.Resources

Public Module Hospital
    Const Red = 0
    Const Yellow = 1
    Const Green = 2

    Private Iterator Function Person(env As SimEnvironment, name As String, code As Integer, hospital As Resource) _
        As IEnumerable(Of SimEvent)
        Using req = hospital.Request(code)
            Yield req
            Console.WriteLine("{0} viene curato...", name)
            Yield env.Timeout(5)
        End Using
    End Function

    Sub Run()
        Dim env = Sim.Environment()
        Dim hospital = Sim.Resource(env, capacity:=2, requestPolicy:=WaitPolicy.Priority)
        env.Process(Person(env, "Pino", Yellow, hospital))
        env.Process(Person(env, "Gino", Green, hospital))
        env.Process(Person(env, "Nino", Green, hospital))
        env.Process(Person(env, "Dino", Yellow, hospital))
        env.Process(Person(env, "Tino", Red, hospital))
        env.Run()
    End Sub

End Module
