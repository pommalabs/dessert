﻿' Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
'
' Licensed under the MIT License. See LICENSE file in the project root for full license information.

Imports PommaLabs.Dessert.Resources

Public Module HospitalPreemption
    Const Red = 0
    Const Yellow = 1
    Const Green = 2

    Private ReadOnly s_names As Dictionary(Of SimProcess, String) = New Dictionary(Of SimProcess, String)()

    Private Iterator Function Person(env As SimEnvironment, code As Integer, hospital As PreemptiveResource,
                                     delay As Double, name As String) As IEnumerable(Of SimEvent)
        s_names.Add(env.ActiveProcess, name)
        Yield env.Timeout(delay)
        Using req = hospital.Request(code, preempt:=(code = Red))
            Yield req
            Console.WriteLine("{0} viene curato...", name)
            Yield env.Timeout(7)
            Dim info As PreemptionInfo = Nothing
            If env.ActiveProcess.Preempted(info) Then
                Console.WriteLine("{0} scavalcato da {1}", name, s_names(info.By))
            Else
                Console.WriteLine("Cure finite per {0}", name)
            End If
        End Using
    End Function

    Sub Run()
        Dim env = Sim.Environment()
        Dim hospital = Sim.PreemptiveResource(env, capacity:=2)
        env.Process(Person(env, Yellow, hospital, 0, "Pino"))
        env.Process(Person(env, Green, hospital, 0, "Gino"))
        env.Process(Person(env, Green, hospital, 1, "Nino"))
        env.Process(Person(env, Yellow, hospital, 1, "Dino"))
        env.Process(Person(env, Red, hospital, 2, "Tino"))
        env.Run()
    End Sub

End Module
