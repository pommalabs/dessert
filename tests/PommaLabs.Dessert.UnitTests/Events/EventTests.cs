﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Events
{
    using System;
    using Dessert.Events;
    using NUnit.Framework;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class EventTests : TestBase
    {
        private SimEvents Triggerer_UnusedInCondition(SimEvent<object> token, Action<SimEvent<object>> triggerer)
        {
            var ev = _env.Event();
            _env.Process(Triggerer_UnusedInCondition_Aux(ev));
            yield return _env.AnyOf(token, ev);
            triggerer(token);
        }

        private SimEvents Triggerer_UnusedInCondition_Aux(SimEvent<object> token)
        {
            yield return _env.Timeout(5);
            token.Succeed();
        }

        [Test]
        public void Triggerer_UnusedInCondition_Fail()
        {
            var ev = _env.Event();
            _env.Process(Triggerer_UnusedInCondition(ev, e => e.Fail()));
            _env.Run();
        }

        [Test]
        public void Triggerer_UnusedInCondition_FailWithValue()
        {
            var ev = _env.Event();
            _env.Process(Triggerer_UnusedInCondition(ev, e => e.Fail(new Exception())));
            _env.Run();
        }

        [Test]
        public void Triggerer_UnusedInCondition_Succeed()
        {
            var ev = _env.Event();
            _env.Process(Triggerer_UnusedInCondition(ev, e => e.Succeed()));
            _env.Run();
        }

        [Test]
        public void Triggerer_UnusedInCondition_SucceedWithValue()
        {
            var ev = _env.Event();
            _env.Process(Triggerer_UnusedInCondition(ev, e => e.Succeed(5)));
            _env.Run();
        }
    }
}
