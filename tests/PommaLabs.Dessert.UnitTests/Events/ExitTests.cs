﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Events
{
    using NUnit.Framework;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class ExitTests : TestBase
    {
        private SimEvents ExitYielder(object value)
        {
            yield return _env.Exit(value);
            Assert.Fail();
        }

        private SimEvents ExitYielder_InnerProcess(object value)
        {
            var process = _env.Process(ExitYielder(value));
            yield return process;
            Assert.AreEqual(value, process.Value);
            yield return _env.Exit(process.Value);
            Assert.Fail();
        }

        [Test]
        public void Exit_CommonYielder()
        {
            const string exitValue = "PINO";
            var process = _env.Process(ExitYielder(exitValue));
            _env.Run();
            Assert.AreEqual(exitValue, process.Value);
        }

        [Test]
        public void Exit_InnerYielder()
        {
            const string value = "PINO";
            var process = _env.Process(ExitYielder_InnerProcess(value));
            _env.Run();
            Assert.AreEqual(value, process.Value);
        }
    }
}
