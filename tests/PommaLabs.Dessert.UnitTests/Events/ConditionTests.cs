﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Events
{
    using System.Collections.Generic;
    using System.Linq;
    using Dessert.Events;
    using Dessert.Resources;
    using NUnit.Framework;
    using IEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed partial class ConditionTests : TestBase
    {
        private IEvents And_Simple()
        {
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            var cond = timeouts[0].And(timeouts[1]).And(timeouts[2]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.True(cond.Ev2.Succeeded);
            Assert.True(cond.Ev3.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(3, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
            Assert.True(cond.Value.Contains(timeouts[1]));
            Assert.True(cond.Value.Contains(timeouts[2]));
        }

        private IEvents And_Simple4()
        {
            var timeouts = Enumerable.Range(0, 4).Select(i => _env.Timeout(i)).ToList();
            var cond = timeouts[0].And(timeouts[1]).And(timeouts[2]).And(timeouts[3]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            Assert.AreSame(timeouts[3], cond.Ev4);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.True(cond.Ev2.Succeeded);
            Assert.True(cond.Ev3.Succeeded);
            Assert.True(cond.Ev4.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(3, cond.Ev4.Value);
            Assert.AreEqual(4, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
            Assert.True(cond.Value.Contains(timeouts[1]));
            Assert.True(cond.Value.Contains(timeouts[2]));
            Assert.True(cond.Value.Contains(timeouts[3]));
        }

        private IEvents And_Simple5()
        {
            var timeouts = Enumerable.Range(0, 5).Select(i => _env.Timeout(i)).ToList();
            var cond = timeouts[0].And(timeouts[1]).And(timeouts[2]).And(timeouts[3]).And(timeouts[4]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            Assert.AreSame(timeouts[3], cond.Ev4);
            Assert.AreSame(timeouts[4], cond.Ev5);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.True(cond.Ev2.Succeeded);
            Assert.True(cond.Ev3.Succeeded);
            Assert.True(cond.Ev4.Succeeded);
            Assert.True(cond.Ev5.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(3, cond.Ev4.Value);
            Assert.AreEqual(4, cond.Ev5.Value);
            Assert.AreEqual(5, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
            Assert.True(cond.Value.Contains(timeouts[1]));
            Assert.True(cond.Value.Contains(timeouts[2]));
            Assert.True(cond.Value.Contains(timeouts[3]));
            Assert.True(cond.Value.Contains(timeouts[4]));
        }

        private IEvents And_Nested()
        {
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            var cond = timeouts[0].And(timeouts[2]).Or(timeouts[1]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[2], cond.Ev2);
            Assert.AreSame(timeouts[1], cond.Ev3);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.False(cond.Ev2.Succeeded);
            Assert.True(cond.Ev3.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(2, cond.Ev2.Value);
            Assert.AreEqual(1, cond.Ev3.Value);
            Assert.AreEqual(2, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
            Assert.True(cond.Value.Contains(timeouts[1]));
            Assert.AreEqual(1, _env.Now);
        }

        private IEvents IAnd_WithAndCond()
        {
            var cond = _env.Timeout(1, value: 1).And(_env.Timeout(2, value: 2));
            var cond2 = cond.And(_env.Timeout(0, value: 0));
            yield return cond2;
            var values = cond2.Value.Select(v => v.Value).ToList();
            values.Sort();
            Assert.True(new List<object> { 0, 1, 2 }.SequenceEqual(values));
        }

        private IEvents IAnd_WithOrCond()
        {
            var cond = _env.Timeout(1, value: 1).Or(_env.Timeout(2, value: 2));
            var cond2 = cond.And(_env.Timeout(0, value: 0));
            yield return cond2;
            var values = cond2.Value.Select(v => v.Value).ToList();
            values.Sort();
            Assert.True(new List<object> { 0, 1 }.SequenceEqual(values));
        }

        private IEvents Or_Simple()
        {
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            var cond = _env.AnyOf(timeouts[0], timeouts[1], timeouts[2]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.False(cond.Ev2.Succeeded);
            Assert.False(cond.Ev3.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(1, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
        }

        private IEvents Or_Simple4()
        {
            var timeouts = Enumerable.Range(0, 4).Select(i => _env.Timeout(i)).ToList();
            var cond = _env.AnyOf(timeouts[0], timeouts[1], timeouts[2], timeouts[3]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            Assert.AreSame(timeouts[3], cond.Ev4);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.False(cond.Ev2.Succeeded);
            Assert.False(cond.Ev3.Succeeded);
            Assert.False(cond.Ev4.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(3, cond.Ev4.Value);
            Assert.AreEqual(1, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
        }

        private IEvents Or_Simple5()
        {
            var timeouts = Enumerable.Range(0, 5).Select(i => _env.Timeout(i)).ToList();
            var cond = _env.AnyOf(timeouts[0], timeouts[1], timeouts[2], timeouts[3], timeouts[4]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            Assert.AreSame(timeouts[3], cond.Ev4);
            Assert.AreSame(timeouts[4], cond.Ev5);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.False(cond.Ev2.Succeeded);
            Assert.False(cond.Ev3.Succeeded);
            Assert.False(cond.Ev4.Succeeded);
            Assert.False(cond.Ev5.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(3, cond.Ev4.Value);
            Assert.AreEqual(4, cond.Ev5.Value);
            Assert.AreEqual(1, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
        }

        private IEvents Or_Nested()
        {
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            var cond = timeouts[0].Or(timeouts[1]).And(timeouts[2]);
            Assert.AreSame(timeouts[0], cond.Ev1);
            Assert.AreSame(timeouts[1], cond.Ev2);
            Assert.AreSame(timeouts[2], cond.Ev3);
            yield return cond;
            Assert.True(cond.Ev1.Succeeded);
            Assert.True(cond.Ev2.Succeeded);
            Assert.True(cond.Ev3.Succeeded);
            Assert.AreEqual(0, cond.Ev1.Value);
            Assert.AreEqual(1, cond.Ev2.Value);
            Assert.AreEqual(2, cond.Ev3.Value);
            Assert.AreEqual(3, cond.Value.Count);
            Assert.True(cond.Value.Contains(timeouts[0]));
            Assert.True(cond.Value.Contains(timeouts[1]));
            Assert.True(cond.Value.Contains(timeouts[2]));
            Assert.AreEqual(2, _env.Now);
        }

        private IEvents IOr_WithOrCond()
        {
            var cond = _env.Timeout(1, value: 1).Or(_env.Timeout(2, value: 2));
            var cond2 = cond.Or(_env.Timeout(0, value: 0));
            yield return cond2;
            var values = cond2.Value.Select(v => v.Value).ToList();
            values.Sort();
            Assert.True(new List<object> { 0 }.SequenceEqual(values));
        }

        private IEvents IOr_WithAndCond()
        {
            var cond = _env.Timeout(1, value: 1).And(_env.Timeout(2, value: 2));
            var cond2 = cond.Or(_env.Timeout(0, value: 0));
            yield return cond2;
            var values = cond2.Value.Select(v => v.Value).ToList();
            values.Sort();
            Assert.True(new List<object> { 0 }.SequenceEqual(values));
        }

        private IEvents All_ManyStorePutEvents(Store<int> store)
        {
            var put3 = store.Put(3);
            yield return _env.AllOf(store.Put(1), store.Put(2), put3);
            Assert.True(store.ItemQueue.Contains(1));
            Assert.True(store.ItemQueue.Contains(2));
            Assert.True(store.ItemQueue.Contains(3));
        }

        private IEvents All_SucceededEvents(Store<int> store)
        {
            var put3 = store.Put(3);
            var put5 = store.Put(5);
            var put7 = store.Put(7);
            yield return _env.AllOf(put3, put5, put7);
            store.Put(9);
        }

        private IEvents ContinueOnInterrupt_Timeouts()
        {
            _env.Process(Interrupter(_env.ActiveProcess, null, 10));
            var cond = _env.Timeout(1).And(_env.Timeout(2));
            yield return cond;
            Assert.True(_env.ActiveProcess.Interrupted());
            yield return cond;
            Assert.AreEqual(2, _env.Now);
        }

        private IEvents ContinueOnInterrupt_Resource()
        {
            _env.Process(Interrupter(_env.ActiveProcess, null, 10));
            var ev1 = _env.Event();
            var ev2 = _env.Event();
            var cond = ev1.And(ev2).And(_env.Timeout(2));
            yield return cond;
            Assert.True(_env.ActiveProcess.Interrupted());
            _env.Process(EventTriggerer(ev1));
            _env.Process(EventTriggerer(ev2));
            yield return cond;
            Assert.AreEqual(2, _env.Now);
            Assert.True(ev1.Succeeded);
            Assert.True(ev2.Succeeded);
        }

        private IEvents ContinueOnInterrupt_Generic()
        {
            _env.Process(Interrupter(_env.ActiveProcess, null, 10));
            var resource = Sim.Resource(_env, 1);
            var cond = resource.Request().And(_env.Timeout(2));
            yield return cond;
            Assert.True(_env.ActiveProcess.Interrupted());
            yield return cond;
            Assert.AreEqual(2, _env.Now);
            Assert.True(resource.Users.Count() == 1);
            cond.Ev1.Dispose();
        }

        private IEvents CustomEvaluator1()
        {
            var ev1 = _env.Event();
            var ev2 = _env.Event();
            _env.Process(CustomEvaluator1_EventTrigger(ev1, ev2));
            var cond = _env.Condition(ev1, ev2, c => Equals(7, c.Ev1.Value) && Equals(3, c.Ev2.Value));
            yield return cond;
            Assert.True(ev1.Succeeded);
            Assert.True(ev2.Succeeded);
            Assert.AreSame(ev1, cond.Ev1);
            Assert.AreSame(ev2, cond.Ev2);
        }

        private IEvents CustomEvaluator1_EventTrigger(SimEvent<object> ev1, SimEvent<object> ev2)
        {
            yield return _env.Timeout(5);
            ev1.Succeed(7);
            yield return _env.Timeout(5);
            ev2.Succeed(3);
        }

        private IEvents CustomEvaluator2()
        {
            var ev1 = _env.Event();
            var ev2 = _env.Event();
            _env.Process(CustomEvaluator2_EventTrigger(ev1, ev2));
            var cond = _env.Condition(ev1, ev2, c => Equals(7, c.Ev1.Value) || Equals(3, c.Ev2.Value));
            yield return cond;
            Assert.True(ev1.Succeeded);
            Assert.False(ev2.Succeeded);
            Assert.AreSame(ev1, cond.Ev1);
            Assert.AreSame(ev2, cond.Ev2);
        }

        private IEvents CustomEvaluator2_EventTrigger(SimEvent<object> ev1, SimEvent<object> ev2)
        {
            yield return _env.Timeout(5);
            ev1.Succeed(7);
            yield return _env.Timeout(5);
            ev2.Succeed(3);
        }

        private IEvents ImmutableResults()
        {
            // Results of conditions should not change after they have been triggered.
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            // The or condition in this expression will trigger immediately. 
            // The and condition will trigger later on.
            var condition = timeouts[0].Or(timeouts[1].And(timeouts[2]));

            yield return condition;
            Assert.AreEqual(1, condition.Value.Count);
            Assert.True(condition.Value.Contains(timeouts[0]));

            // Makes sure that the results of condition were frozen. The results of
            // the nested and condition do not become visible afterwards.
            yield return _env.Timeout(2);
            Assert.AreEqual(1, condition.Value.Count);
            Assert.True(condition.Value.Contains(timeouts[0]));
        }

        private static IEvents SharedAndCondition_P1(IList<Timeout> timeouts, Condition<Timeout, Timeout> condition)
        {
            yield return condition;
            Assert.AreEqual(2, condition.Value.Count);
            Assert.True(condition.Value.Contains(timeouts[0]));
            Assert.True(condition.Value.Contains(timeouts[1]));
        }

        private static IEvents SharedAndCondition_P2(IList<Timeout> timeouts, Condition<Timeout, Timeout, Timeout> condition)
        {
            yield return condition;
            Assert.AreEqual(3, condition.Value.Count);
            Assert.True(condition.Value.Contains(timeouts[0]));
            Assert.True(condition.Value.Contains(timeouts[1]));
            Assert.True(condition.Value.Contains(timeouts[2]));
        }

        private static IEvents SharedOrCondition_P1(IList<Timeout> timeouts, Condition<Timeout, Timeout> condition)
        {
            yield return condition;
            Assert.AreEqual(1, condition.Value.Count);
            Assert.True(condition.Value.Contains(timeouts[0]));
        }

        private static IEvents SharedOrCondition_P2(IList<Timeout> timeouts, Condition<Timeout, Timeout, Timeout> condition)
        {
            yield return condition;
            Assert.AreEqual(1, condition.Value.Count);
            Assert.True(condition.Value.Contains(timeouts[0]));
        }

        private IEnumerable<SimEvent> SameEvent_AndCondition_PEM()
        {
            var t = _env.Timeout(5);
            var c = t & t;
            yield return c;
            Assert.AreEqual(2, c.Value.Count);
            Assert.AreSame(c.Value[0], t);
            Assert.AreSame(c.Value[1], t);
        }

        private IEnumerable<SimEvent> SameEvent_OrCondition_PEM()
        {
            var t = _env.Timeout(5);
            var c = t | t;
            yield return c;
            Assert.AreEqual(1, c.Value.Count);
            Assert.AreSame(c.Value[0], t);
        }

        private IEnumerable<SimEvent> SameSucceedEvent_AndCondition_PEM()
        {
            var t = _env.Timeout(5);
            yield return t;
            var c = t & t;
            yield return c;
            Assert.AreEqual(2, c.Value.Count);
            Assert.AreSame(c.Value[0], t);
            Assert.AreSame(c.Value[1], t);
        }

        private IEnumerable<SimEvent> SameSucceedEvent_OrCondition_PEM()
        {
            var t = _env.Timeout(5);
            yield return t;
            var c = t | t;
            yield return c;
            Assert.AreEqual(1, c.Value.Count);
            Assert.AreSame(c.Value[0], t);
        }

        [Test]
        public void All_ManyStorePutEvents_UnlimitedStore()
        {
            var store = Sim.Store<int>(_env);
            _env.Process(All_ManyStorePutEvents(store));
            _env.Run();
            Assert.AreEqual(3, store.ItemQueue.Count());
        }

        [Test]
        public void All_SucceededEvents_UnlimitedStore()
        {
            var store = Sim.Store<int>(_env);
            _env.Process(All_SucceededEvents(store));
            _env.Run();
            Assert.AreEqual(4, store.ItemQueue.Count());
        }

        [Test]
        public void RunContinueOnInterrupt_Generic()
        {
            _env.Process(ContinueOnInterrupt_Generic());
            _env.Run(5);
        }

        [Test]
        public void RunContinueOnInterrupt_Resource()
        {
            _env.Process(ContinueOnInterrupt_Resource());
            _env.Run(5);
        }

        [Test]
        public void RunContinueOnInterrupt_Timeouts()
        {
            _env.Process(ContinueOnInterrupt_Timeouts());
            _env.Run(5);
        }

        [Test]
        public void RunCustomEvaluator1()
        {
            _env.Process(CustomEvaluator1());
            _env.Run();
        }

        [Test]
        public void RunCustomEvaluator2()
        {
            _env.Process(CustomEvaluator2());
            _env.Run();
        }

        [Test]
        public void Run_And_Nested()
        {
            _env.Process(And_Nested());
            _env.Run();
        }

        [Test]
        public void Run_And_Simple()
        {
            _env.Process(And_Simple());
            _env.Run();
        }

        [Test]
        public void Run_And_Simple4()
        {
            _env.Process(And_Simple4());
            _env.Run();
        }

        [Test]
        public void Run_And_Simple5()
        {
            _env.Process(And_Simple5());
            _env.Run();
        }

        [Test]
        public void Run_IAnd_WithAndCond()
        {
            _env.Process(IAnd_WithAndCond());
            _env.Run();
        }

        [Test]
        public void Run_IAnd_WithOrCond()
        {
            _env.Process(IAnd_WithOrCond());
            _env.Run();
        }

        [Test]
        public void Run_IOr_WithAndCond()
        {
            _env.Process(IOr_WithAndCond());
            _env.Run();
        }

        [Test]
        public void Run_IOr_WithOrCond()
        {
            _env.Process(IOr_WithOrCond());
            _env.Run();
        }

        [Test]
        public void Run_ImmutableResults()
        {
            _env.Process(ImmutableResults());
            _env.Run();
        }

        [Test]
        public void Run_Or_Nested()
        {
            _env.Process(Or_Nested());
            _env.Run();
        }

        [Test]
        public void Run_Or_Simple()
        {
            _env.Process(Or_Simple());
            _env.Run();
        }

        [Test]
        public void Run_Or_Simple4()
        {
            _env.Process(Or_Simple4());
            _env.Run();
        }

        [Test]
        public void Run_Or_Simple5()
        {
            _env.Process(Or_Simple5());
            _env.Run();
        }

        [Test]
        public void SameEvent_AndCondition()
        {
            _env.Process(SameEvent_AndCondition_PEM());
            _env.Run();
        }

        [Test]
        public void SameEvent_OrCondition()
        {
            _env.Process(SameEvent_OrCondition_PEM());
            _env.Run();
        }

        [Test]
        public void SameSucceedEvent_AndCondition()
        {
            _env.Process(SameSucceedEvent_AndCondition_PEM());
            _env.Run();
        }

        [Test]
        public void SameSucceedEvent_OrCondition()
        {
            _env.Process(SameSucceedEvent_OrCondition_PEM());
            _env.Run();
        }

        [Test]
        public void SharedAndCondition()
        {
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            var c1 = timeouts[0].And(timeouts[1]);
            var c2 = c1.And(timeouts[2]);

            _env.Process(SharedAndCondition_P1(timeouts, c1));
            _env.Process(SharedAndCondition_P2(timeouts, c2));
            _env.Run();
        }

        [Test]
        public void SharedOrCondition()
        {
            var timeouts = Enumerable.Range(0, 3).Select(i => _env.Timeout(i)).ToList();
            var c1 = timeouts[0].Or(timeouts[1]);
            var c2 = c1.Or(timeouts[2]);

            _env.Process(SharedOrCondition_P1(timeouts, c1));
            _env.Process(SharedOrCondition_P2(timeouts, c2));
            _env.Run();
        }
    }
}
