﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Events
{
    using System;
    using System.Diagnostics;
    using NUnit.Framework;
    using Shouldly;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class TimeoutTests : TestBase
    {
        [TestCase(TinyNeg), TestCase(SmallNeg), TestCase(LargeNeg)]
        public void Yield_Timeout_NegativeTime(double time)
        {
            _env.Process(TimeoutYielder(time));
            Should.Throw<ArgumentOutOfRangeException>(() => _env.Run(1));
        }

        [TestCase(double.PositiveInfinity), TestCase(double.NegativeInfinity), TestCase(double.NaN)]
        public void Yield_Timeout_Infinity(double time)
        {
            _env.Process(TimeoutYielder(time));
            Should.Throw<ArgumentOutOfRangeException>(() => _env.Run(1));
        }

        private SimEvents AndCombinedTimeoutYielder(double t1, double t2)
        {
            Debug.Assert(t1 < t2);
            while (true)
            {
                var start = _env.Now;
                var min = _env.Timeout(t1);
                var max = _env.Timeout(t2);
                yield return min.And(max);
                Assert.True(min.Succeeded);
                Assert.True(max.Succeeded);
                Assert.AreEqual(_env.Now, start + t2);

                start = _env.Now;
                min = _env.Timeout(t1);
                max = _env.Timeout(t2);
                yield return _env.AllOf(min, max);
                Assert.True(min.Succeeded);
                Assert.True(max.Succeeded);
                Assert.AreEqual(_env.Now, start + t2);
            }
        }

        private SimEvents OrCombinedTimeoutYielder(double t1, double t2)
        {
            Debug.Assert(t1 < t2);
            while (true)
            {
                var start = _env.Now;
                var min = _env.Timeout(t1);
                var max = _env.Timeout(t2);
                yield return min.Or(max);
                Assert.True(min.Succeeded);
                Assert.False(max.Succeeded);
                Assert.AreEqual(_env.Now, start + t1);

                start = _env.Now;
                min = _env.Timeout(t1);
                max = _env.Timeout(t2);
                yield return _env.AnyOf(min, max);
                Assert.True(min.Succeeded);
                Assert.False(max.Succeeded);
                Assert.AreEqual(_env.Now, start + t1);
            }
        }

        private SimEvents TimeoutYielder(double time)
        {
            while (true)
            {
                var start = _env.Now;
                var ev = _env.Timeout(time, 0);
                yield return ev;
                Assert.True(ev.Succeeded);
                Assert.AreEqual(_env.Now, start + time);
            }
        }

        private SimEvents TimeoutYielder_WithCallback(double time, Action<SimEvent> callback)
        {
            while (true)
            {
                var start = _env.Now;
                var ev = _env.Timeout(time);
                ev.Callbacks.Add(callback);
                yield return ev;
                Assert.True(ev.Succeeded);
                Assert.AreEqual(_env.Now, start + time);
            }
        }

        private SimEvents ManyTimesTimeoutYielder()
        {
            var ev = _env.Timeout(10);
            for (var i = 0; i < 10; ++i)
            {
                yield return ev;
            }
        }

        [Test]
        public void Simulate_EnoughTime()
        {
            _env.Process(TimeoutYielder(10));
            _env.Run(95);
            Assert.AreEqual(95, _env.Now);
        }

        [Test]
        public void Simulate_TooEarly()
        {
            _env.Process(TimeoutYielder(100));
            _env.Run(5);
            Assert.AreEqual(_env.Now, 5);
        }

        [Test]
        public void Yield_AndCombined()
        {
            _env.Process(AndCombinedTimeoutYielder(5, 10));
            _env.Run(200);
            Assert.AreEqual(200, _env.Now);
        }

        [Test]
        public void Yield_ManyTimes()
        {
            _env.Process(ManyTimesTimeoutYielder());
            _env.Run(100);
            Assert.AreEqual(10, _env.Now);
        }

        [Test]
        public void Yield_OrCombined()
        {
            _env.Process(OrCombinedTimeoutYielder(5, 10));
            _env.Run(200);
            Assert.AreEqual(200, _env.Now);
        }

        [Test]
        public void Yield_Timeout_NegativeDelay()
        {
            _env.Process(TimeoutYielder(-5));
            Should.Throw<ArgumentOutOfRangeException>(() => _env.Run(1));
        }

        [Test]
        public void Yield_WithCallback()
        {
            var x = 0;
            _env.Process(TimeoutYielder_WithCallback(10, ev => ++x));
            _env.Run(until: 25);
            Assert.AreEqual(2, x);
        }
    }
}
