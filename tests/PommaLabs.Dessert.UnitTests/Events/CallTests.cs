﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Events
{
    using NUnit.Framework;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class CallTests : TestBase
    {
        private SimEvents Caller_NoExitValue()
        {
            var w = new IntWrapper();
            w.X += 5;
            yield return _env.Timeout(5);
            var call = _env.Call(Called_NoExitValue(_env.ActiveProcess, w));
            yield return call;
            Assert.AreEqual(35, w.X);
            Assert.AreEqual(null, call.Value);
        }

        private SimEvents Caller_NoReturnValue_Nested()
        {
            var call = _env.Call(Caller_NoExitValue());
            yield return call;
        }

        private SimEvents Called_NoExitValue(SimProcess caller, IntWrapper w)
        {
            Assert.AreSame(caller, _env.ActiveProcess);
            w.X += 10;
            yield return _env.Timeout(10);
            Assert.AreSame(caller, _env.ActiveProcess);
            w.X += 20;
            yield return _env.Timeout(20);
            Assert.AreSame(caller, _env.ActiveProcess);
        }

        private SimEvents Caller_WithExitValue()
        {
            var w = new IntWrapper();
            w.X += 5;
            yield return _env.Timeout(5);
            var call = _env.Call(Called_WithExitValue(_env.ActiveProcess, w));
            yield return call;
            Assert.AreEqual(35, w.X);
            Assert.AreEqual(w, call.Value);
        }

        private SimEvents Called_WithExitValue(SimProcess caller, IntWrapper w)
        {
            Assert.AreSame(caller, _env.ActiveProcess);
            w.X += 10;
            yield return _env.Timeout(10);
            Assert.AreSame(caller, _env.ActiveProcess);
            w.X += 20;
            yield return _env.Timeout(20);
            Assert.AreSame(caller, _env.ActiveProcess);
            yield return _env.Exit(w);
            Assert.Fail();
        }

        private SimEvents Caller_WithExitValue_Nested()
        {
            var call = _env.Call(Caller_WithExitValue());
            yield return call;
        }

        private SimEvents FibonacciFunc(int n)
        {
            if (n <= 0)
            {
                yield return _env.Exit(0);
            }
            else if (n == 1)
            {
                yield return _env.Exit(1);
            }
            else
            {
                var call = _env.Call<int>(FibonacciFunc(n - 1));
                yield return call;
                var n1 = call.Value;
                call = _env.Call<int>(FibonacciFunc(n - 2));
                yield return call;
                var n2 = call.Value;
                yield return _env.Exit(n1 + n2);
            }
        }

        [TestCase(0, 0), TestCase(1, 1), TestCase(2, 1), TestCase(3, 2), TestCase(4, 3), TestCase(5, 5), TestCase(6, 8)]
        public void Fibonacci(int n, int result)
        {
            var fib = _env.Process(FibonacciFunc(n));
            _env.Run();
            Assert.AreEqual(result, fib.Value);
        }

        private sealed class IntWrapper
        {
            public int X;
        }

        [Test]
        public void NoReturnValue()
        {
            _env.Process(Caller_NoExitValue());
            _env.Run();
        }

        [Test]
        public void NoReturnValue_Nested()
        {
            _env.Process(Caller_NoReturnValue_Nested());
            _env.Run();
        }

        [Test]
        public void WithReturnValue()
        {
            _env.Process(Caller_WithExitValue());
            _env.Run();
        }

        [Test]
        public void WithReturnValue_Nested()
        {
            _env.Process(Caller_WithExitValue_Nested());
            _env.Run();
        }
    }
}
