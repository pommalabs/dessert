﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Recording
{
    using Dessert.Recording;
    using NUnit.Framework;

    internal sealed class TallyTests : RecorderTests<Tally>
    {
        protected override Tally NewBoundRecorder()
        {
            var tally = Sim.Tally(_env);
            Assert.IsNotNull(tally);
            Assert.IsInstanceOf(typeof(Tally), tally);
            return tally;
        }

        protected override Tally NewUnboundRecorder()
        {
            var tally = Sim.Tally();
            Assert.IsNotNull(tally);
            Assert.IsInstanceOf(typeof(Tally), tally);
            return tally;
        }
    }
}
