﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Recording
{
    using System.Diagnostics;
    using Dessert.Recording;
    using NUnit.Framework;

    internal sealed class MonitorTests : RecorderTests<Monitor>
    {
        protected override Monitor NewBoundRecorder()
        {
            var monitor = Sim.Monitor(_env);
            Assert.IsNotNull(monitor);
            Assert.IsInstanceOf(typeof(Monitor), monitor);
            return monitor;
        }

        protected override Monitor NewUnboundRecorder()
        {
            var monitor = Sim.Monitor();
            Assert.IsNotNull(monitor);
            Assert.IsInstanceOf(typeof(Monitor), monitor);
            return monitor;
        }

        [TestCase(1, 2, 3, 4), TestCase(1, 1, 3, 3, 5, 5), TestCase(1, 10)]
        public void Values_IntegerPairs(params int[] values)
        {
            Debug.Assert(values.Length % 2 == 0);
            for (var i = 0; i < values.Length; i += 2)
            {
                BoundRecorder.Observe(values[i], values[i + 1]);
                UnboundRecorder.Observe(values[i], values[i + 1]);
            }
            var bEn = BoundRecorder.Samples.GetEnumerator();
            var uEn = UnboundRecorder.Samples.GetEnumerator();
            for (var i = 0; i < values.Length; i += 2)
            {
                Assert.True(bEn.MoveNext());
                Assert.True(uEn.MoveNext());
                Assert.AreEqual(values[i], bEn.Current.Sample);
                Assert.AreEqual(values[i + 1], bEn.Current.Time);
                Assert.AreEqual(values[i], BoundRecorder[i / 2].Sample);
                Assert.AreEqual(values[i + 1], BoundRecorder[i / 2].Time);
                Assert.AreEqual(values[i], uEn.Current.Sample);
                Assert.AreEqual(values[i + 1], uEn.Current.Time);
                Assert.AreEqual(values[i], UnboundRecorder[i / 2].Sample);
                Assert.AreEqual(values[i + 1], UnboundRecorder[i / 2].Time);
            }
        }

        [TestCase(1.4, 2.3, 3.6, 4.1), TestCase(1.3, 1, 3, 3, 5, 5.3), TestCase(1.23, 10.01)]
        public void Values_DecimalPairs(params double[] values)
        {
            Debug.Assert(values.Length % 2 == 0);
            for (var i = 0; i < values.Length; i += 2)
            {
                BoundRecorder.Observe(values[i], values[i + 1]);
                UnboundRecorder.Observe(values[i], values[i + 1]);
            }
            var bEn = BoundRecorder.Samples.GetEnumerator();
            var uEn = UnboundRecorder.Samples.GetEnumerator();
            for (var i = 0; i < values.Length; i += 2)
            {
                Assert.True(bEn.MoveNext());
                Assert.True(uEn.MoveNext());
                Assert.AreEqual(values[i], bEn.Current.Sample);
                Assert.AreEqual(values[i + 1], bEn.Current.Time);
                Assert.AreEqual(values[i], BoundRecorder[i / 2].Sample);
                Assert.AreEqual(values[i + 1], BoundRecorder[i / 2].Time);
                Assert.AreEqual(values[i], uEn.Current.Sample);
                Assert.AreEqual(values[i + 1], uEn.Current.Time);
                Assert.AreEqual(values[i], UnboundRecorder[i / 2].Sample);
                Assert.AreEqual(values[i + 1], UnboundRecorder[i / 2].Time);
            }
        }
    }
}
