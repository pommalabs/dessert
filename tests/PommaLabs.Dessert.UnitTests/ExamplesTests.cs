﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests
{
    using System;
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Threading;
    using NUnit.Framework;
    using Samples.CSharp;
    using Samples.CSharp.SimPy2;
    using Samples.CSharp.SimPy3;
    using Samples.FSharp;
    using Samples.FSharp.SimPy2;
    using Samples.FSharp.SimPy3;
    using Samples.VisualBasic;
    using Samples.VisualBasic.SimPy3;
    using BankRenege = Samples.CSharp.SimPy3.BankRenege;
    using ClockExample = Samples.CSharp.SimPy3.ClockExample;
    using HelloWorld = Samples.CSharp.HelloWorld;
    using Message = Samples.CSharp.SimPy2.Message;

    [TestFixture]
    internal sealed class ExamplesTests
    {
        private static readonly CultureInfo s_enUsCulture = new CultureInfo("en-US");

        [SetUp]
        public void SetUp()
        {
            // Force en-US culture.
            Thread.CurrentThread.CurrentCulture = s_enUsCulture;
            Thread.CurrentThread.CurrentUICulture = s_enUsCulture;

            _loggedStream = new MemoryStream();
            _loggedWriter = new FormattedWriter(_loggedStream);
            _originalOutput = Console.Out;
            Console.SetOut(_loggedWriter);
        }

        [TearDown]
        public void TearDown()
        {
            _loggedWriter.Close();
            _lines = null;
            Console.SetOut(_originalOutput);
        }

        private readonly Encoding _encoding = Encoding.UTF8;
        private MemoryStream _loggedStream;
        private FormattedWriter _loggedWriter;
        private IEnumerator _lines;
        private TextWriter _originalOutput;

        private void AssertNoMoreLines()
        {
            Assert.False(_lines.MoveNext());
        }

        private void AssertRightLine(string expected)
        {
            Assert.True(_lines.MoveNext());
            Assert.AreEqual(expected, _lines.Current as string);
        }

        private void ReadLoggedStream()
        {
            _loggedWriter.Flush();
            var length = (int)_loggedStream.Length;
            var bytes = new byte[length];

            _loggedStream.Seek(0, SeekOrigin.Begin);
            _loggedStream.Read(bytes, 0, length);

            var text = _encoding.GetString(bytes, 0, length);
            _lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).GetEnumerator();
        }

        private sealed class FormattedWriter : StreamWriter
        {
            public FormattedWriter(Stream stream) : base(stream)
            {
            }

            public override IFormatProvider FormatProvider => s_enUsCulture;
        }

        [Test]
        public void CSharp_Dessert_ConditionOperators()
        {
            ConditionOperators.Run();
            ReadLoggedStream();
            AssertRightLine("7");
            AssertRightLine("True");
            AssertRightLine("True");
            AssertRightLine("True");
            AssertRightLine("10");
            AssertRightLine("False");
            AssertRightLine("False");
            AssertRightLine("True");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_ConditionTester()
        {
            ConditionTester.Run();
            ReadLoggedStream();
            AssertRightLine("ALL: VAL_T, VAL_P");
            AssertRightLine("ANY: VAL_T");
            AssertRightLine("CUSTOM: VAL_T, VAL_P");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_ConditionUsage()
        {
            ConditionUsage.Run();
            ReadLoggedStream();
            AssertRightLine("7");
            AssertRightLine("True");
            AssertRightLine("True");
            AssertRightLine("10");
            AssertRightLine("True");
            AssertRightLine("False");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_DataRecording()
        {
            DataRecording.Run();
            ReadLoggedStream();
            AssertRightLine("Total clients: 250");
            AssertRightLine("Average wait: 6.1");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_DelayedStart()
        {
            DelayedStart.Run();
            ReadLoggedStream();
            AssertRightLine("B: 0");
            AssertRightLine("A: 7");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_EventFailure()
        {
            EventFailure.Run();
            ReadLoggedStream();
            AssertRightLine("SOMETHING BAD HAPPENED");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_EventTrigger()
        {
            EventTrigger.Run();
            ReadLoggedStream();
            AssertRightLine("SI :)");
            AssertRightLine("NO :(");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_FibonacciProducerConsumer()
        {
            FibonacciProducerConsumer.Run();
            ReadLoggedStream();
            AssertRightLine("0");
            AssertRightLine("1");
            AssertRightLine("1");
            AssertRightLine("2");
            AssertRightLine("3");
            AssertRightLine("5");
            AssertRightLine("8");
            AssertRightLine("13");
            AssertRightLine("21");
            AssertRightLine("34");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_HelloWorld()
        {
            HelloWorld.Run();
            ReadLoggedStream();
            AssertRightLine("Hello World simulation :)");
            AssertRightLine("Hello World at 2.1!");
            AssertRightLine("Hello World at 4.2!");
            AssertRightLine("Hello World at 6.3!");
            AssertRightLine("Hello World at 8.4!");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_InterruptHandling()
        {
            InterruptHandling.Run();
            ReadLoggedStream();
            AssertRightLine("Interrupted at: NOW");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_MachineLoad()
        {
            MachineLoad.Run();
            ReadLoggedStream();
            AssertRightLine("0: Carico la macchina...");
            AssertRightLine("5: Eseguo il comando A");
            AssertRightLine("30: Carico la macchina...");
            AssertRightLine("35: Eseguo il comando A");
            AssertRightLine("60: Carico la macchina...");
            AssertRightLine("65: Eseguo il comando C");
            AssertRightLine("90: Carico la macchina...");
            AssertRightLine("95: Eseguo il comando A");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_MachineSensorsMonitoring()
        {
            MachineSensorsMonitoring.Run();
            ReadLoggedStream();
            AssertRightLine("Machine A has powered up");
            AssertRightLine("All sensors for machine A are active");
            AssertRightLine("Machine A has started work cycle 1");
            AssertRightLine("Machine B has powered up");
            AssertRightLine("All sensors for machine B are active");
            AssertRightLine("Machine B has started work cycle 1");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 997.40 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 110 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 947.45 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 101 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 954.36 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 106 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1076.12 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 96 °F");
            AssertRightLine("Machine A has ended work cycle 1");
            AssertRightLine("Machine A has started work cycle 2");
            AssertRightLine("Machine B has ended work cycle 1");
            AssertRightLine("Machine B has started work cycle 2");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 972.79 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 99 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 992.71 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 103 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 905.89 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 108 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 936.15 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 92 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1087.39 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 116 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 978.34 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 92 °F");
            AssertRightLine("Machine A has ended work cycle 2");
            AssertRightLine("Machine A has started work cycle 3");
            AssertRightLine("Machine B has ended work cycle 2");
            AssertRightLine("Machine B has started work cycle 3");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 998.99 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 105 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 966.05 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 102 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1035.52 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 97 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 921.31 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 101 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 966.60 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 117 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 961.30 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 96 °F");
            AssertRightLine("Machine A has ended work cycle 3");
            AssertRightLine("Machine A has started work cycle 4");
            AssertRightLine("Machine B has ended work cycle 3");
            AssertRightLine("Machine B has started work cycle 4");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1068.46 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 114 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 856.30 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 108 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1000.77 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 90 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 957.20 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 91 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 989.40 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 94 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 950.24 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 95 °F");
            AssertRightLine("Machine A has ended work cycle 4");
            AssertRightLine("Machine A has started work cycle 5");
            AssertRightLine("Machine B has ended work cycle 4");
            AssertRightLine("Machine B has started work cycle 5");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1003.31 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 108 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 971.04 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 82 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1001.22 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 100 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 944.30 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 101 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 924.04 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 95 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1098.92 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 101 °F");
            AssertRightLine("Machine A has ended work cycle 5");
            AssertRightLine("Machine A has started work cycle 6");
            AssertRightLine("Machine B has ended work cycle 5");
            AssertRightLine("Machine B has started work cycle 6");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1109.04 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 113 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 964.90 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 70 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 931.58 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 94 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1006.74 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 104 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1018.87 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 83 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 962.74 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 92 °F");
            AssertRightLine("Machine A has ended work cycle 6");
            AssertRightLine("Machine A has started work cycle 7");
            AssertRightLine("Machine B has ended work cycle 6");
            AssertRightLine("Machine B has started work cycle 7");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1086.29 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 104 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 934.96 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 95 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1090.73 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 109 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1054.90 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 98 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 977.59 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 107 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1058.40 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 84 °F");
            AssertRightLine("Machine A has ended work cycle 7");
            AssertRightLine("Machine A has started work cycle 8");
            AssertRightLine("Machine B has ended work cycle 7");
            AssertRightLine("Machine B has started work cycle 8");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1013.50 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 97 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1034.28 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 89 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1053.95 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 99 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 862.67 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 96 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 949.00 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 100 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 991.27 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 111 °F");
            AssertRightLine("Machine A has ended work cycle 8");
            AssertRightLine("Machine A has started work cycle 9");
            AssertRightLine("Machine B has ended work cycle 8");
            AssertRightLine("Machine B has started work cycle 9");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1090.94 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 104 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1035.74 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 87 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 942.61 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 86 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1042.32 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 97 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 977.70 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 103 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 936.79 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 109 °F");
            AssertRightLine("Machine A has ended work cycle 9");
            AssertRightLine("Machine A has started work cycle 10");
            AssertRightLine("Machine B has ended work cycle 9");
            AssertRightLine("Machine B has started work cycle 10");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1077.06 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 99 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1029.76 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 106 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 1052.47 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 96 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1074.99 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 87 °F");
            AssertRightLine("Pressure sensor for machine A has recorded a pressure of 970.45 bar");
            AssertRightLine("Temperature sensor for machine A has recorded a temperature of 120 °F");
            AssertRightLine("Pressure sensor for machine B has recorded a pressure of 1020.43 bar");
            AssertRightLine("Temperature sensor for machine B has recorded a temperature of 94 °F");
            AssertRightLine("Machine A average pressure: 1008.55 bar");
            AssertRightLine("Machine A average temperature: 102.52 °F");
            AssertRightLine("Machine B average pressure: 985.11 bar");
            AssertRightLine("Machine B average temperature: 95.86 °F");
            AssertRightLine("Machine B pressure values: 947.45, 1076.12, 992.71, 936.15, 978.34, 966.05, 921.31, 961.30, 856.30, 957.20, 950.24, 971.04, 944.30, 1098.92, 964.90, 1006.74, 962.74, 934.96, 1054.90, 1058.40, 1034.28, 862.67, 991.27, 1035.74, 1042.32, 936.79, 1029.76, 1074.99, 1020.43");
            AssertRightLine("Machine B temperature values: 101, 96, 103, 92, 92, 102, 101, 96, 108, 91, 95, 82, 101, 101, 70, 104, 92, 95, 98, 84, 89, 96, 111, 87, 97, 109, 106, 87, 94");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_MyFirstSimulation()
        {
            MyFirstSimulation.Run();
            ReadLoggedStream();
            AssertRightLine("Hi, I'm Gino and I will wait for 5 minutes!");
            AssertRightLine("Hi, I'm Dino and I will wait for 5 minutes!");
            AssertRightLine("Ok, Gino's wait has finished at 5. Bye :)");
            AssertRightLine("Ok, Dino's wait has finished at 8. Bye :)");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_MySecondSimulation()
        {
            MySecondSimulation.Run();
            ReadLoggedStream();
            AssertRightLine("Hi, I'm Nino and I entered the office at 0.00");
            AssertRightLine("Finally it's Nino's turn! I waited 0.00 minutes");
            AssertRightLine("Nino's job will take 1.25 minutes");
            AssertRightLine("Hi, I'm Dino and I entered the office at 0.10");
            AssertRightLine("Finally it's Dino's turn! I waited 0.00 minutes");
            AssertRightLine("Dino's job will take 7.43 minutes");
            AssertRightLine("Ok, Nino leaves the office at 1.25. Bye :)");
            AssertRightLine("Hi, I'm Pino and I entered the office at 3.82");
            AssertRightLine("Finally it's Pino's turn! I waited 0.00 minutes");
            AssertRightLine("Pino's job will take 2.04 minutes");
            AssertRightLine("Hi, I'm Bobb and I entered the office at 4.10");
            AssertRightLine("Ok, Pino leaves the office at 5.85. Bye :)");
            AssertRightLine("Finally it's Bobb's turn! I waited 1.75 minutes");
            AssertRightLine("Bobb's job will take 0.12 minutes");
            AssertRightLine("Ok, Bobb leaves the office at 5.97. Bye :)");
            AssertRightLine("Ok, Dino leaves the office at 7.53. Bye :)");
            AssertRightLine("Hi, I'm Bobb and I entered the office at 11.87");
            AssertRightLine("Finally it's Bobb's turn! I waited 0.00 minutes");
            AssertRightLine("Bobb's job will take 6.82 minutes");
            AssertRightLine("Hi, I'm Gino and I entered the office at 14.97");
            AssertRightLine("Finally it's Gino's turn! I waited 0.00 minutes");
            AssertRightLine("Gino's job will take 2.58 minutes");
            AssertRightLine("Hi, I'm Nino and I entered the office at 17.35");
            AssertRightLine("Ok, Gino leaves the office at 17.55. Bye :)");
            AssertRightLine("Finally it's Nino's turn! I waited 0.20 minutes");
            AssertRightLine("Nino's job will take 1.97 minutes");
            AssertRightLine("Ok, Bobb leaves the office at 18.69. Bye :)");
            AssertRightLine("Ok, Nino leaves the office at 19.51. Bye :)");
            AssertRightLine("Hi, I'm Pino and I entered the office at 19.82");
            AssertRightLine("Finally it's Pino's turn! I waited 0.00 minutes");
            AssertRightLine("Pino's job will take 4.02 minutes");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_NamedEvents()
        {
            TimeoutsWithValues.Run();
            ReadLoggedStream();
            AssertRightLine("5");
            AssertRightLine("A");
            AssertRightLine("B");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_PastaCooking()
        {
            PastaCooking.Run();
            ReadLoggedStream();
            AssertRightLine("Pasta in cottura per 9.9 minuti");
            AssertRightLine("Pasta ben cotta!!!");
            AssertRightLine("Pasta in cottura per 9.2 minuti");
            AssertRightLine("Pasta ben cotta!!!");
            AssertRightLine("Pasta in cottura per 9.6 minuti");
            AssertRightLine("Pasta ben cotta!!!");
            AssertRightLine("Pasta in cottura per 9.6 minuti");
            AssertRightLine("Pasta ben cotta!!!");
            AssertRightLine("Pasta in cottura per 9.9 minuti");
            AssertRightLine("Pasta ben cotta!!!");
            AssertRightLine("Pasta in cottura per 11.3 minuti");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_PublicToilet()
        {
            PublicToilet.Run();
            ReadLoggedStream();
            AssertRightLine("0.00: Donna0 in coda");
            AssertRightLine("0.00: Donna0 --> Bagno");
            AssertRightLine("1.24: Donna1 in coda");
            AssertRightLine("1.64: Donna0 <-- Bagno");
            AssertRightLine("1.64: Donna1 --> Bagno");
            AssertRightLine("2.82: Donna2 in coda");
            AssertRightLine("3.23: Donna3 in coda");
            AssertRightLine("3.77: Donna4 in coda");
            AssertRightLine("3.91: Donna5 in coda");
            AssertRightLine("5.64: Donna6 in coda");
            AssertRightLine("5.78: Uomo7 in coda");
            AssertRightLine("5.78: Uomo7 --> Bagno");
            AssertRightLine("5.81: Donna8 in coda");
            AssertRightLine("6.71: Uomo9 in coda");
            AssertRightLine("7.16: Uomo7 <-- Bagno");
            AssertRightLine("7.16: Uomo9 --> Bagno");
            AssertRightLine("7.95: Uomo9 <-- Bagno");
            AssertRightLine("8.01: Uomo10 in coda");
            AssertRightLine("8.01: Uomo10 --> Bagno");
            AssertRightLine("8.29: Donna11 in coda");
            AssertRightLine("9.50: Donna12 in coda");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_ResourcePolicy()
        {
            ResourcePolicy.Run();
            ReadLoggedStream();
            AssertRightLine("A: 6");
            AssertRightLine("B: 5");
            AssertRightLine("C: 0");
            AssertRightLine("D: 8");
            AssertRightLine("E: 2");
            AssertRightLine("F: 3");
            AssertRightLine("G: 9");
            AssertRightLine("H: 4");
            AssertRightLine("I: 1");
            AssertRightLine("J: 7");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_TargetShooting()
        {
            TargetShooting.Run();
            ReadLoggedStream();
            AssertRightLine("1: Alieno mancato, no...");
            AssertRightLine("2: Alieno colpito, si!");
            AssertRightLine("9: Pollo colpito, si!");
            AssertRightLine("21: Alieno mancato, no...");
            AssertRightLine("24: Alieno colpito, si!");
            AssertRightLine("42: Unicorno mancato, no...");
            AssertRightLine("58: Pollo colpito, si!");
            AssertRightLine("73: Alieno mancato, no...");
            AssertRightLine("92: Unicorno colpito, si!");
            AssertRightLine("96: Alieno colpito, si!");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_Dessert_TrainInterrupt()
        {
            TrainInterrupt.Run();
            ReadLoggedStream();
            AssertRightLine("Treno in viaggio per 30.22 minuti");
            AssertRightLine("Arrivo in stazione, attesa passeggeri");
            AssertRightLine("Treno in viaggio per 12.41 minuti");
            AssertRightLine("Arrivo in stazione, attesa passeggeri");
            AssertRightLine("Treno in viaggio per 3.27 minuti");
            AssertRightLine("Arrivo in stazione, attesa passeggeri");
            AssertRightLine("Al minuto 50.00: FRENO EMERGENZA");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy2_BusBreakdown()
        {
            BusBreakdown.Main();
            ReadLoggedStream();
            AssertRightLine("Breakdown Bus at 300");
            AssertRightLine("Bus repaired at 320");
            AssertRightLine("Breakdown Bus at 620");
            AssertRightLine("Bus repaired at 640");
            AssertRightLine("Breakdown Bus at 940");
            AssertRightLine("Bus repaired at 960");
            AssertRightLine("Bus has arrived at 1060");
            AssertRightLine("Dessert: No more events at time 1260");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy2_Customer()
        {
            Customer.Main();
            ReadLoggedStream();
            AssertRightLine("Starting simulation");
            AssertRightLine("Here I am at the shop Marta");
            AssertRightLine("I just bought something Marta");
            AssertRightLine("I just bought something Marta");
            AssertRightLine("I just bought something Marta");
            AssertRightLine("I just bought something Marta");
            AssertRightLine("All I have left is 60 I am going home Marta");
            AssertRightLine("Current time is 30");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy2_Message()
        {
            Message.Run();
            ReadLoggedStream();
            AssertRightLine("Starting simulation");
            AssertRightLine("0 1 Starting");
            AssertRightLine("6 2 Starting");
            AssertRightLine("100 1 Arrived");
            AssertRightLine("106 2 Arrived");
            AssertRightLine("Current time is 106");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy3_BankRenege()
        {
            BankRenege.Run();
            ReadLoggedStream();
            AssertRightLine("Bank renege");
            AssertRightLine("00.0000 Customer00: Here I am");
            AssertRightLine("00.0000 Customer00: Waited 0.000");
            AssertRightLine("03.7438 Customer00: Finished");
            AssertRightLine("09.8206 Customer01: Here I am");
            AssertRightLine("09.8206 Customer01: Waited 0.000");
            AssertRightLine("14.9512 Customer02: Here I am");
            AssertRightLine("17.6836 Customer02: RENEGED after 2.732");
            AssertRightLine("32.1158 Customer01: Finished");
            AssertRightLine("43.4099 Customer03: Here I am");
            AssertRightLine("43.4099 Customer03: Waited 0.000");
            AssertRightLine("48.4996 Customer04: Here I am");
            AssertRightLine("51.1645 Customer04: RENEGED after 2.665");
            AssertRightLine("90.0085 Customer03: Finished");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy3_CarDriver()
        {
            CarDriver.Run();
            ReadLoggedStream();
            AssertRightLine("Start parking and charging at 0");
            AssertRightLine("Was interrupted. Hope, the battery is full enough ...");
            AssertRightLine("Start driving at 3");
            AssertRightLine("Start parking and charging at 5");
            AssertRightLine("Start driving at 10");
            AssertRightLine("Start parking and charging at 12");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy3_Clock()
        {
            ClockExample.Run();
            ReadLoggedStream();
            AssertRightLine("fast 0.0");
            AssertRightLine("slow 0.0");
            AssertRightLine("fast 0.5");
            AssertRightLine("slow 1.0");
            AssertRightLine("fast 1.0");
            AssertRightLine("fast 1.5");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy3_GasStation()
        {
            GasStation.Run();
            ReadLoggedStream();
            AssertRightLine("Gas Station refueling");
            AssertRightLine("Car 0 arriving at gas station at 131.0");
            AssertRightLine("Car 0 finished refueling in 15.5 seconds.");
            AssertRightLine("Car 1 arriving at gas station at 417.0");
            AssertRightLine("Car 1 finished refueling in 21.0 seconds.");
            AssertRightLine("Car 2 arriving at gas station at 608.0");
            AssertRightLine("Car 2 finished refueling in 22.0 seconds.");
            AssertRightLine("Car 3 arriving at gas station at 680.0");
            AssertRightLine("Car 3 finished refueling in 16.5 seconds.");
            AssertRightLine("Car 4 arriving at gas station at 943.0");
            AssertRightLine("Calling tank truck at 950");
            AssertRightLine("Car 4 finished refueling in 22.5 seconds.");
            AssertRightLine("Car 5 arriving at gas station at 1164.0");
            AssertRightLine("Tank truck arriving at time 1250");
            AssertRightLine("Tank truck refueling 195.0 liters.");
            AssertRightLine("Car 5 finished refueling in 100.5 seconds.");
            AssertRightLine("Car 6 arriving at gas station at 1455.0");
            AssertRightLine("Car 6 finished refueling in 21.0 seconds.");
            AssertRightLine("Car 7 arriving at gas station at 1542.0");
            AssertRightLine("Car 7 finished refueling in 19.5 seconds.");
            AssertRightLine("Car 8 arriving at gas station at 1621.0");
            AssertRightLine("Car 8 finished refueling in 18.5 seconds.");
            AssertRightLine("Car 9 arriving at gas station at 1792.0");
            AssertRightLine("Calling tank truck at 1800");
            AssertRightLine("Car 9 finished refueling in 16.5 seconds.");
            AssertRightLine("Car 10 arriving at gas station at 1900.0");
            AssertRightLine("Car 11 arriving at gas station at 1967.0");
            AssertNoMoreLines();
        }

        [Test]
        public void CSharp_SimPy3_MovieRenege()
        {
            MovieRenege.Run();
            ReadLoggedStream();
            AssertRightLine("Movie renege");
            AssertRightLine("Movie \".NET Unchained\" sold out 41.3 minutes after ticket counter opening.");
            AssertRightLine("  Number of people leaving queue when film sold out: 16");
            AssertRightLine("Movie \"Kill Process\" sold out 39.3 minutes after ticket counter opening.");
            AssertRightLine("  Number of people leaving queue when film sold out: 9");
            AssertRightLine("Movie \"Pulp Implementation\" sold out 38.3 minutes after ticket counter opening.");
            AssertRightLine("  Number of people leaving queue when film sold out: 16");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_Dessert_BankExample()
        {
            BankExample.run();
            ReadLoggedStream();
            AssertRightLine("Finanze totali al tempo 300.00: 4037");
            AssertRightLine("Clienti entrati: 102");
            AssertRightLine("Clienti serviti: 76");
            AssertRightLine("Tempo medio di attesa: 27.99");
            AssertRightLine("Tempo medio di servizio: 8.48");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_Dessert_HelloWorld()
        {
            Samples.FSharp.HelloWorld.run();
            ReadLoggedStream();
            AssertRightLine("Hello World simulation :)");
            AssertRightLine("Hello World at 2.1!");
            AssertRightLine("Hello World at 4.2!");
            AssertRightLine("Hello World at 6.3!");
            AssertRightLine("Hello World at 8.4!");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_Dessert_WaterDrinkers()
        {
            WaterDrinkers.run();
            ReadLoggedStream();
            AssertRightLine("5.000000: 0 ha bevuto!");
            AssertRightLine("10.000000: 1 ha bevuto!");
            AssertRightLine("15.000000: 2 ha bevuto!");
            AssertRightLine("20.000000: 3 ha bevuto!");
            AssertRightLine("25.000000: 4 chiama tecnico");
            AssertRightLine("25.000000: 4 ha bevuto!");
            AssertRightLine("30.000000: 5 ha bevuto!");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy2_Client()
        {
            Client.run();
            ReadLoggedStream();
            AssertRightLine("c1 requests 1 unit at time = 0");
            AssertRightLine("c2 requests 1 unit at time = 0");
            AssertRightLine("c3 requests 1 unit at time = 0");
            AssertRightLine("c4 requests 1 unit at time = 0");
            AssertRightLine("c5 requests 1 unit at time = 0");
            AssertRightLine("c6 requests 1 unit at time = 0");
            AssertRightLine("c1 done at time = 100");
            AssertRightLine("c2 done at time = 100");
            AssertRightLine("c3 done at time = 200");
            AssertRightLine("c4 done at time = 200");
            AssertRightLine("c5 done at time = 300");
            AssertRightLine("c6 done at time = 300");
            AssertRightLine("Request order: ['c1', 'c2', 'c3', 'c4', 'c5', 'c6']");
            AssertRightLine("Service order: ['c1', 'c2', 'c3', 'c4', 'c5', 'c6']");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy2_ClientPriority()
        {
            ClientPriority.run();
            ReadLoggedStream();
            AssertRightLine("c1 requests 1 unit at time = 0");
            AssertRightLine("c2 requests 1 unit at time = 0");
            AssertRightLine("c3 requests 1 unit at time = 0");
            AssertRightLine("c4 requests 1 unit at time = 0");
            AssertRightLine("c5 requests 1 unit at time = 0");
            AssertRightLine("c6 requests 1 unit at time = 0");
            AssertRightLine("c1 done at time = 100");
            AssertRightLine("c2 done at time = 100");
            AssertRightLine("c6 done at time = 200");
            AssertRightLine("c5 done at time = 200");
            AssertRightLine("c4 done at time = 300");
            AssertRightLine("c3 done at time = 300");
            AssertRightLine("Request order: ['c1', 'c2', 'c3', 'c4', 'c5', 'c6']");
            AssertRightLine("Service order: ['c1', 'c2', 'c6', 'c5', 'c4', 'c3']");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy2_Message()
        {
            Samples.FSharp.SimPy2.Message.run();
            ReadLoggedStream();
            AssertRightLine("Starting simulation");
            AssertRightLine("0 1 Starting");
            AssertRightLine("6 2 Starting");
            AssertRightLine("100 1 Arrived");
            AssertRightLine("106 2 Arrived");
            AssertRightLine("Current time is 106");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy3_BankRenege()
        {
            Samples.FSharp.SimPy3.BankRenege.run();
            ReadLoggedStream();
            AssertRightLine("Bank renege");
            AssertRightLine("00.0000 Customer00: Here I am");
            AssertRightLine("00.0000 Customer00: Waited 0.000");
            AssertRightLine("03.7438 Customer00: Finished");
            AssertRightLine("09.8206 Customer01: Here I am");
            AssertRightLine("09.8206 Customer01: Waited 0.000");
            AssertRightLine("14.9512 Customer02: Here I am");
            AssertRightLine("17.6836 Customer02: RENEGED after 2.732");
            AssertRightLine("32.1158 Customer01: Finished");
            AssertRightLine("43.4099 Customer03: Here I am");
            AssertRightLine("43.4099 Customer03: Waited 0.000");
            AssertRightLine("48.4996 Customer04: Here I am");
            AssertRightLine("51.1645 Customer04: RENEGED after 2.665");
            AssertRightLine("90.0085 Customer03: Finished");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy3_Car()
        {
            Car.run();
            ReadLoggedStream();
            AssertRightLine("Start parking at 0");
            AssertRightLine("Start driving at 5");
            AssertRightLine("Start parking at 7");
            AssertRightLine("Start driving at 12");
            AssertRightLine("Start parking at 14");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy3_CarCharge()
        {
            CarCharge.run();
            ReadLoggedStream();
            AssertRightLine("Start parking and charging at 0");
            AssertRightLine("Start driving at 5");
            AssertRightLine("Start parking and charging at 7");
            AssertRightLine("Start driving at 12");
            AssertRightLine("Start parking and charging at 14");
            AssertNoMoreLines();
        }

        [Test]
        public void FSharp_SimPy3_Clock()
        {
            Clock.run();
            ReadLoggedStream();
            AssertRightLine("fast 0.0");
            AssertRightLine("slow 0.0");
            AssertRightLine("fast 0.5");
            AssertRightLine("slow 1.0");
            AssertRightLine("fast 1.0");
            AssertRightLine("fast 1.5");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_EventCallbacks()
        {
            EventCallbacks.Run();
            ReadLoggedStream();
            AssertRightLine("Successo: 'True'; Valore: 'SI'");
            AssertRightLine("Successo: 'False'; Valore: 'NO'");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_HelloWorld()
        {
            Samples.VisualBasic.HelloWorld.Run();
            ReadLoggedStream();
            AssertRightLine("Hello World simulation :)");
            AssertRightLine("Hello World at 2.1!");
            AssertRightLine("Hello World at 4.2!");
            AssertRightLine("Hello World at 6.3!");
            AssertRightLine("Hello World at 8.4!");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_Hospital()
        {
            Hospital.Run();
            ReadLoggedStream();
            AssertRightLine("Pino viene curato...");
            AssertRightLine("Gino viene curato...");
            AssertRightLine("Tino viene curato...");
            AssertRightLine("Dino viene curato...");
            AssertRightLine("Nino viene curato...");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_HospitalPreemption()
        {
            HospitalPreemption.Run();
            ReadLoggedStream();
            AssertRightLine("Pino viene curato...");
            AssertRightLine("Gino viene curato...");
            AssertRightLine("Gino scavalcato da Tino");
            AssertRightLine("Tino viene curato...");
            AssertRightLine("Cure finite per Pino");
            AssertRightLine("Dino viene curato...");
            AssertRightLine("Cure finite per Tino");
            AssertRightLine("Nino viene curato...");
            AssertRightLine("Cure finite per Dino");
            AssertRightLine("Cure finite per Nino");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_ProducerConsumer()
        {
            ProducerConsumer.Run();
            ReadLoggedStream();
            AssertRightLine("1: Prodotto un 1");
            AssertRightLine("5: Prodotto un 1");
            AssertRightLine("14: Consumato un 1");
            AssertRightLine("14: Prodotto un 13");
            AssertRightLine("26: Consumato un 1");
            AssertRightLine("26: Prodotto un 6");
            AssertRightLine("29: Consumato un 13");
            AssertRightLine("29: Prodotto un 17");
            AssertRightLine("39: Consumato un 6");
            AssertRightLine("39: Prodotto un 15");
            AssertRightLine("58: Consumato un 17");
            AssertRightLine("58: Prodotto un 8");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_ProducerFilteredConsumer()
        {
            ProducerFilteredConsumer.Run();
            ReadLoggedStream();
            AssertRightLine("1: Prodotto un 4");
            AssertRightLine("2: Prodotto un 6");
            AssertRightLine("14: PARI, consumato un 4");
            AssertRightLine("14: Prodotto un 6");
            AssertRightLine("26: PARI, consumato un 6");
            AssertRightLine("26: Prodotto un 17");
            AssertRightLine("26: DISPARI, consumato un 17");
            AssertRightLine("26: Prodotto un 3");
            AssertRightLine("30: PARI, consumato un 6");
            AssertRightLine("36: Prodotto un 15");
            AssertRightLine("43: DISPARI, consumato un 3");
            AssertRightLine("43: Prodotto un 8");
            AssertRightLine("49: PARI, consumato un 8");
            AssertRightLine("49: Prodotto un 18");
            AssertRightLine("57: DISPARI, consumato un 15");
            AssertRightLine("57: Prodotto un 17");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_Dessert_ValueUsage()
        {
            ValueUsage.Run();
            ReadLoggedStream();
            AssertRightLine("BORING");
            AssertRightLine("12.5");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_SimPy3_Clock()
        {
            Samples.VisualBasic.SimPy3.ClockExample.Run();
            ReadLoggedStream();
            AssertRightLine("fast 0.0");
            AssertRightLine("slow 0.0");
            AssertRightLine("fast 0.5");
            AssertRightLine("slow 1.0");
            AssertRightLine("fast 1.0");
            AssertRightLine("fast 1.5");
            AssertNoMoreLines();
        }

        [Test]
        public void VisualBasic_SimPy3_EventLatency()
        {
            EventLatency.Run();
            ReadLoggedStream();
            AssertRightLine("Event Latency");
            AssertRightLine("Received this at 15 while Sender sent this at 5");
            AssertRightLine("Received this at 20 while Sender sent this at 10");
            AssertRightLine("Received this at 25 while Sender sent this at 15");
            AssertRightLine("Received this at 30 while Sender sent this at 20");
            AssertRightLine("Received this at 35 while Sender sent this at 25");
            AssertRightLine("Received this at 40 while Sender sent this at 30");
            AssertRightLine("Received this at 45 while Sender sent this at 35");
            AssertRightLine("Received this at 50 while Sender sent this at 40");
            AssertRightLine("Received this at 55 while Sender sent this at 45");
            AssertRightLine("Received this at 60 while Sender sent this at 50");
            AssertRightLine("Received this at 65 while Sender sent this at 55");
            AssertRightLine("Received this at 70 while Sender sent this at 60");
            AssertRightLine("Received this at 75 while Sender sent this at 65");
            AssertRightLine("Received this at 80 while Sender sent this at 70");
            AssertRightLine("Received this at 85 while Sender sent this at 75");
            AssertRightLine("Received this at 90 while Sender sent this at 80");
            AssertRightLine("Received this at 95 while Sender sent this at 85");
            AssertNoMoreLines();
        }
    }
}
