﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using Dessert.Events;
    using NUnit.Framework;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    [TestFixture]
    internal abstract class TestBase
    {
        [SetUp]
        public virtual void SetUp()
        {
            _env = Sim.Environment();
            Assert.IsNotNull(_env);
            Assert.IsInstanceOf(typeof(SimEnvironment), _env);
            Assert.AreEqual(0, _env.Now);
        }

        [TearDown]
        public virtual void TearDown()
        {
            _env = null;
        }

        protected const double TinyNeg = -0.01;
        protected const double SmallNeg = -1;
        protected const double LargeNeg = -100;

        private const double Delta = 0.000001;
        private const double Epsilon = 0.15; // Relative error: less than 15%

        protected SimEnvironment _env;

        protected static SimEvents EmptyProcess()
        {
            yield break;
        }

        protected static SimEvents EventTriggerer(SimEvent<object> ev, object value = null)
        {
            ev.Succeed(value);
            yield break;
        }

        protected SimEvents Interrupter(SimProcess victim, string value, double timeout = 0)
        {
            while (true)
            {
                victim.Interrupt(value);
                yield return _env.Timeout(timeout);
            }
        }

        protected SimEvents TimeoutYielder()
        {
            yield return _env.Timeout(5);
        }

        protected static void ApproxEquals(double expected, double observed)
        {
            if (double.IsNaN(expected))
            {
                Assert.Fail("NaN should not be returned");
            }
            var errMsg = string.Format("Expected {0}, observed {1}", expected, observed);
            if (expected > -Delta && expected < Delta)
            {
                Assert.True(Math.Abs(expected - observed) < Epsilon, errMsg);
            }
            else
            {
                Assert.True(Math.Abs((expected - observed) / expected) < Epsilon, errMsg);
            }
        }
    }

    /// <summary>
    ///   Taken from following page:
    ///   http://blogs.iis.net/yigalatz/archive/2011/03/31/unit-tests-should-not-debug-assert.aspx
    ///   The code was then edited to better suit this project needs.
    /// </summary>
    [SetUpFixture]
    public class DebugPopupRemover
    {
        [OneTimeSetUp]
        public static void SetUp()
        {
            var removeListener = Trace.Listeners.OfType<DefaultTraceListener>().FirstOrDefault();
            if (removeListener != null)
            {
                Trace.Listeners.Remove(removeListener);
                Trace.Listeners.Add(new FailOnAssert());
            }
        }

        private sealed class FailOnAssert : TraceListener
        {
            public override void Fail(string message)
            {
                var errMsg = "DEBUG.ASSERT: " + message;
                Console.WriteLine(errMsg);
                Assert.Fail(errMsg);
            }

            public override void Fail(string message, string detailMessage)
            {
                var errMsg = "DEBUG.ASSERT: " + message + Environment.NewLine + detailMessage;
                Console.WriteLine(errMsg);
                Assert.Fail(errMsg);
            }

            public override void Write(string message)
            {
            }

            public override void WriteLine(string message)
            {
            }
        }
    }
}
