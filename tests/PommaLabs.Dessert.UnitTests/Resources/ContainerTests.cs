﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Resources
{
    using System;
    using System.Collections.Generic;
    using Dessert.Resources;
    using NUnit.Framework;
    using Shouldly;

    internal sealed class ContainerTests : TestBase
    {
        [TestCase(0), TestCase(TinyNeg), TestCase(SmallNeg), TestCase(LargeNeg)]
        public void Construction_InvalidCapacity(double capacity)
        {
            Should.Throw<ArgumentOutOfRangeException>(() => Sim.Container(_env, capacity));
        }

        [TestCase(0, 0), TestCase(TinyNeg, TinyNeg), TestCase(SmallNeg, SmallNeg), TestCase(LargeNeg, LargeNeg),
         TestCase(1, TinyNeg), TestCase(1, SmallNeg), TestCase(1, LargeNeg), TestCase(0.1, TinyNeg),
         TestCase(0.1, SmallNeg), TestCase(0.1, LargeNeg), TestCase(0.1, 0.2), TestCase(1, 2)]
        public void Construction_InvalidQuantityArguments(double capacity, double level)
        {
            Should.Throw<ArgumentOutOfRangeException>(() => Sim.Container(_env, capacity, level));
        }

        [TestCase(0, 0), TestCase(TinyNeg, TinyNeg), TestCase(SmallNeg, SmallNeg), TestCase(LargeNeg, LargeNeg),
         TestCase(1, TinyNeg), TestCase(1, SmallNeg), TestCase(1, LargeNeg), TestCase(0.1, TinyNeg),
         TestCase(0.1, SmallNeg), TestCase(0.1, LargeNeg), TestCase(0.1, 0.2), TestCase(1, 2)]
        public void Construction_InvalidQuantityArguments_WithGetPolicy(double capacity, double level)
        {
            Should.Throw<ArgumentOutOfRangeException>(() => Sim.Container(_env, capacity, level, WaitPolicy.Random, WaitPolicy.FIFO));
        }

        [TestCase(0, 0), TestCase(TinyNeg, TinyNeg), TestCase(SmallNeg, SmallNeg), TestCase(LargeNeg, LargeNeg),
         TestCase(1, TinyNeg), TestCase(1, SmallNeg), TestCase(1, LargeNeg), TestCase(0.1, TinyNeg),
         TestCase(0.1, SmallNeg), TestCase(0.1, LargeNeg), TestCase(0.1, 0.2), TestCase(1, 2)]
        public void Construction_InvalidQuantityArguments_WithBothPolicies(double capacity, double level)
        {
            Should.Throw<ArgumentOutOfRangeException>(() => Sim.Container(_env, capacity, level, WaitPolicy.Random, WaitPolicy.Random));
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Fifo(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Get(Quantity).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Put(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Lifo(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, 0, WaitPolicy.LIFO, WaitPolicy.FIFO);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Get(Quantity).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Put(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Priority_Default(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, 0, WaitPolicy.Priority, WaitPolicy.FIFO);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Get(Quantity).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Put(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Priority_Increasing(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, 0, WaitPolicy.Priority, WaitPolicy.FIFO);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Get(Quantity, i).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Put(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Priority_Decreasing(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, 0, WaitPolicy.Priority, WaitPolicy.FIFO);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Get(Quantity, -i).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Put(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Fifo(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, Quantity * eventCount);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Put(Quantity).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Get(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Lifo(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, Quantity * eventCount, WaitPolicy.FIFO,
                                             WaitPolicy.LIFO);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Put(Quantity).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Get(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Priority_Default(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, Quantity * eventCount, WaitPolicy.FIFO,
                                             WaitPolicy.Priority);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Put(Quantity).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Get(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Priority_Increasing(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, Quantity * eventCount, WaitPolicy.FIFO,
                                             WaitPolicy.Priority);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Put(Quantity, i).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Get(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Priority_Decreasing(int eventCount)
        {
            const int Quantity = 10;
            var container = Sim.Container(_env, Quantity * eventCount, Quantity * eventCount, WaitPolicy.FIFO,
                                             WaitPolicy.Priority);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                container.Put(Quantity, -i).Callbacks.Add(e => list.Add(tmpI));
            }
            container.Get(Quantity * eventCount);
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1]);
            }
        }

        [Test]
        public void Construction_RightType()
        {
            Assert.IsInstanceOf(typeof(Container), Sim.Container(_env));
        }
    }
}
