﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Dessert.Resources;
    using NUnit.Framework;
    using PommaLabs.Hippie.Core.LinkedLists;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class ResourceTests : TestBase
    {
        private SimEvents ResourceOccupier(Resource resource, double timeout)
        {
            using (var req = resource.Request())
            {
                yield return req;
                yield return _env.Timeout(timeout);
            }
        }

        private SimEvents ResourceRequester(Resource resource, ILinkedQueue<SimProcess> completed)
        {
            using (var req = resource.Request())
            {
                yield return req;
                completed.Enqueue(_env.ActiveProcess);
                yield return _env.Timeout(10);
            }
        }

        private SimEvents ResourceRequester_WithCount(Resource resource, int expectedCount)
        {
            using (var req = resource.Request())
            {
                yield return req;
                Assert.AreEqual(expectedCount, resource.Count);
                yield return _env.Timeout(10);
            }
        }

        private SimEvents ResourceRequester_WithPriority(Resource resource, ILinkedQueue<SimProcess> completed, double priority)
        {
            using (var req = resource.Request(priority))
            {
                yield return req;
                completed.Enqueue(_env.ActiveProcess);
                yield return _env.Timeout(10);
            }
        }

        private SimEvents ResourceRequester_Occupied_WithTimeout(Resource resource, double timeout)
        {
            var req = resource.Request();
            var cond = req.Or(_env.Timeout(timeout));
            yield return cond; // Resource is occupied
            Assert.False(cond.Ev1.Succeeded);
            Assert.True(cond.Ev2.Succeeded);
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10)]
        public void Request_ManyProcesses_CountCheck(int resourceCapacity, int userCount)
        {
            var resource = Sim.Resource(_env, resourceCapacity);
            for (var i = 1; i <= userCount; ++i)
            {
                _env.Process(ResourceRequester_WithCount(resource, userCount));
            }
            _env.Run(20);
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Fifo(int resourceCapacity, int userCount)
        {
            var resource = Sim.Resource(_env, resourceCapacity);
            var completed = new LinkedQueue<SimProcess>();
            var expected = new LinkedQueue<SimProcess>();
            for (var i = 1; i <= userCount; ++i)
            {
                expected.Enqueue(_env.Process(ResourceRequester(resource, completed)));
            }
            _env.Run();
            Assert.True(expected.SequenceEqual(completed));
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Lifo(int resourceCapacity, int userCount)
        {
            var resource = Sim.Resource(_env, resourceCapacity, WaitPolicy.LIFO);
            var completed = new LinkedQueue<SimProcess>();
            var expected1 = new LinkedQueue<SimProcess>();
            var expected2 = new LinkedStack<SimProcess>();
            for (var i = 1; i <= resourceCapacity; ++i)
            {
                expected1.Enqueue(_env.Process(ResourceRequester(resource, completed)));
            }
            for (var i = resourceCapacity + 1; i <= userCount; ++i)
            {
                expected2.Push(_env.Process(ResourceRequester(resource, completed)));
            }
            _env.Run();
            Assert.True(expected1.Union(expected2).SequenceEqual(completed));
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Priority_Default(int resourceCapacity, int userCount)
        {
            var resource = Sim.Resource(_env, resourceCapacity, WaitPolicy.Priority);
            var completed = new LinkedQueue<SimProcess>();
            var expected = new LinkedQueue<SimProcess>();
            for (var i = 1; i <= userCount; ++i)
            {
                expected.Enqueue(_env.Process(ResourceRequester(resource, completed)));
            }
            _env.Run();
            Assert.True(expected.SequenceEqual(completed));
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Priority_Increasing(int resourceCapacity, int userCount)
        {
            var resource = Sim.Resource(_env, resourceCapacity, WaitPolicy.Priority);
            var completed = new LinkedQueue<SimProcess>();
            var expected = new LinkedQueue<SimProcess>();
            for (var i = 1; i <= userCount; ++i)
            {
                expected.Enqueue(_env.Process(ResourceRequester_WithPriority(resource, completed, i)));
            }
            _env.Run();
            Assert.True(expected.SequenceEqual(completed));
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Priority_Decreasing(int resourceCapacity, int userCount)
        {
            var resource = Sim.Resource(_env, resourceCapacity, WaitPolicy.Priority);
            var completed = new LinkedQueue<SimProcess>();
            var expected1 = new LinkedQueue<SimProcess>();
            var expected2 = new LinkedStack<SimProcess>();
            for (var i = 1; i <= resourceCapacity; ++i)
            {
                expected1.Enqueue(_env.Process(ResourceRequester_WithPriority(resource, completed, userCount - i)));
            }
            for (var i = resourceCapacity + 1; i <= userCount; ++i)
            {
                expected2.Push(_env.Process(ResourceRequester_WithPriority(resource, completed, userCount - i)));
            }
            _env.Run();
            Assert.True(expected1.Union(expected2).SequenceEqual(completed));
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void Request_OccupiedResource_WithTimeout(int userCount)
        {
            var resource = Sim.Resource(_env, 1);
            _env.Process(ResourceOccupier(resource, 1000));
            for (var i = 0; i < userCount; ++i)
            {
                _env.Process(ResourceRequester_Occupied_WithTimeout(resource, 10));
            }
            _env.Run();
        }

        private SimEvents Simple_PEM(Resource resource, string name, ICollection<Tuple<string, double>> log)
        {
            var req = resource.Request();
            yield return req;
            Assert.AreEqual(1, resource.Count);

            yield return _env.Timeout(1);
            var rel = resource.Release(req);
            Assert.IsInstanceOf(typeof(Resource.ReleaseEvent), rel);

            log.Add(Tuple.Create(name, _env.Now));
        }

        private SimEvents Request_DoubleDispose_WithoutYield_PEM()
        {
            var request = Sim.Resource(_env, 3).Request();
            request.Dispose();
            request.Dispose();
            yield break;
        }

        private SimEvents ContextManager_PEM(Resource resource, string name, ICollection<Tuple<string, double>> log)
        {
            using (var req = resource.Request())
            {
                yield return req;
                Assert.AreEqual(1, resource.Count);
                yield return _env.Timeout(1);
            }

            log.Add(Tuple.Create(name, _env.Now));
        }

        private SimEvents Slots_PEM(Resource resource, string name, ICollection<Tuple<string, double>> log)
        {
            using (var req = resource.Request())
            {
                yield return req;
                log.Add(Tuple.Create(name, _env.Now));
                yield return _env.Timeout(1);
            }
        }

        [Test]
        public void Construction_RightType()
        {
            Assert.IsInstanceOf(typeof(Resource), Sim.Resource(_env, 10));
        }

        [Test]
        public void ContextManager()
        {
            var resource = Sim.Resource(_env, 1);
            Assert.AreEqual(resource.Capacity, 1);
            Assert.AreEqual(resource.Count, 0);
            var log = new SinglyLinkedList<Tuple<string, double>>();
            _env.Process(ContextManager_PEM(resource, "a", log));
            _env.Process(ContextManager_PEM(resource, "b", log));
            _env.Run();

            Assert.AreEqual("a", log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("b", log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
        }

        [Test]
        public void Request_DoubleDispose_WithoutYield()
        {
            _env.Process(Request_DoubleDispose_WithoutYield_PEM());
            _env.Run();
        }

        [Test]
        public void Simple()
        {
            var resource = Sim.Resource(_env, 1);
            Assert.AreEqual(resource.Capacity, 1);
            Assert.AreEqual(resource.Count, 0);
            var log = new SinglyLinkedList<Tuple<string, double>>();
            _env.Process(Simple_PEM(resource, "a", log));
            _env.Process(Simple_PEM(resource, "b", log));
            _env.Run();

            Assert.AreEqual("a", log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("b", log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
        }

        [Test]
        public void Slots()
        {
            var resource = Sim.Resource(_env, 3);
            var log = new SinglyLinkedList<Tuple<string, double>>();
            for (var i = 0; i < 9; ++i)
            {
                _env.Process(Slots_PEM(resource, i.ToString(CultureInfo.InvariantCulture), log));
            }
            _env.Run();

            Assert.AreEqual("0", log.First.Item1);
            Assert.AreEqual(0, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("1", log.First.Item1);
            Assert.AreEqual(0, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("2", log.First.Item1);
            Assert.AreEqual(0, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("3", log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("4", log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("5", log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("6", log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("7", log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
            log.RemoveFirst();
            Assert.AreEqual("8", log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
        }
    }
}
