﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Dessert.Resources;
    using NUnit.Framework;
    using PommaLabs.Hippie.Core.LinkedLists;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class PreemptiveResourceTests : TestBase
    {
        private SimEvents ResourceOccupier(PreemptiveResource resource, double timeout)
        {
            using (var req = resource.Request())
            {
                yield return req;
                yield return _env.Timeout(timeout);
            }
        }

        private SimEvents ResourceRequester(PreemptiveResource resource, ILinkedQueue<SimProcess> completed)
        {
            using (var req = resource.Request())
            {
                yield return req;
                completed.Enqueue(_env.ActiveProcess);
                yield return _env.Timeout(10);
            }
        }

        private SimEvents ResourceRequester_Occupied_WithTimeout(PreemptiveResource resource, double timeout)
        {
            var req = resource.Request();
            var cond = req.Or(_env.Timeout(timeout));
            yield return cond; // Resource is occupied
            Assert.False(cond.Ev1.Succeeded);
            Assert.True(cond.Ev2.Succeeded);
        }

        private SimEvents ResourceRequester_WithCount(PreemptiveResource resource, int expectedCount)
        {
            using (var req = resource.Request())
            {
                yield return req;
                Assert.AreEqual(expectedCount, resource.Count);
                yield return _env.Timeout(10);
            }
        }

        private SimEvents ResourceRequester_WithPriority(PreemptiveResource resource, ILinkedQueue<SimProcess> completed,
                                                 double priority)
        {
            using (var req = resource.Request(priority, false))
            {
                yield return req;
                completed.Enqueue(_env.ActiveProcess);
                yield return _env.Timeout(10);
            }
        }

        private SimEvents Simple_PEM(int id, PreemptiveResource res, double priority,
                             ICollection<Tuple<double, int, PreemptionInfo>> log)
        {
            using (var req = res.Request(priority))
            {
                yield return req;
                yield return _env.Timeout(5);
                PreemptionInfo info;
                log.Add(_env.ActiveProcess.Preempted(out info)
                            ? Tuple.Create(_env.Now, id, info)
                            : Tuple.Create(_env.Now, id, (PreemptionInfo)null));
            }
        }

        private SimEvents WithTimeout_PEM_A(PreemptiveResource resource, double priority)
        {
            using (var req = resource.Request(priority))
            {
                yield return req;
                Assert.True(_env.ActiveProcess.Preempted());
                yield return _env.Event();
            }
        }

        private SimEvents WithTimeout_PEM_B(PreemptiveResource resource, double priority)
        {
            using (var req = resource.Request(priority))
            {
                yield return req;
            }
        }

        private SimEvents MixedPreemption_PEM(int id, PreemptiveResource resource, double priority, bool preempt,
                                      ICollection<Tuple<double, int, PreemptionInfo>> log)
        {
            using (var req = resource.Request(priority, preempt))
            {
                yield return req;
                yield return _env.Timeout(5);
                PreemptionInfo info;
                log.Add(_env.ActiveProcess.Preempted(out info)
                            ? Tuple.Create(_env.Now, id, info)
                            : Tuple.Create(_env.Now, id, (PreemptionInfo)null));
            }
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10)]
        public void Request_ManyProcesses_CountCheck(int resourceCapacity, int userCount)
        {
            var resource = Sim.PreemptiveResource(_env, resourceCapacity);
            for (var i = 1; i <= userCount; ++i)
            {
                _env.Process(ResourceRequester_WithCount(resource, userCount));
            }
            _env.Run(20);
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Priority_Default(int resourceCapacity, int userCount)
        {
            var resource = Sim.PreemptiveResource(_env, resourceCapacity);
            var completed = new LinkedQueue<SimProcess>();
            var expected = new LinkedQueue<SimProcess>();
            for (var i = 1; i <= userCount; ++i)
            {
                expected.Enqueue(_env.Process(ResourceRequester(resource, completed)));
            }
            _env.Run();
            Assert.True(expected.SequenceEqual(completed));
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Priority_Increasing(int resourceCapacity, int userCount)
        {
            var resource = Sim.PreemptiveResource(_env, resourceCapacity);
            var completed = new LinkedQueue<SimProcess>();
            var expected = new LinkedQueue<SimProcess>();
            for (var i = 1; i <= userCount; ++i)
            {
                expected.Enqueue(_env.Process(ResourceRequester_WithPriority(resource, completed, i)));
            }
            _env.Run();
            Assert.True(expected.SequenceEqual(completed));
        }

        [TestCase(10, 1), TestCase(10, 2), TestCase(10, 5), TestCase(10, 9), TestCase(10, 10), TestCase(10, 20),
         TestCase(10, 100)]
        public void Request_ManyProcesses_Priority_Decreasing(int resourceCapacity, int userCount)
        {
            var resource = Sim.PreemptiveResource(_env, resourceCapacity);
            var completed = new LinkedQueue<SimProcess>();
            var expected1 = new LinkedQueue<SimProcess>();
            var expected2 = new LinkedStack<SimProcess>();
            for (var i = 1; i <= resourceCapacity; ++i)
            {
                expected1.Enqueue(_env.Process(ResourceRequester_WithPriority(resource, completed, userCount - i)));
            }
            for (var i = resourceCapacity + 1; i <= userCount; ++i)
            {
                expected2.Push(_env.Process(ResourceRequester_WithPriority(resource, completed, userCount - i)));
            }
            _env.Run();
            Assert.True(expected1.Union(expected2).SequenceEqual(completed));
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void Request_OccupiedResource_WithTimeout(int userCount)
        {
            var resource = Sim.PreemptiveResource(_env, 1);
            _env.Process(ResourceOccupier(resource, 1000));
            for (var i = 0; i < userCount; ++i)
            {
                _env.Process(ResourceRequester_Occupied_WithTimeout(resource, 10));
            }
            _env.Run();
        }

        [Test]
        public void MixedPreemption()
        {
            var resource = Sim.PreemptiveResource(_env, 2);
            var log = new SinglyLinkedList<Tuple<double, int, PreemptionInfo>>();
            _env.Process(MixedPreemption_PEM(0, resource, 1, true, log));
            _env.Process(MixedPreemption_PEM(1, resource, 1, true, log));
            _env.DelayedProcess(MixedPreemption_PEM(2, resource, 0, false, log), delay: 1);
            var p3 = _env.DelayedProcess(MixedPreemption_PEM(3, resource, 0, true, log), delay: 1);
            _env.DelayedProcess(MixedPreemption_PEM(4, resource, 2, true, log), 2);
            _env.Run();

            Assert.AreEqual(1, log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            Assert.AreEqual(p3, log.First.Item3.By);
            Assert.AreEqual(0, log.First.Item3.UsageSince);
            log.RemoveFirst();
            Assert.AreEqual(5, log.First.Item1);
            Assert.AreEqual(0, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
            log.RemoveFirst();
            Assert.AreEqual(6, log.First.Item1);
            Assert.AreEqual(3, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
            log.RemoveFirst();
            Assert.AreEqual(10, log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
            log.RemoveFirst();
            Assert.AreEqual(11, log.First.Item1);
            Assert.AreEqual(4, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
        }

        [Test]
        public void Simple()
        {
            var resource = Sim.PreemptiveResource(_env, 2);
            var log = new SinglyLinkedList<Tuple<double, int, PreemptionInfo>>();
            _env.DelayedProcess(Simple_PEM(0, resource, 1, log), 0);
            _env.DelayedProcess(Simple_PEM(1, resource, 1, log), 0);
            var p2 = _env.DelayedProcess(Simple_PEM(2, resource, 0, log), delay: 1);
            _env.DelayedProcess(Simple_PEM(3, resource, 2, log), delay: 2);
            _env.Run();

            Assert.AreEqual(1, log.First.Item1);
            Assert.AreEqual(1, log.First.Item2);
            Assert.AreEqual(p2, log.First.Item3.By);
            Assert.AreEqual(0, log.First.Item3.UsageSince);
            log.RemoveFirst();
            Assert.AreEqual(5, log.First.Item1);
            Assert.AreEqual(0, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
            log.RemoveFirst();
            Assert.AreEqual(6, log.First.Item1);
            Assert.AreEqual(2, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
            log.RemoveFirst();
            Assert.AreEqual(10, log.First.Item1);
            Assert.AreEqual(3, log.First.Item2);
            Assert.AreEqual(null, log.First.Item3);
        }

        [Test]
        public void WithTimeout()
        {
            var resource = Sim.PreemptiveResource(_env, 1);
            _env.Process(WithTimeout_PEM_A(resource, 1));
            _env.Process(WithTimeout_PEM_B(resource, 0));
            _env.Run();
        }
    }
}
