﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Resources
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Dessert.Resources;
    using NUnit.Framework;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class FilterStoreTests : StoreTestBase
    {
        private SimEvents StorePutter(FilterStore<int> store, int putCount, int timeout)
        {
            Debug.Assert(putCount >= 0);
            Debug.Assert(timeout >= 0);
            for (var i = 0; i < putCount; ++i)
            {
                Integers.Add(i);
                yield return store.Put(i);
                if (timeout > 0)
                {
                    yield return _env.Timeout(timeout);
                }
            }
        }

        private SimEvents StorePutter_SameEvent(FilterStore<int> store)
        {
            var putEv = store.Put(0);
            while (true)
            {
                yield return putEv;
                yield return _env.Timeout(1);
            }
        }

        private SimEvents Simple_PEM()
        {
            var filterStore = Sim.FilterStore<string>(_env, 2);
            var getEv = filterStore.Get(i => i == "b");
            yield return filterStore.Put("a");
            Assert.False(getEv.Succeeded);
            yield return filterStore.Put("b");
            Assert.True(getEv.Succeeded);
        }

        private SimEvents StoreGetter(FilterStore<int> store, int getCount, int timeout)
        {
            Debug.Assert(getCount >= 0);
            Debug.Assert(timeout >= 0);
            for (var i = 0; i < getCount; ++i)
            {
                var getEv = store.Get();
                yield return getEv;
                Assert.AreEqual(Integers[GetIdx++], getEv.Value);
                if (timeout > 0)
                {
                    yield return _env.Timeout(timeout);
                }
            }
        }

        private SimEvents StoreGetter_SameEvent(FilterStore<int> store)
        {
            var getEv = store.Get();
            while (true)
            {
                yield return store.Put(5);
                yield return getEv;
                yield return _env.Timeout(1);
            }
        }

        [TestCase(0), TestCase(1), TestCase(10), TestCase(100)]
        public void Put_InfiniteCapacity(int putCount)
        {
            var store = Sim.FilterStore<int>(_env);
            Assert.AreEqual(WaitPolicy.FIFO, store.ItemPolicy);
            Assert.AreEqual(WaitPolicy.FIFO, store.GetPolicy);
            Assert.AreEqual(WaitPolicy.FIFO, store.PutPolicy);
            _env.Process(StorePutter(store, putCount, 0));
            _env.Run(until: 100);
            Assert.AreEqual(putCount, store.Count);
            Assert.AreEqual(putCount, store.ItemQueue.Count());
            Assert.AreEqual(int.MaxValue, store.Capacity);
            Assert.IsEmpty(store.GetQueue);
            Assert.IsEmpty(store.PutQueue);
        }

        [TestCase(2, 1), TestCase(5, 2), TestCase(10, 9), TestCase(100, 10)]
        public void Put_BoundedCapacity(int putCount, int capacity)
        {
            Debug.Assert(putCount >= capacity);
            var store = Sim.FilterStore<int>(_env, capacity);
            Assert.AreEqual(WaitPolicy.FIFO, store.ItemPolicy);
            Assert.AreEqual(WaitPolicy.FIFO, store.GetPolicy);
            Assert.AreEqual(WaitPolicy.FIFO, store.PutPolicy);
            _env.Process(StorePutter(store, putCount, 0));
            _env.Run(until: 100);
            Assert.AreEqual(capacity, store.Count);
            Assert.AreEqual(capacity, store.ItemQueue.Count());
            Assert.AreEqual(capacity, store.Capacity);
            Assert.IsEmpty(store.GetQueue);
            Assert.True(store.PutQueue.Count() == 1);
        }

        [TestCase(2, 1), TestCase(5, 2), TestCase(10, 10), TestCase(100, 10)]
        public void Put_BoundedCapacity_ManyProducers(int putCount, int capacity)
        {
            Debug.Assert(putCount >= capacity);
            var store = Sim.FilterStore<int>(_env, capacity);
            Assert.AreEqual(WaitPolicy.FIFO, store.ItemPolicy);
            Assert.AreEqual(WaitPolicy.FIFO, store.GetPolicy);
            Assert.AreEqual(WaitPolicy.FIFO, store.PutPolicy);
            _env.Process(StorePutter(store, putCount, 1));
            _env.Process(StorePutter(store, putCount, 1));
            _env.Run(until: 100);
            Assert.AreEqual(capacity, store.Count);
            Assert.AreEqual(capacity, store.ItemQueue.Count());
            Assert.AreEqual(capacity, store.Capacity);
            Assert.IsEmpty(store.GetQueue);
            Assert.True(store.PutQueue.Count() == 2);
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Fifo(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount);
            var list = new List<Tuple<int, int>>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                store.Get().Callbacks.Add(e => list.Add(Tuple.Create(tmpI, e.Value)));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i].Item1);
                Assert.AreEqual(i, list[i].Item2);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Lifo(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.LIFO, WaitPolicy.FIFO);
            var list = new List<Tuple<int, int>>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                store.Get().Callbacks.Add(e => list.Add(Tuple.Create(tmpI, e.Value)));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1].Item1);
                Assert.AreEqual(i, list[i].Item2);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Priority_Default(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.Priority, WaitPolicy.FIFO);
            var list = new List<Tuple<int, int>>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                store.Get().Callbacks.Add(e => list.Add(Tuple.Create(tmpI, e.Value)));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i].Item1);
                Assert.AreEqual(i, list[i].Item2);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Priority_Increasing(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.Priority, WaitPolicy.FIFO);
            var list = new List<Tuple<int, int>>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                store.Get(i).Callbacks.Add(e => list.Add(Tuple.Create(tmpI, e.Value)));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i].Item1);
                Assert.AreEqual(i, list[i].Item2);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void GetPolicy_Priority_Decreasing(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.Priority, WaitPolicy.FIFO);
            var list = new List<Tuple<int, int>>();
            for (var i = 0; i < eventCount; ++i)
            {
                var tmpI = i;
                store.Get(-i).Callbacks.Add(e => list.Add(Tuple.Create(tmpI, e.Value)));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1].Item1);
                Assert.AreEqual(i, list[i].Item2);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Fifo(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i).Callbacks.Add(e => list.Add(e.Item));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get();
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Lifo(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.LIFO);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i).Callbacks.Add(e => list.Add(e.Item));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get();
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Priority_Default(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.Priority);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i).Callbacks.Add(e => list.Add(e.Item));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get();
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Priority_Increasing(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.Priority);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i, i).Callbacks.Add(e => list.Add(e.Item));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get();
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void PutPolicy_Priority_Decreasing(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.Priority);
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i, -i).Callbacks.Add(e => list.Add(e.Item));
            }
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get();
            }
            _env.Run();
            Assert.AreEqual(eventCount, list.Count);
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[eventCount - i - 1]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void ItemPolicy_Fifo(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount);
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get().Callbacks.Add(e => list.Add(e.Value));
            }
            _env.Run();
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void ItemPolicy_Lifo(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.FIFO, WaitPolicy.LIFO);
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get().Callbacks.Add(e => list.Add(e.Value));
            }
            _env.Run();
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(eventCount - i - 1, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void ItemPolicy_Priority_Default(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.FIFO, WaitPolicy.Priority);
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i);
            }
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get().Callbacks.Add(e => list.Add(e.Value));
            }
            _env.Run();
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void ItemPolicy_Priority_Increasing(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.FIFO, WaitPolicy.Priority);
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i, 0, i);
            }
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get().Callbacks.Add(e => list.Add(e.Value));
            }
            _env.Run();
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(i, list[i]);
            }
        }

        [TestCase(1), TestCase(10), TestCase(100)]
        public void ItemPolicy_Priority_Decreasing(int eventCount)
        {
            var store = Sim.FilterStore<int>(_env, eventCount, WaitPolicy.FIFO, WaitPolicy.FIFO, WaitPolicy.Priority);
            for (var i = 0; i < eventCount; ++i)
            {
                store.Put(i, 0, -i);
            }
            var list = new List<int>();
            for (var i = 0; i < eventCount; ++i)
            {
                store.Get().Callbacks.Add(e => list.Add(e.Value));
            }
            _env.Run();
            for (var i = 0; i < eventCount; ++i)
            {
                Assert.AreEqual(eventCount - i - 1, list[i]);
            }
        }

        [Test]
        public void Construction_RightType()
        {
            Assert.IsInstanceOf(typeof(FilterStore<int>), Sim.FilterStore<int>(_env));
        }

        [Test]
        public void Get_SameEvent()
        {
            var store = Sim.FilterStore<int>(_env);
            _env.Process(StoreGetter_SameEvent(store));
            _env.Run(until: 100);
        }

        [Test]
        public void LastItemGoodForFilter()
        {
            var filterStore = Sim.FilterStore<int>(_env, 10);
            filterStore.Put(1);
            filterStore.Put(3);
            filterStore.Put(5);
            filterStore.Put(2);
            var getEv = filterStore.Get(i => i % 2 == 0);
            _env.Run();
            Assert.True(getEv.Succeeded);
            Assert.AreEqual(2, getEv.Value);
        }

        [Test]
        public void ProducerConsumer_OneProducer_OneConsumer_BoundedCapacity()
        {
            var store = Sim.FilterStore<int>(_env, 50);
            _env.Process(StorePutter(store, 100, 1));
            _env.Process(StoreGetter(store, 10, 10));
            _env.Run(until: 1000);
            Assert.AreEqual(50, store.Count);
            Assert.AreEqual(50, store.ItemQueue.Count());
            Assert.AreEqual(50, store.Capacity);
            Assert.IsEmpty(store.GetQueue);
            Assert.True(store.PutQueue.Count() == 1);
        }

        [Test]
        public void ProducerConsumer_OneProducer_OneConsumer_UnboundCapacity()
        {
            var store = Sim.FilterStore<int>(_env);
            _env.Process(StorePutter(store, 10, 1));
            _env.Process(StoreGetter(store, 10, 1));
            _env.Run(until: 100);
            Assert.AreEqual(0, store.Count);
            Assert.AreEqual(0, store.ItemQueue.Count());
            Assert.AreEqual(int.MaxValue, store.Capacity);
            Assert.IsEmpty(store.GetQueue);
            Assert.IsEmpty(store.PutQueue);
        }

        [Test]
        public void ProducerConsumer_TwoProducers_TwoConsumers_UnboundCapacity()
        {
            var store = Sim.FilterStore<int>(_env);
            _env.Process(StorePutter(store, 100, 1));
            _env.Process(StorePutter(store, 100, 1));
            _env.Process(StoreGetter(store, 100, 1));
            _env.Process(StoreGetter(store, 100, 1));
            _env.Run(until: 1000);
            Assert.AreEqual(0, store.Count);
            Assert.AreEqual(0, store.ItemQueue.Count());
            Assert.AreEqual(int.MaxValue, store.Capacity);
            Assert.IsEmpty(store.GetQueue);
            Assert.IsEmpty(store.PutQueue);
        }

        [Test]
        public void Put_SameEvent()
        {
            var store = Sim.FilterStore<int>(_env);
            _env.Process(StorePutter_SameEvent(store));
            _env.Run(until: 100);
        }

        [Test]
        public void Simple()
        {
            _env.Process(Simple_PEM());
            _env.Run();
        }
    }
}
