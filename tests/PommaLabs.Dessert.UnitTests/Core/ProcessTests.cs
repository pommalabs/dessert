﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Core
{
    using System;
    using Dessert.Events;
    using Dessert.Resources;
    using NUnit.Framework;
    using Shouldly;
    using IEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class ProcessTests : TestBase
    {
        private IEvents Interrupted_YieldingGetToken()
        {
            Container.GetEvent getEv;
            _env.Process(Interrupter(_env.ActiveProcess, 5));
            yield return getEv = Sim.Container(_env, 20, 10).Get(20);
            Assert.AreEqual(5, _env.Now);
            Assert.False(getEv.Disposed);
            Assert.True(_env.ActiveProcess.Interrupted());
            yield return _env.Timeout(7);
        }

        private IEvents Interrupted_YieldingTimeoutToken()
        {
            Timeout<double> timeout;
            _env.Process(Interrupter(_env.ActiveProcess, 5));
            yield return timeout = _env.Timeout(7);
            Assert.AreEqual(5, _env.Now);
            Assert.False(timeout.Succeeded);
            Assert.True(_env.ActiveProcess.Interrupted());
            yield return _env.Timeout(7);
        }

        private IEvents Interrupter(SimProcess victim, double delay = 0, object value = null)
        {
            yield return _env.Timeout(delay);
            if (value != null)
            {
                victim.Interrupt(value);
            }
            else
            {
                victim.Interrupt();
            }
        }

        private IEvents TargetTester1()
        {
            while (true)
            {
                Assert.AreSame(null, _env.ActiveProcess.Target);
                yield return _env.Process(TargetTester2(_env.ActiveProcess));
                yield return _env.Timeout(10);
            }
        }

        private IEvents TargetTester2(SimProcess starter)
        {
            Assert.AreSame(null, _env.ActiveProcess.Target);
            Assert.AreSame(_env.ActiveProcess, starter.Target);
            yield break;
        }

        private IEvents TimeoutYielder_WithInner()
        {
            yield return _env.Timeout(5);
            var inner = _env.Process(TargetTester1());
            Assert.True(inner.IsAlive);
            yield return inner;
            Assert.False(inner.IsAlive);
        }

        private IEvents TriggeredTimeout_PEM()
        {
            const string value = "I was already done";
            var ev = _env.Timeout(1, value);
            // Starts the child after the timeout has already happened.
            yield return _env.Timeout(2);
            var child = _env.Process(TriggeredTimeout_PEM_Child(ev));
            yield return child;
            Assert.AreEqual(value, child.Value);
        }

        private IEvents TriggeredTimeout_PEM_Child(SimEvent ev)
        {
            yield return ev;
            _env.Exit(ev.Value);
        }

        [Test]
        public void Interrupt_DeadProcess()
        {
            var dead = _env.Process(EmptyProcess());
            _env.Process(Interrupter(dead));
            Should.Throw<InvalidOperationException>(() => _env.Run());
        }

        [Test]
        public void Interrupt_YieldingGetToken()
        {
            _env.Process(Interrupted_YieldingGetToken());
            _env.Run();
            Assert.AreEqual(12, _env.Now);
        }

        [Test]
        public void Interrupt_YieldingTimeoutToken()
        {
            _env.Process(Interrupted_YieldingTimeoutToken());
            _env.Run();
            Assert.AreEqual(12, _env.Now);
        }

        [Test]
        public void IsAlive_TimeoutYielder()
        {
            var yielder = _env.Process(TimeoutYielder());
            Assert.True(yielder.IsAlive);
            _env.Run(100);
            Assert.False(yielder.IsAlive);
        }

        [Test]
        public void IsAlive_TimeoutYielder_WithInner()
        {
            var yielder = _env.Process(TimeoutYielder_WithInner());
            Assert.True(yielder.IsAlive);
            _env.Run(100);
            // Inner is a never ending process...
            Assert.True(yielder.IsAlive);
        }

        [Test]
        public void Target_YieldingProcess()
        {
            _env.Process(TargetTester1());
            _env.Run(100);
        }

        [Test]
        public void TriggeredTimeout()
        {
            _env.Run(_env.Process(TriggeredTimeout_PEM()));
        }
    }
}
