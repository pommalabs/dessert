﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Dessert.Tests.Core
{
    using System;
    using NodaTime;
    using NodaTime.Testing;
    using NUnit.Framework;
    using Shouldly;
    using SimEvents = System.Collections.Generic.IEnumerable<SimEvent>;

    internal sealed class EnvironmentTests : TestBase
    {
        private SimEvents Interrupted()
        {
            const string SentValue = "PINO";
            _env.Process(Interrupter(_env.ActiveProcess, SentValue, 5));
            while (true)
            {
                yield return _env.Timeout(6);
                Assert.True(_env.ActiveProcess.Interrupted(out var value));
                Assert.AreEqual(SentValue, value);
            }
        }

        private SimEvents Interrupted_NotCaught()
        {
            _env.Process(Interrupter(_env.ActiveProcess, null));
            while (true)
            {
                yield return _env.Timeout(5);
            }
        }

        private static SimEvents NullYielder()
        {
            while (true)
            {
                yield return null;
            }
        }

        [Test]
        public void Interrupt_ManyTimes()
        {
            _env.Process(Interrupted());
            _env.Run(700);
        }

        [Test]
        public void Interrupt_OneTime()
        {
            _env.Process(Interrupted());
            _env.Run(3);
        }

        [Test]
        public void Interrupt_Uncaught()
        {
            _env.Process(Interrupted_NotCaught());
            Should.Throw<InterruptUncaughtException>(() => _env.Run(7));
        }

        [Test]
        public void Peek_NoActivities()
        {
            Assert.AreEqual(double.PositiveInfinity, _env.Peek);
        }

        [Test]
        public void Peek_OneActivity()
        {
            _env.Process(TimeoutYielder());
            Assert.AreEqual(0, _env.Peek);
        }

        [Test]
        public void Peek_OneActivity_WithDelay()
        {
            const double Delay = 7;
            _env.DelayedProcess(TimeoutYielder(), Delay);
            Assert.AreEqual(0, _env.Peek);
        }

        [Test]
        public void Peek_OneEvent()
        {
            const double Delay = 7;
            _env.Timeout(Delay);
            Assert.AreEqual(Delay, _env.Peek);
        }

        [Test]
        public void Peek_TwoActivities()
        {
            const double ShortDelay = 7;
            const double LongDelay = 14;
            _env.DelayedProcess(TimeoutYielder(), LongDelay);
            _env.DelayedProcess(TimeoutYielder(), ShortDelay);
            Assert.AreEqual(0, _env.Peek);
        }

        [Test]
        public void Peek_TwoActivities_AfterRun()
        {
            const double ShortDelay = 7;
            const double LongDelay = 14;
            _env.DelayedProcess(TimeoutYielder(), LongDelay);
            _env.DelayedProcess(TimeoutYielder(), ShortDelay).Callbacks.Add(p => Assert.AreEqual(LongDelay, _env.Peek));
            _env.Run();
            Assert.AreEqual(double.PositiveInfinity, _env.Peek);
        }

        [Test]
        public void Peek_TwoEvents()
        {
            const double ShortDelay = 7;
            const double LongDelay = 14;
            _env.Timeout(LongDelay);
            _env.Timeout(ShortDelay);
            Assert.AreEqual(ShortDelay, _env.Peek);
        }

        [Test]
        public void Run_NoActivities()
        {
            Should.Throw<InvalidOperationException>(() => _env.Run(10));
        }

        [Test]
        public void Yield_NullEvent()
        {
            _env.Process(NullYielder());
            Should.Throw<ArgumentNullException>(() => _env.Run(1));
        }

        [Test]
        public void Environment_ShouldNotChangeDefaultRealTimeOptionsAfterCreation_ScalingFactor()
        {
            var env = Sim.Environment();
            Assert.That(env.RealTime.Enabled, Is.False);
            Assert.That(env.RealTime.Locked, Is.True);
            Should.Throw<InvalidOperationException>(() => env.RealTime.ScalingFactor = 3.0);
        }

        [Test]
        public void Environment_ShouldNotChangeDefaultRealTimeOptionsAfterCreation_WallClock()
        {
            var env = Sim.Environment();
            Assert.That(env.RealTime.Enabled, Is.False);
            Assert.That(env.RealTime.Locked, Is.True);
            Should.Throw<InvalidOperationException>(() => env.RealTime.WallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant()));
        }

        [Test]
        public void Environment_ShouldNotChangeDefaultRealTimeOptionsAfterCreation_WithSeed_ScalingFactor()
        {
            var env = Sim.Environment(21);
            Assert.That(env.RealTime.Enabled, Is.False);
            Assert.That(env.RealTime.Locked, Is.True);
            Should.Throw<InvalidOperationException>(() => env.RealTime.ScalingFactor = 3.0);
        }

        [Test]
        public void Environment_ShouldNotChangeDefaultRealTimeOptionsAfterCreation_WithSeed_WallClock()
        {
            var env = Sim.Environment(21);
            Assert.That(env.RealTime.Enabled, Is.False);
            Assert.That(env.RealTime.Locked, Is.True);
            Should.Throw<InvalidOperationException>(() => env.RealTime.WallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant()));
        }

        [Test]
        public void RealTimeOptions_ShouldOverwriteDefaultScalingFactor()
        {
            var opts = new SimEnvironment.RealTimeOptions();
            Assert.That(opts.ScalingFactor, Is.EqualTo(SimEnvironment.RealTimeOptions.DefaultScalingFactor));

            opts.ScalingFactor = Math.PI;
            Assert.That(opts.ScalingFactor, Is.EqualTo(Math.PI));
        }

        [Test]
        public void RealTimeOptions_ShouldOverwriteDefaultScalingFactorTwice()
        {
            var opts = new SimEnvironment.RealTimeOptions();
            Assert.That(opts.ScalingFactor, Is.EqualTo(SimEnvironment.RealTimeOptions.DefaultScalingFactor));

            opts.ScalingFactor = Math.PI;
            Assert.That(opts.ScalingFactor, Is.EqualTo(Math.PI));

            opts.ScalingFactor = Math.E;
            Assert.That(opts.ScalingFactor, Is.EqualTo(Math.E));
        }

        [Test]
        public void RealTimeOptions_ShouldNotOverwriteIfLessThanMinimum()
        {
            var opts = new SimEnvironment.RealTimeOptions();
            Assert.That(opts.ScalingFactor, Is.EqualTo(SimEnvironment.RealTimeOptions.DefaultScalingFactor));

            Should.Throw<ArgumentOutOfRangeException>(() => opts.ScalingFactor = SimEnvironment.RealTimeOptions.MinScalingFactor / 2.0);
        }

        [Test]
        public void RealTimeOptions_ShouldOverwriteDefaultWallClock()
        {
            var opts = new SimEnvironment.RealTimeOptions();
            Assert.That(opts.WallClock, Is.SameAs(SimEnvironment.RealTimeOptions.DefaultWallClock));

            var newWallClock = NetworkClock.Instance;
            opts.WallClock = newWallClock;
            Assert.That(opts.WallClock, Is.SameAs(newWallClock));
        }

        [Test]
        public void RealTimeOptions_ShouldOverwriteDefaultWallClockTwice()
        {
            var opts = new SimEnvironment.RealTimeOptions();
            Assert.That(opts.WallClock, Is.SameAs(SimEnvironment.RealTimeOptions.DefaultWallClock));

            IClock newWallClock = NetworkClock.Instance;
            opts.WallClock = newWallClock;
            Assert.That(opts.WallClock, Is.SameAs(newWallClock));

            newWallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant());
            opts.WallClock = newWallClock;
            Assert.That(opts.WallClock, Is.SameAs(newWallClock));
        }

        [Test]
        public void RealTimeEnvironment_ShouldBeCreatedWithDefaultOptions()
        {
            var env = Sim.RealTimeEnvironment();
            Assert.That(env.RealTime.Enabled, Is.True);
            Assert.That(env.RealTime.ScalingFactor, Is.EqualTo(SimEnvironment.RealTimeOptions.DefaultScalingFactor));
            Assert.That(env.RealTime.WallClock, Is.SameAs(SimEnvironment.RealTimeOptions.DefaultWallClock));
        }

        [Test]
        public void RealTimeEnvironment_ShouldBeCreatedWithDefaultOptions_WithSeed()
        {
            var env = Sim.RealTimeEnvironment(21);
            Assert.That(env.RealTime.Enabled, Is.True);
            Assert.That(env.RealTime.ScalingFactor, Is.EqualTo(SimEnvironment.RealTimeOptions.DefaultScalingFactor));
            Assert.That(env.RealTime.WallClock, Is.SameAs(SimEnvironment.RealTimeOptions.DefaultWallClock));
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeDefaultOptionsAfterCreation_ScalingFactor()
        {
            var env = Sim.RealTimeEnvironment();
            Should.Throw<InvalidOperationException>(() => env.RealTime.ScalingFactor = 3.0);
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeDefaultOptionsAfterCreation_WallClock()
        {
            var env = Sim.RealTimeEnvironment();
            Should.Throw<InvalidOperationException>(() => env.RealTime.WallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant()));
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeDefaultOptionsAfterCreation_WithSeed_ScalingFactor()
        {
            var env = Sim.RealTimeEnvironment(21);
            Should.Throw<InvalidOperationException>(() => env.RealTime.ScalingFactor = 3.0);
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeDefaultOptionsAfterCreation_WithSeed_WallClock()
        {
            var env = Sim.RealTimeEnvironment(21);
            Should.Throw<InvalidOperationException>(() => env.RealTime.WallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant()));
        }

        [Test]
        public void RealTimeEnvironment_ShouldBeCreatedWithCustomOptions()
        {
            const double NewScalingFactor = 3.0;
            var newWallClock = NetworkClock.Instance;
            var env = Sim.RealTimeEnvironment(new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = NewScalingFactor,
                WallClock = newWallClock
            });
            Assert.That(env.RealTime.Enabled, Is.True);
            Assert.That(env.RealTime.Locked, Is.True);
            Assert.That(env.RealTime.ScalingFactor, Is.EqualTo(NewScalingFactor));
            Assert.That(env.RealTime.WallClock, Is.SameAs(newWallClock));
        }

        [Test]
        public void RealTimeEnvironment_ShouldBeCreatedWithCustomOptions_WithSeed()
        {
            const double NewScalingFactor = 3.0;
            var newWallClock = NetworkClock.Instance;
            var env = Sim.RealTimeEnvironment(21, new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = NewScalingFactor,
                WallClock = newWallClock
            });
            Assert.That(env.RealTime.Enabled, Is.True);
            Assert.That(env.RealTime.Locked, Is.True);
            Assert.That(env.RealTime.ScalingFactor, Is.EqualTo(NewScalingFactor));
            Assert.That(env.RealTime.WallClock, Is.SameAs(newWallClock));
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeCustomOptionsAfterCreation_ScalingFactor()
        {
            const double NewScalingFactor = 3.0;
            var newWallClock = NetworkClock.Instance;
            var env = Sim.RealTimeEnvironment(new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = NewScalingFactor,
                WallClock = newWallClock
            });
            Should.Throw<InvalidOperationException>(() => env.RealTime.ScalingFactor = 3.0);
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeCustomOptionsAfterCreation_WallClock()
        {
            const double NewScalingFactor = 3.0;
            var newWallClock = NetworkClock.Instance;
            var env = Sim.RealTimeEnvironment(new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = NewScalingFactor,
                WallClock = newWallClock
            });
            Should.Throw<InvalidOperationException>(() => env.RealTime.WallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant()));
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeCustomOptionsAfterCreation_WithSeed_ScalingFactor()
        {
            const double NewScalingFactor = 3.0;
            var newWallClock = NetworkClock.Instance;
            var env = Sim.RealTimeEnvironment(21, new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = NewScalingFactor,
                WallClock = newWallClock
            });
            Should.Throw<InvalidOperationException>(() => env.RealTime.ScalingFactor = 3.0);
        }

        [Test]
        public void RealTimeEnvironment_ShouldNotChangeCustomOptionsAfterCreation_WithSeed_WallClock()
        {
            const double NewScalingFactor = 3.0;
            var newWallClock = NetworkClock.Instance;
            var env = Sim.RealTimeEnvironment(21, new SimEnvironment.RealTimeOptions
            {
                ScalingFactor = NewScalingFactor,
                WallClock = newWallClock
            });
            Should.Throw<InvalidOperationException>(() => env.RealTime.WallClock = new FakeClock(SystemClock.Instance.GetCurrentInstant()));
        }
    }
}
