# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.2.0] - 2021-11-28

### Changed

- The project currently targets: .NET Core 3.1 LTS, .NET 6 LTS, .NET Framework 4.6.1 and 4.7.2.
- The project will target only .NET LTS releases and supported .NET Framework 4.x releases.

## [3.1.0] - 2021-01-02

### Changed

- Project has been cleaned up and upgraded to .NET Core/Standard.
- Namespace has been changed from DIBRIS to PommaLabs.

### Removed

- Boo samples were removed, support for that language is missing from recent IDEs.

## [3.0.12] - 2016-04-03

### Changed

- Fixed a few unit tests which were failing under the official NUnit test runner.
- Real-time options could not be changed, since modifiers were internal.
- Added an example, see "RealTime/CustomOptions", in which it is shown how to change the scaling factor.
- Fixed a wrong management of the scaling factor at runtime.
- Updated some dependencies.

[3.2.0]: https://gitlab.com/pomma89/dessert/-/compare/3.1.0...3.2.0
[3.1.0]: https://gitlab.com/pomma89/dessert/-/compare/v3.0.12...3.1.0
[3.0.12]: https://gitlab.com/pomma89/dessert/-/compare/v3.0.11...v3.0.12
